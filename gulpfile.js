var gulp = require('gulp');
var changed = require('gulp-changed');

gulp.task('ru', function() {
	var DEST = "./static/ru";

	return gulp.src("./content/ru/**/*.{png,gif,jpg}")
		.pipe(changed(DEST))
		.pipe(gulp.dest(DEST));
});

gulp.task('en', function() {
	var DEST = "./static/en";

	return gulp.src("./content/en/**/*.{png,gif,jpg}")
		.pipe(changed(DEST))
		.pipe(gulp.dest(DEST));
});

gulp.task('pl', function() {
    var DEST = "./static/pl";

    return gulp.src("./content/pl/**/*.{png,gif,jpg}")
        .pipe(changed(DEST))
        .pipe(gulp.dest(DEST));
});