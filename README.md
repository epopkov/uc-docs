# Unite Control docs

You can see documentation online here:

* [http://en.unitecontrol.com/docs](http://en.unitecontrol.com/docs)
* [http://ru.unitecontrol.com/docs](http://ru.unitecontrol.com/docs)

## Installation and compilation

1. Install Node.js
2. cd daux.io && npm install
3. cd ..
4. npm install
5. npm run gen