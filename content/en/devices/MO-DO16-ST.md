# UC MO-DO16-ST {.header}

[UC MO-DO16-ST Expansion Module](http://en.unitecontrol.com/MO-DO16-ST.html), 16 digital outputs Modbus RTU (RS485)

![](images/MO-DO16-ST/device.png) {.img-responsive}

## User Manual

## Safety Rules

* Before first use, refer to this manual
* Before first use, make sure that all cables are connected properly
* Please ensure proper working conditions, according to the device specifications (e.g. supply voltage, temperature, maximum power consumption)
* Before making any modifications to wiring connections, turn off the power supply

## Module Features

### Purpose and description of the module

- Name: Expansion Module, 16 digital outputs, Modbus RTU (RS485), ASCII
- art.: MO-DO16-ST
- Module MO-DO16 works in Slave mode.
- The module has 16 digital outputs. All inputs and outputs are isolated from the logic of using optocouplers. The module is produced in two versions with PNP and NPN outputs:
Mark:
Output type PNP: MO-DO16-ST (-PNP)
Output type NPN: MO DO16-ST-NPN
- This module is connected to the RS485 bus with twisted-pair wire. Communication is via Modbus RTU or Modbus ASCII.
- The module is designed for mounting on a DIN rail in accordance with DIN EN 5002.
- The module is equipped with a set of LEDs are used to indicate the status of inputs and outputs useful for diagnostics purposes and helping to find errors.
- Module configuration is done via USB by using a dedicated computer program. You can also change the parameters using the ModBus protocol.

### Techical Specifications

<table>
  <tr>
    <td>
      Power Supply
    </td>
    <td>
      Voltage
    </td>
    <td>
      10-30 V DC; 10-28 V AC
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Maximum Current
    </td>
    <td>
      DC: 100 mA @ 24 V
      <br>AC: 125 mA @ 24 V
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Max Power Consumption
    </td>
    <td>
      DC: 2,4 W
      <br>AC: 3 VA
    </td>
  </tr>
  <tr>
    <td>
      Connection Speed
    </td>
    <td>
    </td>
    <td>
      2400 - 115200 bps
    </td>
  </tr>
  <tr>
    <td>
      Digital Outputs
    </td>
    <td>
      No of outputs
    </td>
    <td>
      16
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Max Voltage
    </td>
    <td>
      30 В
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Max current
    </td>
    <td>
      500 mA
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Output Type
    </td>
    <td>
      PNP or NPN (depends on version)
    </td>
  </tr>
  <tr>
    <td>
      Temperature
    </td>
    <td>
      Work
    </td>
    <td>
      -20°C ÷ +65°C
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Storage
    </td>
    <td>
      -40°C ÷ +85°C
    </td>
  </tr>
  <tr>
    <td>
      Connectors
    </td>
    <td>
      Power Supply
    </td>
    <td>
      2 pin
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Communication
    </td>
    <td>
      3 pin
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Outputs
    </td>
    <td>
      2 x 10 pin
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Configuration
    </td>
    <td>
      MiniUSB
    </td>
  </tr>
  <tr>
    <td>
      Dimensions
    </td>
    <td>
      Height
    </td>
    <td>
      88 mm
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Length
    </td>
    <td>
      110 mm
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Width
    </td>
    <td>
      62 mm
    </td>
  </tr>
  <tr>
    <td>
      Interface
    </td>
    <td>
      RS485
    </td>
    <td>
      Up to 128 devices
    </td>
  </tr>
</table>

### Dimensions of the product

Look and dimensions of the module are shown below. The module is mounted directly to the rail in the DIN industry standard. Power connectors, communication and IOs are at the bottom and top of the module. USB connector configuration and indicators located on the front of the module.

![](images/MO-DO16-ST/overview.png) {.img-responsive}

## Communication configuration

### Grounding and shielding

In most cases, IO (Inputs/Outputs) modules will be installed in an enclosure along with other devices which generate electromagnetic radiation. Examples of these devices are relays and contactors, transformers, motor controllers etc. This electromagnetic radiation can induce electrical noise into both power and signal lines, as well as direct radiation into the module causing negative effects on the system. Appropriate grounding, shielding and other protective steps should be taken at the installation stage to prevent these effects. These protective steps include control cabinet grounding, module grounding, cable shield grounding, protective elements for electromagnetic switching devices, correct wiring as well as consideration of cable types and their cross sections.

### Network Termination

Transmission line effects often present a problem on data communication networks. These problems include reflections and signal attenuation.

To eliminate the presence of reflections from the end of the cable, the cable must be terminated at both ends with a resistor across the line equal to its characteristic impedance. Both ends must be terminated since the direction of propagation is bidirectional. In the case of an RS485 twisted pair cable this termination is typically 120 Ω.

### Setting Module Address in RS485 Modbus Network

The following table shows how to set switch to determine the address of the module. The module address is set with the switches in the range of 0 to 127. Addresses From 127 to 255 can by set via RS485 or USB.

<table>
  <tr>
    <td>
      <b>Switch</b>
    </td>
    <td>
      <b>Address</b>
    </td>
  </tr>
  <tr>
    <td>
      SW1
    </td>
    <td>
      +1
    </td>
  </tr>
  <tr>
    <td>
      SW2
    </td>
    <td>
      +2
    </td>
  </tr>
  <tr>
    <td>
      SW3
    </td>
    <td>
      +4
    </td>
  </tr>
  <tr>
    <td>
      SW4
    </td>
    <td>
      +8
    </td>
  </tr>
  <tr>
    <td>
      SW5
    </td>
    <td>
      +16
    </td>
  </tr>
  <tr>
    <td>
      SW6
    </td>
    <td>
      +32
    </td>
  </tr>
  <tr>
    <td>
      SW7
    </td>
    <td>
      +64
    </td>
  </tr>
</table>

### Types of Modbus Registers

There are 4 types of variables available in the module

<table>
  <tr>
    <th>
      <b>Type</b>
    </th>
    <th>
      <b>Beginning address</b>
    </th>
    <th>
      <b>Variable</b>
    </th>
    <th>
      <b>Access</b>
    </th>
    <th>
      <b>Modbus Command</b>
    </th>
  </tr>
  <tr>
    <td>
      1
    </td>
    <td>
      00001
    </td>
    <td>
      Digital Outputs
    </td>
    <td>
      Bit Read &amp; Write
    </td>
    <td>
      1, 5, 15
    </td>
  </tr>
  <tr>
    <td>
      2
    </td>
    <td>
      10001
    </td>
    <td>
      Digital Inputs
    </td>
    <td>
      Bit Read
    </td>
    <td>
      2
    </td>
  </tr>
  <tr>
    <td>
      3
    </td>
    <td>
      30001
    </td>
    <td>
      Input Registers
    </td>
    <td>
      Registered Read
    </td>
    <td>
      3
    </td>
  </tr>
  <tr>
    <td>
      4
    </td>
    <td>
      40001
    </td>
    <td>
      Output Registers
    </td>
    <td>
      Registered Read &amp; Write
    </td>
    <td>
      4, 6, 16
    </td>
  </tr>
</table>

### Communication settings

The data stored in the modules memory are in 16-bit registers. Access to registers is via MODBUS RTU or MODBUS ASCII.

#### Default settings

You can restore the default configuration by the switch SW8 (see - Restore the default configuration)

<table>
  <tr>
    <td>
      Baud rate, bps
    </td>
    <td>
      19200
    </td>
  </tr>
  <tr>
    <td>
      Parity
    </td>
    <td>
      No
    </td>
  </tr>
  <tr>
    <td>
      Data bits
    </td>
    <td>
      1
    </td>
  </tr>
  <tr>
    <td>
      Stop bits
    </td>
    <td>
      1
    </td>
  </tr>
  <tr>
    <td>
      Reply Delay, ms
    </td>
    <td>
      0
    </td>
  </tr>
  <tr>
    <td>
      Modbus Type
    </td>
    <td>
      RTU
    </td>
  </tr>
</table>

#### Restore the default configuration

To restore the default configuration:
* turn off the power
* turn on the switch SW8
* turn on the power
* when power and communication LED flash turn off the switch SW8

Caution! After restoring the default configuration all values stored in the registers will be cleared as well.

#### Configuration registers

<table>
  <tr>
    <th>
      Address
    </th>
    <th>
      Name
    </th>
    <th>
      Values
    </th>
  </tr>
  <tr>
    <td>
      40003
    </td>
    <td>
      Baud rate
    </td>
    <td>
      0 - 2400
      <br>1 - 4800
      <br>2 - 9600
      <br>3 - 19200
      <br>4 - 38400
      <br>5 - 57600
      <br>6 - 115200
      <br>other – value * 10
    </td>
  </tr>
  <tr>
    <td>
      40005
    </td>
    <td>
      Parity
    </td>
    <td>
      0 - none
      <br>1 - odd
      <br>2 - even
      <br>3 - always 1
      <br>4 - always 0
    </td>
  </tr>
  <tr>
    <td>
      40004
    </td>
    <td>
      Stop Bits LSB
    </td>
    <td>
      1 - one stop bit
      <br>2 - 2 two stop bits
    </td>
  </tr>
  <tr>
    <td>
      40004
    </td>
    <td>
      Data Bits MSB
    </td>
    <td>
      7 - 7 data bits
      <br>8 - 8 data bits
    </td>
  </tr>
  <tr>
    <td>
      40006
    </td>
    <td>
      Response delay
    </td>
    <td>
      Time in ms
    </td>
  </tr>
  <tr>
    <td>
      40007
    </td>
    <td>
      Modbus Mode
    </td>
    <td>
      0 - RTU
      <br>1 - ASCII
    </td>
  </tr>
</table>

## Indicators

![](images/MO-DO16-ST/leds.png) {.img-responsive}

<table>
  <tr>
    <th>
      Indicator
    </th>
    <th>
      Description
    </th>
  </tr>
  <tr>
    <td>
      Power (ON)
    </td>
    <td>
      LED indicates that the module is correctly powered.
    </td>
  </tr>
  <tr>
    <td>
      TX
    </td>
    <td>
      The LED lights up when the unit received the correct packet and sends the answer.
    </td>
  </tr>
  <tr>
    <td>
      Outputs 
    </td>
    <td>
      LED indicates that on the output is high state.
    </td>
  </tr>
</table>

## Module Connection - PNP Outputs

![](images/MO-DO16-ST/connection1.jpg) {.img-responsive}

## Module Connection - NPN Outputs

![](images/MO-DO16-ST/connection2.jpg) {.img-responsive}

## Front panel removing

![](images/MO-DO16-ST/disassemble.gif) {.img-responsive}

## Switches

![](images/MO-DO16-ST/switch.png) {.img-responsive}

<table>
  <tr>
    <th>
      Switch
    </th>
    <th>
      Function
    </th>
    <th>
      Description
    </th>
  </tr>
  <tr>
    <td>
      1
    </td>
    <td>
      Module address +1
    </td>
    <td rowspan="7">
      Setting module address from 0 to 127
    </td>
  </tr>
  <tr>
    <td>
      2
    </td>
    <td>
      Module address +2
    </td>
  </tr>
  <tr>
    <td>
      3
    </td>
    <td>
      Module address +4
    </td>
  </tr>
  <tr>
    <td>
      4
    </td>
    <td>
      Module address +8
    </td>
  </tr>
  <tr>
    <td>
      5
    </td>
    <td>
      Module address +16
    </td>
  </tr>
  <tr>
    <td>
      6
    </td>
    <td>
      Module address +32
    </td>
  </tr>
  <tr>
    <td>
      7
    </td>
    <td>
      Module address +64
    </td>
  </tr>
  <tr>
    <td>
      8
    </td>
    <td>
      Restoring default settings
    </td>
    <td>
      Restoring default settings (see - Default settings and - Restore the default configuration).
    </td>
  </tr>
</table>

## Modules Registers

### Registered access

<table>
  <thead>
    <tr>
      <th>
        Address
      </th>
      <th>
        Dec
      </th>
      <th>
        Hex
      </th>
      <th>
        Register name
      </th>
      <th>
        Acess
      </th>
      <th>
        Description
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        30001
      </td>
      <td>
        0
      </td>
      <td>
        0x00
      </td>
      <td>
        Version/Type
      </td>
      <td>
        Read
      </td>
      <td>
        Version and Type of the device
      </td>
    </tr>
    <tr>
      <td>
        30002
      </td>
      <td>
        1
      </td>
      <td>
        0x01
      </td>
      <td>
        Switches
      </td>
      <td>
        Read
      </td>
      <td>
        Switches state
      </td>
    </tr>
    <tr>
      <td>
        40003
      </td>
      <td>
        2
      </td>
      <td>
        0x02
      </td>
      <td>
        Baud rate
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        RS485 baud rate
      </td>
    </tr>
    <tr>
      <td>
        40004
      </td>
      <td>
        3
      </td>
      <td>
        0x03
      </td>
      <td>
        Stop Bits & Data Bits
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of Stop bits & Data Bits
      </td>
    </tr>
    <tr>
      <td>
        40005
      </td>
      <td>
        4
      </td>
      <td>
        0x04
      </td>
      <td>
        Parity
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Parity bit
      </td>
    </tr>
    <tr>
      <td>
        40006
      </td>
      <td>
        5
      </td>
      <td>
        0x05
      </td>
      <td>
        Response Delay
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Response delay in ms
      </td>
    </tr>
    <tr>
      <td>
        40007
      </td>
      <td>
        6
      </td>
      <td>
        0x06
      </td>
      <td>
        Modbus Mode
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Modbus Mode (ASCII or RTU)
      </td>
    </tr>
    <tr>
      <td>
        40009
      </td>
      <td>
        8
      </td>
      <td>
        0x08
      </td>
      <td>
        Watchdog
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Watchdog
      </td>
    </tr>
    <tr>
      <td>
        40013
      </td>
      <td>
        12
      </td>
      <td>
        0x0C
      </td>
      <td>
        Default Output State
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Default output state (after power on or watchdog reset)
      </td>
    </tr>
    <tr>
      <td>
        40033
      </td>
      <td>
        32
      </td>
      <td>
        0x20
      </td>
      <td>
        Received packets MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets
      </td>
    </tr>
    <tr>
      <td>
        40034
      </td>
      <td>
        33
      </td>
      <td>
        0x21
      </td>
      <td>
        Received packets LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets
      </td>
    </tr>
    <tr>
      <td>
        40035
      </td>
      <td>
        34
      </td>
      <td>
        0x22
      </td>
      <td>
        Incorrect packets MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets with error
      </td>
    </tr>
    <tr>
      <td>
        40036
      </td>
      <td>
        35
      </td>
      <td>
        0x23
      </td>
      <td>
        Incorrect packets LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets with error
      </td>
    </tr>
    <tr>
      <td>
        40037
      </td>
      <td>
        36
      </td>
      <td>
        0x24
      </td>
      <td>
        Sent packets MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of sent packets
      </td>
    </tr>
    <tr>
      <td>
        40038
      </td>
      <td>
        37
      </td>
      <td>
        0x25
      </td>
      <td>
        Sent packets LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of sent packets
      </td>
    </tr>
    <tr>
      <td>
        40052
      </td>
      <td>
        51
      </td>
      <td>
        0x33
      </td>
      <td>
        Outputs
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Output state
      </td>
    </tr>
  </tbody>
</table>

### Побитовый доступ

<table>
  <tr>
    <th>
      Address
    </th>
    <th>
      Address
      <br>DEC
    </th>
    <th>
      Address
      <br>HEX
    </th>
    <th>
      Register name
    </th>
    <th>
      Access
    </th>
    <th>
      Description
    </th>
  </tr>
  <tr>
    <td>
      193
    </td>
    <td>
      192
    </td>
    <td>
      0x0C0
    </td>
    <td>
      Default state of output 1
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 1
    </td>
  </tr>
  <tr>
    <td>
      194
    </td>
    <td>
      193
    </td>
    <td>
      0x0C1
    </td>
    <td>
      Default state of output 2
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 2
    </td>
  </tr>
  <tr>
    <td>
      195
    </td>
    <td>
      194
    </td>
    <td>
      0x0C2
    </td>
    <td>
      Default state of output 3
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 3
    </td>
  </tr>
  <tr>
    <td>
      196
    </td>
    <td>
      195
    </td>
    <td>
      0x0C3
    </td>
    <td>
      Default state of output 4
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 4
    </td>
  </tr>
  <tr>
    <td>
      197
    </td>
    <td>
      196
    </td>
    <td>
      0x0C4
    </td>
    <td>
      Default state of output 5
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 5
    </td>
  </tr>
  <tr>
    <td>
      198
    </td>
    <td>
      197
    </td>
    <td>
      0x0C5
    </td>
    <td>
      Default state of output 6
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 6
    </td>
  </tr>
  <tr>
    <td>
      199
    </td>
    <td>
      198
    </td>
    <td>
      0x0C6
    </td>
    <td>
      Default state of output 7
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 7
    </td>
  </tr>
  <tr>
    <td>
      200
    </td>
    <td>
      199
    </td>
    <td>
      0x0C7
    </td>
    <td>
      Default state of output 8
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 8
    </td>
  </tr>
  <tr>
    <td>
      201
    </td>
    <td>
      200
    </td>
    <td>
      0x0C8
    </td>
    <td>
      Default state of output 9
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 9
    </td>
  </tr>
  <tr>
    <td>
      202
    </td>
    <td>
      201
    </td>
    <td>
      0x0C9
    </td>
    <td>
      Default state of output 10
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 10
    </td>
  </tr>
  <tr>
    <td>
      203
    </td>
    <td>
      202
    </td>
    <td>
      0x0CA
    </td>
    <td>
      Default state of output 11
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 11
    </td>
  </tr>
  <tr>
    <td>
      204
    </td>
    <td>
      203
    </td>
    <td>
      0x0CB
    </td>
    <td>
      Default state of output 12
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 12
    </td>
  </tr>
  <tr>
    <td>
      205
    </td>
    <td>
      204
    </td>
    <td>
      0x0CC
    </td>
    <td>
      Default state of output 13
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 13
    </td>
  </tr>
  <tr>
    <td>
      206
    </td>
    <td>
      205
    </td>
    <td>
      0x0CD
    </td>
    <td>
      Default state of output 14
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 14
    </td>
  </tr>
  <tr>
    <td>
      207
    </td>
    <td>
      206
    </td>
    <td>
      0x0CE
    </td>
    <td>
      Default state of output 15
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 15
    </td>
  </tr>
  <tr>
    <td>
      208
    </td>
    <td>
      207
    </td>
    <td>
      0x0CF
    </td>
    <td>
      Default state of output 16
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 16
    </td>
  </tr>
  <tr>
    <td>
      817
    </td>
    <td>
      816
    </td>
    <td>
      0x330
    </td>
    <td>
      Output 1
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 1 state
    </td>
  </tr>
  <tr>
    <td>
      818
    </td>
    <td>
      817
    </td>
    <td>
      0x331
    </td>
    <td>
      Output 2
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 2 state
    </td>
  </tr>
  <tr>
    <td>
      819
    </td>
    <td>
      818
    </td>
    <td>
      0x332
    </td>
    <td>
      Output 3
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 3 state
    </td>
  </tr>
  <tr>
    <td>
      820
    </td>
    <td>
      819
    </td>
    <td>
      0x333
    </td>
    <td>
      Output 4
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 4 state
    </td>
  </tr>
  <tr>
    <td>
      821
    </td>
    <td>
      820
    </td>
    <td>
      0x334
    </td>
    <td>
      Output 5
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 5 state
    </td>
  </tr>
  <tr>
    <td>
      822
    </td>
    <td>
      821
    </td>
    <td>
      0x335
    </td>
    <td>
      Output 6
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 6 state
    </td>
  </tr>
  <tr>
    <td>
      823
    </td>
    <td>
      822
    </td>
    <td>
      0x336
    </td>
    <td>
      Output 7
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 7 state
    </td>
  </tr>
  <tr>
    <td>
      824
    </td>
    <td>
      823
    </td>
    <td>
      0x337
    </td>
    <td>
      Output 8
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 8 state
    </td>
  </tr>
  <tr>
    <td>
      825
    </td>
    <td>
      824
    </td>
    <td>
      0x338
    </td>
    <td>
      Output 9
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 9 state
    </td>
  </tr>
  <tr>
    <td>
      826
    </td>
    <td>
      825
    </td>
    <td>
      0x339
    </td>
    <td>
      Output 10
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 10 state
    </td>
  </tr>
  <tr>
    <td>
      827
    </td>
    <td>
      826
    </td>
    <td>
      0x33A
    </td>
    <td>
      Output 11
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 11 state
    </td>
  </tr>
  <tr>
    <td>
      828
    </td>
    <td>
      827
    </td>
    <td>
      0x33B
    </td>
    <td>
      Output 12
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 12 state
    </td>
  </tr>
  <tr>
    <td>
      829
    </td>
    <td>
      828
    </td>
    <td>
      0x33C
    </td>
    <td>
      Output 13
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 13 state
    </td>
  </tr>
  <tr>
    <td>
      830
    </td>
    <td>
      829
    </td>
    <td>
      0x33D
    </td>
    <td>
      Output 14
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 14 state
    </td>
  </tr>
  <tr>
    <td>
      831
    </td>
    <td>
      830
    </td>
    <td>
      0x33E
    </td>
    <td>
      Output 15
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 15 state
    </td>
  </tr>
  <tr>
    <td>
      832
    </td>
    <td>
      831
    </td>
    <td>
      0x33F
    </td>
    <td>
      Output 16
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 16 state
    </td>
  </tr>
</table>

## Configuration software

Modbus Configurator is software that is designed to set the module registers responsible for communication over Modbus network as well as to read and write the current value of other registers of the module. This program can be a convenient way to test the system as well as to observe real-time changes in the registers.
Communication with the module is done via the USB cable. The module does not require any drivers.

Configurator is a universal program, whereby it is possible to configure all available modules.

![](images/MO-DO16-ST/configurator.png) {.img-responsive}

