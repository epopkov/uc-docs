# UC MO-AI1-MI {.header}

[UC MO-AI1-MI Expansion Module](http://en.unitecontrol.com/MO-AI1-MI.html), 1 analog input, 1 digital output Modbus RTU (RS485)

![](images/MO-AI1-MI/device.png) {.img-responsive}

## User Manual

## Safety Rules

* Before first use, refer to this manual
* Before first use, make sure that all cables are connected properly
* Please ensure proper working conditions, according to the device specifications (e.g. supply voltage, temperature, maximum power consumption)
* Before making any modifications to wiring connections, turn off the power supply


## Module Features

### Purpose and description of the module

- Name: Expansion Module, 1 analog input, Modbus RTU (RS485), ASCII
- art. MO-AI1-MI
- Module MO-AI1 works in Slave mode.
- Module MO-AI1 has 1 input to voltage measurement and 1 input for current measurement (both inputs can be used in the same time). In addition, the module is equipped with 1 configurable digital output (PNP or NPN type).
- This module is connected to the RS485 bus with twisted-pair wire. Communication is via Modbus RTU or Modbus ASCII.
- The module is designed for mounting on a DIN rail in accordance with DIN EN 5002.
- The module is equipped with a set of LEDs are used to indicate the status of inputs and outputs useful for diagnostics purposes and helping to find errors.
- Module configuration is done via USB by using a dedicated computer program. You can also change the parameters using the ModBus protocol.

### Techical Specifications

<table>
  <tr>
      <td>
          Power Supply
      </td>
      <td>
          Voltage
      </td>
      <td>
          10-36 V DC±20%
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Maximum Current
      </td>
      <td>
          70 mA @ 12 V / 38 mA @ 24 V
      </td>
  </tr>
  <tr>
      <td>
          Connection Speed
      </td>
      <td>
      </td>
      <td>
          2400 - 115200 bps
      </td>
  </tr>
  <tr>
      <td>
          Inputs
      </td>
      <td>
          No of inputs
      </td>
      <td>
          2
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Voltage input
      </td>
      <td>
          -10 V - 10 V
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Current input
      </td>
      <td>
          -20 mA - 20 mA
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Measurement resolution
      </td>
      <td>
          16 bits
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          ADC processing time
      </td>
      <td>
          70 ms / channel
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Voltage measurement error
      </td>
      <td>
          Max ±1.7%
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Current measurement error
      </td>
      <td>
          Max ±0.1%
      </td>
  </tr>
  <tr>
      <td>
          Digital outputs
      </td>
      <td>
          Maximum current and voltage
      </td>
      <td>
          250 mA / 50 V
      </td>
  </tr>
  <tr>
      <td>
          Temperature
      </td>
      <td>
          Work
      </td>
      <td>
          -20°C ÷ +65°C
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Storage
      </td>
      <td>
          -40°C ÷ +85°C
      </td>
  </tr>
  <tr>
      <td>
          Connectors
      </td>
      <td>
          Power Supply
      </td>
      <td>
          2 pin
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Communication
      </td>
      <td>
          3 pin
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Inputs &amp; Outputs
      </td>
      <td>
          2 x 3 pin
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Configuration
      </td>
      <td>
          MiniUSB
      </td>
  </tr>
  <tr>
      <td>
          Dimensions
      </td>
      <td>
          Height
      </td>
      <td>
          90 mm
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Length
      </td>
      <td>
          56 mm
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Width
      </td>
      <td>
          17 mm
      </td>
  </tr>
  <tr>
      <td>
          Interface
      </td>
      <td>
          RS485
      </td>
      <td>
          Up to 128 devices
      </td>
  </tr>
</table>

### Dimensions of the product

Look at dimensions of the module are shown below. The module is mounted directly to the rail in the DIN industry standart.

![](images/MO-AI1-MI/overview.png) {.img-responsive}

## Communication configuration

### Grounding and shielding

In most cases, IO (Inputs/Outputs) modules will be installed in an enclosure along with other devices which generate electromagnetic radiation. Examples of these devices are relays and contactors, transformers, motor controllers etc. This electromagnetic radiation can induce electrical noise into both power and signal lines, as well as direct radiation into the module causing negative effects on the system. Appropriate grounding, shielding and other protective steps should be taken at the installation stage to prevent these effects. These protective steps include control cabinet grounding, module grounding, cable shield grounding, protective elements for electromagnetic switching devices, correct wiring as well as consideration of cable types and their cross sections.

### Network Termination

Transmission line effects often present a problem on data communication networks. These problems include reflections and signal attenuation.

To eliminate the presence of reflections from the end of the cable, the cable must be terminated at both ends with a resistor across the line equal to its characteristic impedance. Both ends must be terminated since the direction of propagation is bidirectional. In the case of an RS485 twisted pair cable this termination is typically 120 Ω.

### Types of Modbus Registers

There are 4 types of variables available in the module

<table>
  <tr>
      <th>
          <b>Type</b>
      </th>
      <th>
          <b>Beginning address</b>
      </th>
      <th>
          <b>Variable</b>
      </th>
      <th>
          <b>Access</b>
      </th>
      <th>
          <b>Modbus Command</b>
      </th>
  </tr>
  <tr>
      <td>
          1
      </td>
      <td>
          00001
      </td>
      <td>
          Coils
      </td>
      <td>
          Read &amp; Write
      </td>
      <td>
          1, 5, 15
      </td>
  </tr>
  <tr>
      <td>
          2
      </td>
      <td>
          10001
      </td>
      <td>
          Discrete Inputs
      </td>
      <td>
          Read
      </td>
      <td>
          2
      </td>
  </tr>
  <tr>
      <td>
          3
      </td>
      <td>
          30001
      </td>
      <td>
          Input Registers
      </td>
      <td>
          Registered Read
      </td>
      <td>
          3
      </td>
  </tr>
  <tr>
      <td>
          4
      </td>
      <td>
          40001
      </td>
      <td>
          Holding Registers
      </td>
      <td>
          Read & Write
      </td>
      <td>
          4, 6, 16
      </td>
  </tr>
</table>

### Communication settings

The data stored in the modules memory are in 16-bit registers. Access to registers is via MODBUS RTU or MODBUS ASCII.

#### Default settings

<table>
  <tr>
      <td>
          Baud rate, bps
      </td>
      <td>
          19200
      </td>
  </tr>
  <tr>
      <td>
          Adress
      </td>
      <td>
          1
      </td>
  </tr>
  <tr>
      <td>
          Parity
      </td>
      <td>
          No
      </td>
  </tr>
  <tr>
      <td>
          Data bits
      </td>
      <td>
          8
      </td>
  </tr>
  <tr>
      <td>
          Stop bits
      </td>
      <td>
          1
      </td>
  </tr>
  <tr>
      <td>
          Reply Delay, ms
      </td>
      <td>
          0
      </td>
  </tr>
  <tr>
      <td>
          Modbus Type
      </td>
      <td>
          RTU
      </td>
  </tr>
</table>

#### Configuration registers

<table>
  <thead>
      <tr>
          <th>
              Address
          </th>
          <th>
              Name
          </th>
          <th>
              Values
          </th>
      </tr>
  </thead>
  <tbody>
      <tr>
          <td>
              40002
          </td>
          <td>
              Adress
          </td>
          <td>
              From 0 to 255
          </td>
      </tr>
	  <tr>
          <td>
              40003
          </td>
          <td>
              Baud rate
          </td>
          <td>
              0 - 2400
              <br>1 - 4800
              <br>2 - 9600
              <br>3 - 19200
              <br>4 - 38400
              <br>5 - 57600
              <br>6 - 115200
              <br>other – value * 10
          </td>
      </tr>
      <tr>
          <td>
              40005
          </td>
          <td>
              Parity
          </td>
          <td>
              0 - none
              <br>1 - odd
              <br>2 - even
              <br>3 - always 1
              <br>4 - вalways 0
          </td>
      </tr>
      <tr>
          <td>
              40004
          </td>
          <td>
              Stop Bits LSB
          </td>
          <td>
              1 - one stop bit
              <br>2 - two stop bits
          </td>
      </tr>
      <tr>
          <td>
              40004
          </td>
          <td>
              Data Bits MSB
          </td>
          <td>
              7 - 7 data bits
              <br>8 - 8 data bits
          </td>
      </tr>
      <tr>
          <td>
              40006
          </td>
          <td>
              Response delay
          </td>
          <td>
              Time in ms
          </td>
      </tr>
      <tr>
          <td>
              40007
          </td>
          <td>
              Modbus Mode
          </td>
          <td>
              0 - RTU
              <br>1 - ASCII
          </td>
      </tr>
  </tbody>
</table>

## Indicators

![](images/MO-AI1-MI/leds.gif) {.img-responsive}

<table>
  <tr>
      <th>
          Indicator
      </th>
      <th>
          Description
      </th>
  </tr>
  <tr>
      <td>
          Power (ON)
      </td>
      <td>
          LED indicates that the module is correctly powered.
      </td>
  </tr>
  <tr>
      <td>
          TX
      </td>
      <td>
          The LED lights up when the unit received the correct packet and sends the answer.
      </td>
  </tr>
  <tr>
      <td>
          AI<sub>i</sub>, AI<sub>v</sub>
      </td>
      <td>
          LED indicates that the signal to input is connected and is different from 0.
      </td>
  </tr>
  <tr>
      <td>
          DO
      </td>
      <td>
          LED indicates that the output is on.
      </td>
  </tr>
</table>

## Block diagram

![](images/MO-AI1-MI/scheme.png) {.img-responsive}

## Module Connection

![](images/MO-AI1-MI/connection.gif) {.img-responsive}

## Modules Registers

### Registered access

<table>
  <thead>
      <tr>
          <th>
              Modbus
          </th>
          <th>
              DEC
          </th>
          <th>
              HEX
          </th>
          <th>
              Register name
          </th>
          <th>
              Acess
          </th>
          <th>
              Description
          </th>
      </tr>
  </thead>
  <tbody>
      <tr>
          <td>
              30001
          </td>
          <td>
              0
          </td>
          <td>
              0x00
          </td>
          <td>
              Version/Type
          </td>
          <td>
              Read
          </td>
          <td>
              Version and Type of the device
          </td>
      </tr>
      <tr>
          <td>
              30002
          </td>
          <td>
              1
          </td>
          <td>
              0x01
          </td>
          <td>
              Adress
          </td>
          <td>
              Read
          </td>
          <td>
              Module Address
          </td>
      </tr>
      <tr>
          <td>
              40003
          </td>
          <td>
              2
          </td>
          <td>
              0x02
          </td>
          <td>
              Baud rate
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              RS485 baud rate
          </td>
      </tr>
      <tr>
          <td>
              40004
          </td>
          <td>
              3
          </td>
          <td>
              0x03
          </td>
          <td>
              Stop Bits &amp; Data Bits
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              No of Stop bits & Data Bits (see 3.4.2)
          </td>
      </tr>
      <tr>
          <td>
              40005
          </td>
          <td>
              4
          </td>
          <td>
              0x04
          </td>
          <td>
              Parity
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              Parity bit
          </td>
      </tr>
      <tr>
          <td>
              40006
          </td>
          <td>
              5
          </td>
          <td>
              0x05
          </td>
          <td>
              Response Delay
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              Response delay in ms
          </td>
      </tr>
      <tr>
          <td>
              40007
          </td>
          <td>
              6
          </td>
          <td>
              0x06
          </td>
          <td>
              Modbus Mode
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              Modbus Mode (ASCII or RTU)
          </td>
      </tr>
      <tr>
          <td>
              40010
          </td>
          <td>
              9
          </td>
          <td>
              0x09
          </td>
          <td>
              Filter
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              Measurement filtering, value from 1 to 10
          </td>
      </tr>
      <tr>
          <td>
              40033
          </td>
          <td>
              32
          </td>
          <td>
              0x20
          </td>
          <td>
              Received packets MSB
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              No of received packets
          </td>
      </tr>
      <tr>
          <td>
              40034
          </td>
          <td>
              33
          </td>
          <td>
              0x21
          </td>
          <td>
              Received packets LSB
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              No of received packets
          </td>
      </tr>
      <tr>
          <td>
              40035
          </td>
          <td>
              34
          </td>
          <td>
              0x22
          </td>
          <td>
              Incorrect packets MSB
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              No of received packets with error
          </td>
      </tr>
      <tr>
          <td>
              40036
          </td>
          <td>
              35
          </td>
          <td>
              0x23
          </td>
          <td>
              Incorrect packets LSB
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              No of received packets with error
          </td>
      </tr>
      <tr>
          <td>
              40037
          </td>
          <td>
              36
          </td>
          <td>
              0x24
          </td>
          <td>
              Sent packets MSB
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              No of sent packets
          </td>
      </tr>
      <tr>
          <td>
              40038
          </td>
          <td>
              37
          </td>
          <td>
              0x25
          </td>
          <td>
              Sent packets LSB
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              No of sent packets
          </td>
      </tr>
      <tr>
          <td>
              30051
          </td>
          <td>
              50
          </td>
          <td>
              0x32
          </td>
          <td>
              Inputs
          </td>
          <td>
              Read
          </td>
          <td>
              Connected inputs Bit in high state → signal is connected
          </td>
      </tr>
      <tr>
          <td>
              40052
          </td>
          <td>
              51
          </td>
          <td>
              0x33
          </td>
          <td>
              Outputs
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              Alarms state bit no. 3 digital output
          </td>
      </tr>
      <tr>
          <td>
              30054
          </td>
          <td>
              53
          </td>
          <td>
              0x35
          </td>
          <td>
              Voltage
          </td>
          <td>
              Read
          </td>
          <td>
              Voltage in μV
          </td>
      </tr>
      <tr>
          <td>
              30055
          </td>
          <td>
              54
          </td>
          <td>
              0x36
          </td>
          <td>
              Current
          </td>
          <td>
              Read
          </td>
          <td>
              Current in μA or ‰
          </td>
      </tr>
      <tr>
          <td>
              30056
          </td>
          <td>
              55
          </td>
          <td>
              0x37
          </td>
          <td>
              Alarm – max voltage
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              Maximum value of voltage excess which causes set bit no 1 in the register 40052
          </td>
      </tr>
      <tr>
          <td>
              30057
          </td>
          <td>
              56
          </td>
          <td>
              0x38
          </td>
          <td>
              Alarm – min voltage
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              Minimum value of voltage. If voltage drops below this voltage bit no 1 in the register 40052 is set.
          </td>
      </tr>
      <tr>
          <td>
              30058
          </td>
          <td>
              57
          </td>
          <td>
              0x39
          </td>
          <td>
              Alarm – max current
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              Maximum value of current excess which causes set bit no 1 in the register 40052
          </td>
      </tr>
      <tr>
          <td>
              30059
          </td>
          <td>
              58
          </td>
          <td>
              0x3A
          </td>
          <td>
              Alarm – min current
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              Minimum value of current. If current drops below this voltage bit no 1 in the register 40052 is set.
          </td>
      </tr>
      <tr>
          <td>
              30060
          </td>
          <td>
              59
          </td>
          <td>
              0x3B
          </td>
          <td>
              Voltage alarm configuration
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              Alarms configuration: 0 – alarms state depends on actual values; 1 – alarms state need to clear by master
          </td>
      </tr>
      <tr>
          <td>
              30061
          </td>
          <td>
              60
          </td>
          <td>
              0x3C
          </td>
          <td>
              Current alarm configuration
          </td>
          <td>
              Read &amp; Write
          </td>
      </tr>
      <tr>
          <td>
              30062
          </td>
          <td>
              61
          </td>
          <td>
              0x3D
          </td>
          <td>
              Voltage input configuration
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              0 — OFF
              <br>1 — 0..10 V
              <br>2 — -10..10 V
              <br>3 — 0..1 V
              <br>4 — -1..1 V
          </td>
      </tr>
      <tr>
          <td>
              40063
          </td>
          <td>
              62
          </td>
          <td>
              0x3E
          </td>
          <td>
              Current input configuration
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              0 — OFF
              <br>1 — 0..20 μА (in μA)
              <br>2 — 4..20 μА (in ‰)
              <br>3 — -20..20 μА (in μA)
          </td>
      </tr>
      <tr>
          <td>
              40064
          </td>
          <td>
              63
          </td>
          <td>
              0x3F
          </td>
          <td>
              Digital output configuration
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              Digital output configuration:
              <br>0 – output controlled by master;
              <br>1 – output state depends voltage;
              <br>2 – output state depends current;
              <br>+256 – output set if value is greater than alarm value (40065 register) („cooling”);
              <br>+512 – output set if value is below than alarm value (40065 register) („warming”)
          </td>
      </tr>
      <tr>
          <td>
              40065
          </td>
          <td>
              64
          </td>
          <td>
              0x40
          </td>
          <td>
              Alarm value
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              Alarm value
          </td>
      </tr>
      <tr>
          <td>
              40066
          </td>
          <td>
              65
          </td>
          <td>
              0x41
          </td>
          <td>
              Alarm hysteresis
          </td>
          <td>
              Read &amp; Write
          </td>
          <td>
              Hysteresis for alarm
          </td>
      </tr>
  </tbody>
</table>

### Bit access

<table>
  <tr>
      <th>
          Modbus Address
      </th>
      <th>
          DEC
      </th>
      <th>
          HEX
      </th>
      <th>
          Register name
      </th>
      <th>
          Access
      </th>
      <th>
          Description
      </th>
  </tr>
  <tr>
      <td>
          801
      </td>
      <td>
          800
      </td>
      <td>
          0x320
      </td>
      <td>
          Voltage input
      </td>
      <td>
          Read
      </td>
      <td>
          Voltage input state
      </td>
  </tr>
  <tr>
      <td>
          802
      </td>
      <td>
          801
      </td>
      <td>
          0x321
      </td>
      <td>
          Current input
      </td>
      <td>
          Read
      </td>
      <td>
          Current input state
      </td>
  </tr>
  <tr>
      <td>
          817
      </td>
      <td>
          816
      </td>
      <td>
          0x330
      </td>
      <td>
          Voltage alarm
      </td>
      <td>
          Read &amp; Write
      </td>
      <td>
          Voltage alarm state
      </td>
  </tr>
  <tr>
      <td>
          818
      </td>
      <td>
          817
      </td>
      <td>
          0x331
      </td>
      <td>
          Current alarm
      </td>
      <td>
          Read &amp; Write
      </td>
      <td>
          Current alarm state
      </td>
  </tr>
  <tr>
      <td>
          819
      </td>
      <td>
          818
      </td>
      <td>
          0x332
      </td>
      <td>
          Digital output
      </td>
      <td>
          Read &amp; Write
      </td>
      <td>
          Digital output state
      </td>
  </tr>
</table>

## Configuration software

Modbus Configurator is software that is designed to set the module registers responsible for communication over Modbus network as well as to read and write the current value of other registers of the module. This program can be a convenient way to test the system as well as to observe real-time changes in the registers.
Communication with the module is done via the USB cable. The module does not require any drivers.

Configurator is a universal program, whereby it is possible to configure all available modules.

![](images/MO-AI1-MI/configurator.png) {.img-responsive}