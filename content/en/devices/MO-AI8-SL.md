# UC MO-AI8-SL {.header}

[UC MO-AI8-SL Expansion Module](http://en.unitecontrol.com/MO-AI8-SL.html), 8 analog inputs Modbus RTU (RS485)

![](images/MO-AI8-SL/device.png) {.img-responsive}

## User Manual

## Safety Rules

* Before first use, refer to this manual
* Before first use, make sure that all cables are connected properly
* Please ensure proper working conditions, according to the device specifications (e.g. supply voltage, temperature, maximum power consumption)
* Before making any modifications to wiring connections, turn off the power supply

## Module Features

### Purpose and description of the module

- Name: Expansion Module, 8 analog inputs, Modbus RTU (RS485), ASCII
- Art. MO-AI8-SL
- Module MO-AI8 works in Slave mode.
- The device has a set of 8 analog input measurements. In addition, the module is equipped with 2 configurable digital outputs.
- This module is connected to the RS485 bus with twisted-pair wire. Communication is via ModBus RTU or ModBus ASCII.
- The module is designed for mounting on a DIN rail in accordance with DIN EN 5002.
- The module is equipped with a set of LEDs used to indicate the status of inputs and outputs useful for diagnostic purposes and helping to find errors.
- Module configuration is done via USB by using a dedicated computer program. You can also change the parameters using the ModBus protocol.

### Technical Specifications

<table>
  <tr>
    <td>
      Power Supply
    </td>
    <td>
      Voltage
    </td>
    <td>
      12-24 V DC±20%
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Maximum Current
    </td>
    <td>
      120 mA @ 12 В / 100 mA @ 24 V
    </td>
  </tr>
  <tr>
    <td>
      Connection Speed
    </td>
    <td>
    </td>
    <td>
      2400 - 115200 bps
    </td>
  </tr>
  <tr>
    <td>
      Inputs
    </td>
    <td>
      No of inputs
    </td>
    <td>
      8
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Voltage input
    </td>
    <td>
      0-10 V (resolution 1,5 mV)
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Voltage input
    </td>
    <td>
      -10-10 В (resolution 1,5 mV)
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Voltage input
    </td>
    <td>
      0-1 В ( resolution 0,1875 mV)
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Voltage input
    </td>
    <td>
      -1-1 В (resolution 0,1875 mV)
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Current input
    </td>
    <td>
      4-20 mA ( resolution 3,75 μA)
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Current input
    </td>
    <td>
      0-20 mA ( resolution 3,75 μA)
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Current input
    </td>
    <td>
      -20-20 mA (resolution 3,75 μA)
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Measurement resolution
    </td>
    <td>
      14 bits
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      ADC processing time
    </td>
    <td>
      16ms / channel
    </td>
  </tr>
  <tr>
    <td>
      Digital outputs
    </td>
    <td>
      Maximum current and voltage
    </td>
    <td>
      500mA / 55V
    </td>
  </tr>
  <tr>
    <td>
      Temperature
    </td>
    <td>
      Work
    </td>
    <td>
      -20°C ÷ +65°C
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Storage
    </td>
    <td>
      -40°C ÷ +85°C
    </td>
  </tr>
  <tr>
    <td>
      Connectors
    </td>
    <td>
      Power Supply
    </td>
    <td>
      2 pin
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Communication
    </td>
    <td>
      3 pin
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Inputs &amp; Outputs
    </td>
    <td>
      2 x 10 pin
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Configuration
    </td>
    <td>
      MiniUSB
    </td>
  </tr>
  <tr>
    <td>
      Dimensions
    </td>
    <td>
      Height
    </td>
    <td>
      120 mm
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Length
    </td>
    <td>
      101 mm
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Width
    </td>
    <td>
      22,5 mm
    </td>
  </tr>
  <tr>
    <td>
      Interface
    </td>
    <td>
      RS485
    </td>
    <td>
      Up to 128 devices
    </td>
  </tr>
</table>

### Dimensions of the product

Look and dimensions of the module are shown below. The module is mounted directly to the rail in the DIN industry standard. Power connectors, communication and IOs are at the bottom and top of the module. USB connector configuration and indicators located on the front of the module.

![](images/MO-AI8-SL/overview.png) {.img-responsive}

## Communication configuration

### Grounding and shielding

In most cases, IO (Inputs/Outputs) modules will be installed in an enclosure along with other devices which generate electromagnetic radiation. Examples of these devices are relays and contactors, transformers, motor controllers etc. This electromagnetic radiation can induce electrical noise into both power and signal lines, as well as direct radiation into the module causing negative effects on the system. Appropriate grounding, shielding and other protective steps should be taken at the installation stage to prevent these effects. These protective steps include control cabinet grounding, module grounding, cable shield grounding, protective elements for electromagnetic switching devices, correct wiring as well as consideration of cable types and their cross sections.

### Network Termination

Transmission line effects often present a problem on data communication networks. These problems include reflections and signal attenuation.

To eliminate the presence of reflections from the end of the cable, the cable must be terminated at both ends with a resistor across the line equal to its characteristic impedance. Both ends must be terminated since the direction of propagation is bidirectional. In the case of an RS485 twisted pair cable this termination is typically 120 Ω.

### Setting Module Address in RS485 Modbus Network

The following table shows how to set switch to determine the address of the module. The module address is set with the switches in the range of 0 to 31. Addresses From 32 to 255 can by set via RS485 or USB.

<table>
  <tbody>
    <tr>
      <td bgcolor="#cfe2f3">Adr</td>
      <td>SW5</td>
      <td>SW4</td>
      <td>SW3</td>
      <td>SW2</td>
      <td>SW1</td>
      <td bgcolor="#cfe2f3">Adr</td>
      <td>SW5</td>
      <td>SW4</td>
      <td>SW3</td>
      <td>SW2</td>
      <td>SW1</td>
      <td bgcolor="#cfe2f3">Adr</td>
      <td>SW5</td>
      <td>SW4</td>
      <td>SW3</td>
      <td>SW2</td>
      <td>SW1</td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">0</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#cfe2f3">11</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#cfe2f3">22</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">1</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#cfe2f3">12</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#cfe2f3">23</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">2</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#cfe2f3">13</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#cfe2f3">24</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">3</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#cfe2f3">14</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#cfe2f3">25</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">4</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#cfe2f3">15</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#cfe2f3">26</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">5</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#cfe2f3">16</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#cfe2f3">27</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">6</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#cfe2f3">17</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#cfe2f3">28</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">7</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#cfe2f3">18</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#cfe2f3">29</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">8</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#cfe2f3">19</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#cfe2f3">30</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">9</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#cfe2f3">20</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#cfe2f3">31</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#b6d7a8">ON</td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">10</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#cfe2f3">21</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td bgcolor="#f4cccc">OFF</td>
      <td bgcolor="#b6d7a8">ON</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

### Types of Modbus Registers

There are 4 types of variables available in the module

<table>
  <tr>
    <th>
      <b>Type</b>
    </th>
    <th>
      <b>Beginning address</b>
    </th>
    <th>
      <b>Variable</b>
    </th>
    <th>
      <b>Access</b>
    </th>
    <th>
      <b>Modbus Command</b>
    </th>
  </tr>
  <tr>
    <td>
      1
    </td>
    <td>
      00001
    </td>
    <td>
      Digital Outputs
    </td>
    <td>
      Bit Read &amp; Write
    </td>
    <td>
      1, 5, 15
    </td>
  </tr>
  <tr>
    <td>
      2
    </td>
    <td>
      10001
    </td>
    <td>
      Digital Inputs
    </td>
    <td>
      Bit Read
    </td>
    <td>
      2
    </td>
  </tr>
  <tr>
    <td>
      3
    </td>
    <td>
      30001
    </td>
    <td>
      Input Registers
    </td>
    <td>
      Registered Read
    </td>
    <td>
      3
    </td>
  </tr>
  <tr>
    <td>
      4
    </td>
    <td>
      40001
    </td>
    <td>
      Output Registers
    </td>
    <td>
      Registered Read &amp; Write
    </td>
    <td>
      4, 6, 16
    </td>
  </tr>
</table>

### Communication settings

The data stored in the modules memory are in 16-bit registers. Access to registers is via MODBUS RTU or MODBUS ASCII.

#### Default settings

You can restore the default configuration by the switch SW6 (see - Restore the default configuration)

<table>
  <tr>
    <td>
      Baud rate, bps
    </td>
    <td>
      19200
    </td>
  </tr>
  <tr>
    <td>
      Parity
    </td>
    <td>
      No
    </td>
  </tr>
  <tr>
    <td>
      Data bits
    </td>
    <td>
      1
    </td>
  </tr>
  <tr>
    <td>
      Stop bits
    </td>
    <td>
      1
    </td>
  </tr>
  <tr>
    <td>
      Reply Delay, ms
    </td>
    <td>
      0
    </td>
  </tr>
  <tr>
    <td>
      Modbus Type
    </td>
    <td>
      RTU
    </td>
  </tr>
</table>

#### Restore the default configuration

To restore the default configuration:
* turn off the power
* turn on the switch SW6
* turn on the power
* when power and communication LED flash turn off the switch SW6

Caution! After restoring the default configuration all values stored in the registers will be cleared as well.

#### Configuration registers

<table>
  <tr>
    <th>
      Address
    </th>
    <th>
      Name
    </th>
    <th>
      Values
    </th>
  </tr>
  <tr>
    <td>
      40003
    </td>
    <td>
      Baud rate
    </td>
    <td>
      0 - 2400
      <br>1 - 4800
      <br>2 - 9600
      <br>3 - 19200
      <br>4 - 38400
      <br>5 - 57600
      <br>6 - 115200
      <br>other – value * 10
    </td>
  </tr>
  <tr>
    <td>
      40005
    </td>
    <td>
      Parity
    </td>
    <td>
      0 - none
      <br>1 - odd
      <br>2 - even
      <br>3 - always 1
      <br>4 - always 0
    </td>
  </tr>
  <tr>
    <td>
      40004
    </td>
    <td>
      Stop Bits LSB
    </td>
    <td>
      1 - one stop bit
      <br>2 - 2 two stop bits
    </td>
  </tr>
  <tr>
    <td>
      40004
    </td>
    <td>
      Data Bits MSB
    </td>
    <td>
      7 - 7 data bits
      <br>8 - 8 data bits
    </td>
  </tr>
  <tr>
    <td>
      40006
    </td>
    <td>
      Response delay
    </td>
    <td>
      Time in ms
    </td>
  </tr>
  <tr>
    <td>
      40007
    </td>
    <td>
      Modbus Mode
    </td>
    <td>
      0 - RTU
      <br>1 - ASCII
    </td>
  </tr>
</table>

## Indicators

![](images/MO-AI8-SL/leds.jpg) {.img-responsive}

<table>
  <tr>
    <th>
      Indicator
    </th>
    <th>
      Description
    </th>
  </tr>
  <tr>
    <td>
      Power supply
    </td>
    <td>
      LED indicates that the module is correctly powered.
    </td>
  </tr>
  <tr>
    <td>
      Communication
    </td>
    <td>
      The LED lights up when the unit received the correct packet and sends the answer.
    </td>
  </tr>
  <tr>
    <td>
      Inputs state
    </td>
    <td>
      LED indicates that the signal to input is connected.
    </td>
  </tr>
  <tr>
    <td>
      Outputs state
    </td>
    <td>
      LED indicates that the output is on.
    </td>
  </tr>
</table>

## Module Connection

![](images/MO-AI8-SL/connection.jpg) {.img-responsive}

## Selecting the input mode

Each input can be used to measure the voltage (the default) or current. To change the operating mode in addition to configuration changes by using the program, also set the jumpers inside the module as shown below.

<table>
  <tr>
    <th colspan="2">
      Jumper
    </th>
    <th>
      Description
    </th>
  </tr>
  <tr>
    <td>
      off
    </td>
    <td>
      ![](images/MO-AI8-SL/jump0.png) {.img-responsive}
    </td>
    <td>
      Voltage measurement
    </td>
  </tr>
  <tr>
    <td>
      shorted
    </td>
    <td>
      ![](images/MO-AI8-SL/jump1.png) {.img-responsive}
    </td>
    <td>
      Current measurement
    </td>
  </tr>
</table>

## Opening the enclosure

![](images/MO-AI8-SL/disassemble.jpg) {.img-responsive}

1.  Remove the terminal, pressing and shifting towards the centre of enclosure. Pay attention to the terminal which is under spring.
2.  Divide the enclosure gently opening terminals (with a small screwdriver), using picture.

## Switches

![](images/MO-AI8-SL/switch.png) {.img-responsive}

<table>
  <tr>
    <th>
      Switch
    </th>
    <th>
      Function
    </th>
    <th>
      Description
    </th>
  </tr>
  <tr>
    <td>
      1
    </td>
    <td>
      Module address +1
    </td>
    <td rowspan="5">
      Setting module address from 0 to 31
    </td>
  </tr>
  <tr>
    <td>
      2
    </td>
    <td>
      Module address +2
    </td>
  </tr>
  <tr>
    <td>
      3
    </td>
    <td>
      Module address +4
    </td>
  </tr>
  <tr>
    <td>
      4
    </td>
    <td>
      Module address +8
    </td>
  </tr>
  <tr>
    <td>
      5
    </td>
    <td>
      Module address +16
    </td>
  </tr>
  <tr>
    <td>
      6
    </td>
    <td>
      Restoring default settings
    </td>
    <td>
      Restoring default settings ( see 3.5.1 - Default settings and 3.5.2 - Restore the default configuration).
    </td>
  </tr>
</table>

## Modules Registers

### Registered access

<table>
  <tr>
    <th>
      Address
    </th>
    <th>
      Address
      <br>DEC
    </th>
    <th>
      Address
      <br>HEX
    </th>
    <th>
      Register Name
    </th>
    <th>
      Access
    </th>
    <th>
      Description
    </th>
  </tr>
  <tr>
    <td>
      30001
    </td>
    <td>
      0
    </td>
    <td>
      0x00
    </td>
    <td>
      Version/Type
    </td>
    <td>
      Read
    </td>
    <td>
      Version and Type of the device
    </td>
  </tr>
  <tr>
    <td>
      30002
    </td>
    <td>
      1
    </td>
    <td>
      0x01
    </td>
    <td>
      Switches
    </td>
    <td>
      Read
    </td>
    <td>
      Switches state
    </td>
  </tr>
  <tr>
    <td>
      40003
    </td>
    <td>
      2
    </td>
    <td>
      0x02
    </td>
    <td>
      Baud rate
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      RS485 baud rate
    </td>
  </tr>
  <tr>
    <td>
      40004
    </td>
    <td>
      3
    </td>
    <td>
      0x03
    </td>
    <td>
      Stop Bits &amp; Data Bits
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      No of Stop bits &amp;  Data Bits (see 3.5.3)
    </td>
  </tr>
  <tr>
    <td>
      40005
    </td>
    <td>
      4
    </td>
    <td>
      0x04
    </td>
    <td>
      Parity
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Parity bit
    </td>
  </tr>
  <tr>
    <td>
      40006
    </td>
    <td>
      5
    </td>
    <td>
      0x05
    </td>
    <td>
      Response Delay
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Response delay in ms
    </td>
  </tr>
  <tr>
    <td>
      40007
    </td>
    <td>
      6
    </td>
    <td>
      0x06
    </td>
    <td>
      Modbus Mode
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Modbus Mode (ASCII or RTU)
    </td>
  </tr>
  <tr>
    <td>
      40033
    </td>
    <td>
      32
    </td>
    <td>
      0x20
    </td>
    <td>
      Received packets MSB
    </td>
    <td>
      Read &amp; Write
    </td>
    <td rowspan="2">
      No of received packets
    </td>
  </tr>
  <tr>
    <td>
      40034
    </td>
    <td>
      33
    </td>
    <td>
      0x21
    </td>
    <td>
      Received packets LSB
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40035
    </td>
    <td>
      34
    </td>
    <td>
      0x22
    </td>
    <td>
      Incorrect packets MSB
    </td>
    <td>
      Read &amp; Write
    </td>
    <td rowspan="2">
      No of received packets with error
    </td>
  </tr>
  <tr>
    <td>
      40036
    </td>
    <td>
      35
    </td>
    <td>
      0x23
    </td>
    <td>
      Incorrect packets LSB
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40037
    </td>
    <td>
      36
    </td>
    <td>
      0x24
    </td>
    <td>
      Sent packets MSB
    </td>
    <td>
      Read &amp; Write
    </td>
    <td rowspan="2">
      No of sent packets
    </td>
  </tr>
  <tr>
    <td>
      40038
    </td>
    <td>
      37
    </td>
    <td>
      0x25
    </td>
    <td>
      Sent packets LSB
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      30051
    </td>
    <td>
      50
    </td>
    <td>
      0x32
    </td>
    <td>
      Inputs
    </td>
    <td>
      Read
    </td>
    <td>
      Connected inputs Bit in high state → signal is connected
    </td>
  </tr>
  <tr>
    <td>
      40052
    </td>
    <td>
      51
    </td>
    <td>
      0x33
    </td>
    <td>
      Outputs
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Alarms state bit 8 and 9 alarm outputs
    </td>
  </tr>
  <tr>
    <td>
      30053
    </td>
    <td>
      52
    </td>
    <td>
      0x34
    </td>
    <td>
      Analog 1
    </td>
    <td>
      Read
    </td>
    <td rowspan="8">
      Value of analog input: in mV for voltage inputs;
      <br>in рќ›ЌA for current inputs
    </td>
  </tr>
  <tr>
    <td>
      30054
    </td>
    <td>
      53
    </td>
    <td>
      0x35
    </td>
    <td>
      Analog 2
    </td>
    <td>
      Read
    </td>
  </tr>
  <tr>
    <td>
      30055
    </td>
    <td>
      54
    </td>
    <td>
      0x36
    </td>
    <td>
      Analog 3
    </td>
    <td>
      Read
    </td>
  </tr>
  <tr>
    <td>
      30056
    </td>
    <td>
      55
    </td>
    <td>
      0x37
    </td>
    <td>
      Analog 4
    </td>
    <td>
      Read
    </td>
  </tr>
  <tr>
    <td>
      30057
    </td>
    <td>
      56
    </td>
    <td>
      0x38
    </td>
    <td>
      Analog 5
    </td>
    <td>
      Read
    </td>
  </tr>
  <tr>
    <td>
      30058
    </td>
    <td>
      57
    </td>
    <td>
      0x39
    </td>
    <td>
      Analog 6
    </td>
    <td>
      Read
    </td>
  </tr>
  <tr>
    <td>
      30059
    </td>
    <td>
      58
    </td>
    <td>
      0x3A
    </td>
    <td>
      Analog 7
    </td>
    <td>
      Read
    </td>
  </tr>
  <tr>
    <td>
      30060
    </td>
    <td>
      59
    </td>
    <td>
      0x3B
    </td>
    <td>
      Analog 8
    </td>
    <td>
      Read
    </td>
  </tr>
  <tr>
    <td>
      30061
    </td>
    <td>
      60
    </td>
    <td>
      0x3C
    </td>
    <td>
      Value of 1. alarm input
    </td>
    <td>
      Read
    </td>
    <td rowspan="2">
      Current values of voltage / current for alarm inputs
    </td>
  </tr>
  <tr>
    <td>
      30062
    </td>
    <td>
      61
    </td>
    <td>
      0x3D
    </td>
    <td>
      Value of 2. alarm input
    </td>
    <td>
      Read
    </td>
  </tr>
  <tr>
    <td>
      40063
    </td>
    <td>
      62
    </td>
    <td>
      0x3E
    </td>
    <td>
      MAX alarm level 1
    </td>
    <td>
      Read &amp; Write
    </td>
    <td rowspan="8">
      If the analog signal exceeds this value the corresponding alarm flag is set
    </td>
  </tr>
  <tr>
    <td>
      40064
    </td>
    <td>
      63
    </td>
    <td>
      0x3F
    </td>
    <td>
      MAX alarm level 2
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40065
    </td>
    <td>
      64
    </td>
    <td>
      0x40
    </td>
    <td>
      MAX alarm level 3
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40066
    </td>
    <td>
      65
    </td>
    <td>
      0x41
    </td>
    <td>
      MAX alarm level 4
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40067
    </td>
    <td>
      66
    </td>
    <td>
      0x42
    </td>
    <td>
      MAX alarm level 5
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40068
    </td>
    <td>
      67
    </td>
    <td>
      0x43
    </td>
    <td>
      MAX alarm level 6
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40069
    </td>
    <td>
      68
    </td>
    <td>
      0x44
    </td>
    <td>
      MAX alarm level 7
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40070
    </td>
    <td>
      69
    </td>
    <td>
      0x45
    </td>
    <td>
      MAX alarm level 8
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40071
    </td>
    <td>
      70
    </td>
    <td>
      0x46
    </td>
    <td>
      MIN alarm level 1
    </td>
    <td>
      Read &amp; Write
    </td>
    <td rowspan="8">
      If the analog signal is below this value corresponding alarm flag is set.
    </td>
  </tr>
  <tr>
    <td>
      40072
    </td>
    <td>
      71
    </td>
    <td>
      0x47
    </td>
    <td>
      MIN alarm level 2
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40073
    </td>
    <td>
      72
    </td>
    <td>
      0x48
    </td>
    <td>
      MIN alarm level 3
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40074
    </td>
    <td>
      73
    </td>
    <td>
      0x49
    </td>
    <td>
      MIN alarm level 4
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40075
    </td>
    <td>
      74
    </td>
    <td>
      0x4A
    </td>
    <td>
      MIN alarm level 5
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40076
    </td>
    <td>
      75
    </td>
    <td>
      0x4B
    </td>
    <td>
      MIN alarm level 6
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40077
    </td>
    <td>
      76
    </td>
    <td>
      0x4C
    </td>
    <td>
      MIN alarm level 7
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40078
    </td>
    <td>
      77
    </td>
    <td>
      0x4D
    </td>
    <td>
      MIN alarm level 8
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40079
    </td>
    <td>
      78
    </td>
    <td>
      0x4E
    </td>
    <td>
      Alarm settings 1
    </td>
    <td>
      Read &amp; Write
    </td>
    <td rowspan="8">
      Alarm settings 1: 0 – alarm due to the current analog signal value; 1 – Remember the value of the alarm, until reset by the master via Modbus
    </td>
  </tr>
  <tr>
    <td>
      40080
    </td>
    <td>
      79
    </td>
    <td>
      0x4F
    </td>
    <td>
      Alarm settings 2
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40081
    </td>
    <td>
      80
    </td>
    <td>
      0x50
    </td>
    <td>
      Alarm settings 3
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40082
    </td>
    <td>
      81
    </td>
    <td>
      0x51
    </td>
    <td>
      Alarm settings 4
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40083
    </td>
    <td>
      82
    </td>
    <td>
      0x52
    </td>
    <td>
      Alarm settings 5
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40084
    </td>
    <td>
      83
    </td>
    <td>
      0x53
    </td>
    <td>
      Alarm settings 6
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40085
    </td>
    <td>
      84
    </td>
    <td>
      0x54
    </td>
    <td>
      Alarm settings 7
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40086
    </td>
    <td>
      85
    </td>
    <td>
      0x55
    </td>
    <td>
      Alarm settings 8
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40087
    </td>
    <td>
      86
    </td>
    <td>
      0x56
    </td>
    <td>
      Input 1 settings
    </td>
    <td>
      Read &amp; Write
    </td>
    <td rowspan="8">
      Analog input mode:0 — input disabled;
      <br>1 — voltage 0V to 10V;
      <br>2 — voltage -10V to 10V;
      <br>3 — voltage 0V to 1V;
      <br>4 — voltage -1V to 1V;
      <br>5 — current 4mA to 20mA;
      <br>6 — current 0mA to 20mA;
      <br>7 — current -20mA to 20mA
      <br>To change the input mode you must to set jumper inside the module (see 5 – Module Connection and 6 - Selecting the input mode)
    </td>
  </tr>
  <tr>
    <td>
      40088
    </td>
    <td>
      87
    </td>
    <td>
      0x57
    </td>
    <td>
      Input 2 settings
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40089
    </td>
    <td>
      88
    </td>
    <td>
      0x58
    </td>
    <td>
      Input 3 settings
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40090
    </td>
    <td>
      89
    </td>
    <td>
      0x59
    </td>
    <td>
      Input 4 settings
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40091
    </td>
    <td>
      90
    </td>
    <td>
      0x5A
    </td>
    <td>
      Input 5 settings
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40092
    </td>
    <td>
      91
    </td>
    <td>
      0x5B
    </td>
    <td>
      Input 6 settings
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40093
    </td>
    <td>
      92
    </td>
    <td>
      0x5C
    </td>
    <td>
      Input 7 settings
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40094
    </td>
    <td>
      93
    </td>
    <td>
      0x5D
    </td>
    <td>
      Input 8 settings
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40095
    </td>
    <td>
      94
    </td>
    <td>
      0x5E
    </td>
    <td>
      Output 1 settings
    </td>
    <td>
      Read &amp; Write
    </td>
    <td rowspan="2">
      Alarm output settings:
      <br>0 — output is set by PLC;
      <br>+1 — value from input 1;
      <br>+2 — value from input 2;
      <br>+4 — value from input 3;
      <br>+8 — value from input 4;
      <br>+16 — value from input 5;
      <br>+32 — value from input 6;
      <br>+64 — value from input 7;
      <br>+128 — value from input 8;
      <br>+256 — Output is set if value is greater than Alarm Value (register 40097 or 40098) („cooling”);
      <br>+512 — Output is set if value is less than Alarm Value ( register 40097 or 40098) („heating”)
      <br>+1024 — The lowest value from selected inputs;
      <br>+2048 — The greatest value from selected inputs (if not select either of the two above options than is used average value of selected inputs)
    </td>
  </tr>
  <tr>
    <td>
      40096
    </td>
    <td>
      95
    </td>
    <td>
      0x5F
    </td>
    <td>
      Output 2 settings
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40097
    </td>
    <td>
      96
    </td>
    <td>
      0x60
    </td>
    <td>
      Alarm Value 1
    </td>
    <td>
      Read &amp; Write
    </td>
    <td rowspan="2">
      Alarm value for outputs
    </td>
  </tr>
  <tr>
    <td>
      40098
    </td>
    <td>
      97
    </td>
    <td>
      0x61
    </td>
    <td>
      Alarm Value 2
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
  <tr>
    <td>
      40099
    </td>
    <td>
      98
    </td>
    <td>
      0x62
    </td>
    <td>
      Alarm hysteresis 1
    </td>
    <td>
      Read &amp; Write
    </td>
    <td rowspan="2">
      The hysteresis value for alarm outputs
    </td>
  </tr>
  <tr>
    <td>
      40100
    </td>
    <td>
      99
    </td>
    <td>
      0x63
    </td>
    <td>
      Alarm hysteresis 2
    </td>
    <td>
      Read &amp; Write
    </td>
  </tr>
</table>

### Bit access

<table>
  <tr>
    <th>
      Address
    </th>
    <th>
      Address
      <br>DEC
    </th>
    <th>
      Address
      <br>HEX
    </th>
    <th>
      Register name
    </th>
    <th>
      Access
    </th>
    <th>
      Description
    </th>
  </tr>
  <tr>
    <td>
      801
    </td>
    <td>
      800
    </td>
    <td>
      0х320
    </td>
    <td>
      Input 1
    </td>
    <td>
      Read
    </td>
    <td>
      Set when the input is connected
    </td>
  </tr>
  <tr>
    <td>
      802
    </td>
    <td>
      801
    </td>
    <td>
      0х321
    </td>
    <td>
      Input 2
    </td>
    <td>
      Read
    </td>
    <td>
      Set when the input is connected
    </td>
  </tr>
  <tr>
    <td>
      803
    </td>
    <td>
      802
    </td>
    <td>
      0х322
    </td>
    <td>
      Input 3
    </td>
    <td>
      Read
    </td>
    <td>
      Set when the input is connected
    </td>
  </tr>
  <tr>
    <td>
      804
    </td>
    <td>
      803
    </td>
    <td>
      0х323
    </td>
    <td>
      Input 4
    </td>
    <td>
      Read
    </td>
    <td>
      Set when the input is connected
    </td>
  </tr>
  <tr>
    <td>
      805
    </td>
    <td>
      804
    </td>
    <td>
      0х324
    </td>
    <td>
      Input 5
    </td>
    <td>
      Read
    </td>
    <td>
      Set when the input is connected
    </td>
  </tr>
  <tr>
    <td>
      806
    </td>
    <td>
      805
    </td>
    <td>
      0х325
    </td>
    <td>
      Input 6
    </td>
    <td>
      Read
    </td>
    <td>
      Set when the input is connected
    </td>
  </tr>
  <tr>
    <td>
      807
    </td>
    <td>
      806
    </td>
    <td>
      0х326
    </td>
    <td>
      Input 7
    </td>
    <td>
      Read
    </td>
    <td>
      Set when the input is connected
    </td>
  </tr>
  <tr>
    <td>
      808
    </td>
    <td>
      807
    </td>
    <td>
      0х327
    </td>
    <td>
      Input 8
    </td>
    <td>
      Read
    </td>
    <td>
      Set when the input is connected
    </td>
  </tr>
  <tr>
    <td>
      817
    </td>
    <td>
      816
    </td>
    <td>
      0х330
    </td>
    <td>
      Alarm 1
    </td>
    <td>
      Read
    </td>
    <td>
      Alarm state 1
    </td>
  </tr>
  <tr>
    <td>
      818
    </td>
    <td>
      817
    </td>
    <td>
      0х331
    </td>
    <td>
      Alarm 2
    </td>
    <td>
      Read
    </td>
    <td>
      Alarm state 2
    </td>
  </tr>
  <tr>
    <td>
      819
    </td>
    <td>
      818
    </td>
    <td>
      0х332
    </td>
    <td>
      Alarm 3
    </td>
    <td>
      Read
    </td>
    <td>
      Alarm state 3
    </td>
  </tr>
  <tr>
    <td>
      820
    </td>
    <td>
      819
    </td>
    <td>
      0х333
    </td>
    <td>
      Alarm 4
    </td>
    <td>
      Read
    </td>
    <td>
      Alarm state 4
    </td>
  </tr>
  <tr>
    <td>
      821
    </td>
    <td>
      820
    </td>
    <td>
      0х334
    </td>
    <td>
      Alarm 5
    </td>
    <td>
      Read
    </td>
    <td>
      Alarm state 5
    </td>
  </tr>
  <tr>
    <td>
      822
    </td>
    <td>
      821
    </td>
    <td>
      0х335
    </td>
    <td>
      Alarm 6
    </td>
    <td>
      Read
    </td>
    <td>
      Alarm state 6
    </td>
  </tr>
  <tr>
    <td>
      823
    </td>
    <td>
      822
    </td>
    <td>
      0х336
    </td>
    <td>
      Alarm 7
    </td>
    <td>
      Read
    </td>
    <td>
      Alarm state 7
    </td>
  </tr>
  <tr>
    <td>
      824
    </td>
    <td>
      823
    </td>
    <td>
      0х337
    </td>
    <td>
      Alarm 8
    </td>
    <td>
      Read
    </td>
    <td>
      Alarm state 8
    </td>
  </tr>
  <tr>
    <td>
      825
    </td>
    <td>
      824
    </td>
    <td>
      0х338
    </td>
    <td>
      Digital output 1
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      State of digital output 1
    </td>
  </tr>
  <tr>
    <td>
      826
    </td>
    <td>
      825
    </td>
    <td>
      0х339
    </td>
    <td>
      Digital output 2
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      State of digital output 2
    </td>
  </tr>
</table>

## Configuration software

Modbus Configurator is software that is designed to set the module registers responsible for communication over Modbus network as well as to read and write the current value of other registers of the module. This program can be a convenient way to test the system as well as to observe real-time changes in the registers.
Communication with the module is done via the USB cable. The module does not require any drivers.

Configurator is a universal program, whereby it is possible to configure all available modules.

![](images/MO-AI8-SL/configurator.png) {.img-responsive}

