# UC MO-DO4-MI {.header}

[UC MO-DO4-MI Expansion Module](http://en.unitecontrol.com/MO-DO4-MI.html), 4 digital outputs Modbus RTU (RS485)

![](images/MO-DO4-MI/device.png) {.img-responsive}

## User Manual

## Safety Rules

* Before first use, refer to this manual
* Before first use, make sure that all cables are connected properly
* Please ensure proper working conditions, according to the device specifications (e.g. supply voltage, temperature, maximum power consumption)
* Before making any modifications to wiring connections, turn off the power supply

## Module Features

### Purpose and description of the module

- Name: Expansion Module, 4 digital outputs, Modbus RTU (RS485), ASCII
- art.: MO-DO4-MI
- Module MO-DO4 works in Slave mode.
- The module has 4 digital outputs. 
- This module is connected to the RS485 bus with twisted-pair wire. Communication is via Modbus RTU or Modbus ASCII.
- The module is designed for mounting on a DIN rail in accordance with DIN EN 5002.
- The module is equipped with a set of LEDs are used to indicate the status of inputs and outputs useful for diagnostics purposes and helping to find errors.
- Module configuration is done via USB by using a dedicated computer program. You can also change the parameters using the ModBus protocol.

### Techical Specifications

<table>
  <tr>
    <td>
      Power Supply
    </td>
    <td>
      Voltage
    </td>
    <td>
      12-24 V DC±20%
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Maximum Current
    </td>
    <td>
      62 mA @ 12 V / 35 mA @ 24 V
    </td>
  </tr>
  <tr>
    <td>
      Connection Speed
    </td>
    <td>
    </td>
    <td>
      2400 - 115200 bps
    </td>
  </tr>
  <tr>
    <td>
      Digital Outputs
    </td>
    <td>
      No of outputs
    </td>
    <td>
      4
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Max Voltage
    </td>
    <td>
      55 V
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Max current
    </td>
    <td>
      250 mA
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Output Type
    </td>
    <td>
      PNP
    </td>
  </tr>
  <tr>
    <td>
      Temperature
      </td>
      <td>
          Work
      </td>
      <td>
          -20°C ÷ +65°C
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Storage
      </td>
      <td>
          -40°C ÷ +85°C
      </td>
  </tr>
  <tr>
      <td>
          Connectors
      </td>
      <td>
          Power Supply
      </td>
      <td>
          2 pin
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Communication
      </td>
      <td>
          3 pin
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Outputs
      </td>
      <td>
          2 x 3 pin
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Configuration
      </td>
      <td>
          MiniUSB
      </td>
  </tr>
  <tr>
      <td>
          Dimensions
      </td>
      <td>
          Height
      </td>
      <td>
          90 mm
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Length
      </td>
      <td>
          56 mm
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Width
      </td>
      <td>
          17 mm
      </td>
  </tr>
  <tr>
      <td>
          Interface
      </td>
      <td>
          RS485
      </td>
      <td>
          Up to 128 devices
      </td>
  </tr>
</table>


### Dimensions of the product

Look at dimensions of the module are shown below. The module is mounted directly to the rail in the DIN industry standart.

![](images/MO-DO4-MI/overview.png) {.img-responsive}

## Communication configuration

### Grounding and shielding

In most cases, IO (Inputs/Outputs) modules will be installed in an enclosure along with other devices which generate electromagnetic radiation. Examples of these devices are relays and contactors, transformers, motor controllers etc. This electromagnetic radiation can induce electrical noise into both power and signal lines, as well as direct radiation into the module causing negative effects on the system. Appropriate grounding, shielding and other protective steps should be taken at the installation stage to prevent these effects. These protective steps include control cabinet grounding, module grounding, cable shield grounding, protective elements for electromagnetic switching devices, correct wiring as well as consideration of cable types and their cross sections.

### Network Termination

Transmission line effects often present a problem on data communication networks. These problems include reflections and signal attenuation.

To eliminate the presence of reflections from the end of the cable, the cable must be terminated at both ends with a resistor across the line equal to its characteristic impedance. Both ends must be terminated since the direction of propagation is bidirectional. In the case of an RS485 twisted pair cable this termination is typically 120 Ω.

### Types of Modbus Registers

There are 4 types of variables available in the module

<table>
  <tr>
      <th>
          <b>Type</b>
      </th>
      <th>
          <b>Beginning address</b>
      </th>
      <th>
          <b>Variable</b>
      </th>
      <th>
          <b>Access</b>
      </th>
      <th>
          <b>Modbus Command</b>
      </th>
  </tr>
  <tr>
      <td>
          1
      </td>
      <td>
          00001
      </td>
      <td>
          Digital Outputs
      </td>
      <td>
          Bit Read &amp; Write
      </td>
      <td>
          1, 5, 15
      </td>
  </tr>
  <tr>
      <td>
          2
      </td>
      <td>
          10001
      </td>
      <td>
          Digital Inputs
      </td>
      <td>
          Bit Read
      </td>
      <td>
          2
      </td>
  </tr>
  <tr>
      <td>
          3
      </td>
      <td>
          30001
      </td>
      <td>
          Input Registers
      </td>
      <td>
          Registered Read
      </td>
      <td>
          3
      </td>
  </tr>
  <tr>
      <td>
          4
      </td>
      <td>
          40001
      </td>
      <td>
          Output Registers
      </td>
      <td>
          Registered Read & Write
      </td>
      <td>
          4, 6, 16
      </td>
  </tr>
</table>

### Communication settings

The data stored in the modules memory are in 16-bit registers. Access to registers is via MODBUS RTU or MODBUS ASCII.

#### Default settings

<table>
  <tr>
      <td>
          Baud rate, bps
      </td>
      <td>
          19200
      </td>
  </tr>
  <tr>
      <td>
          Adress
      </td>
      <td>
          1
      </td>
  </tr>
  <tr>
      <td>
          Parity
      </td>
      <td>
          No
      </td>
  </tr>
  <tr>
      <td>
          Data bits
      </td>
      <td>
          8
      </td>
  </tr>
  <tr>
      <td>
          Stop bits
      </td>
      <td>
          1
      </td>
  </tr>
  <tr>
      <td>
          Reply Delay, ms
      </td>
      <td>
          0
      </td>
  </tr>
  <tr>
      <td>
          Modbus Type
      </td>
      <td>
          RTU
      </td>
  </tr>
</table>

#### Configuration registers

<table>
  <thead>
      <tr>
          <th>
              Address
          </th>
          <th>
              Name
          </th>
          <th>
              Values
          </th>
      </tr>
  </thead>
  <tbody>
      <tr>
          <td>
              40002
          </td>
          <td>
              Adress
          </td>
          <td>
              From 0 to 255
          </td>
      </tr>
	  <tr>
          <td>
              40003
          </td>
          <td>
              Baud rate
          </td>
          <td>
              0 - 2400
              <br>1 - 4800
              <br>2 - 9600
              <br>3 - 19200
              <br>4 - 38400
              <br>5 - 57600
              <br>6 - 115200
              <br>other – value * 10
          </td>
      </tr>
      <tr>
          <td>
              40005
          </td>
          <td>
              Parity
          </td>
          <td>
              0 - none
              <br>1 - odd
              <br>2 - even
              <br>3 - always 1
              <br>4 - вalways 0
          </td>
      </tr>
      <tr>
          <td>
              40004
          </td>
          <td>
              Stop Bits LSB
          </td>
          <td>
              1 - one stop bit
              <br>2 - two stop bits
          </td>
      </tr>
      <tr>
          <td>
              40004
          </td>
          <td>
              Data Bits MSB
          </td>
          <td>
              7 - 7 data bits
              <br>8 - 8 data bits
          </td>
      </tr>
      <tr>
          <td>
              40006
          </td>
          <td>
              Response delay
          </td>
          <td>
              Time in ms
          </td>
      </tr>
      <tr>
          <td>
              40007
          </td>
          <td>
              Modbus Mode
          </td>
          <td>
              0 - RTU
              <br>1 - ASCII
          </td>
      </tr>
  </tbody>
</table>

## Indicators

![](images/MO-DO4-MI/leds.png) {.img-responsive}

<table>
  <tr>
    <th>
      Indicator
    </th>
    <th>
      Description
    </th>
  </tr>
  <tr>
    <td>
      Power (ON)
    </td>
    <td>
      LED indicates that the module is correctly powered.
    </td>
  </tr>
  <tr>
    <td>
      TX
    </td>
    <td>
      The LED lights up when the unit received the correct packet and sends the answer.
    </td>
  </tr>
  <tr>
    <td>
      Outputs (DO1..DO4)
    </td>
    <td>
      LED indicates that on the output is high state.
    </td>
  </tr>
</table>

## Block diagram

![](images/MO-DO4-MI/scheme.png) {.img-responsive}

## Module Connection

![](images/MO-DO4-MI/connection.gif) {.img-responsive}

## Modules Registers

### Registered access

<table>
  <thead>
    <tr>
      <th>
        Address
      </th>
      <th>
        Dec
      </th>
      <th>
        Hex
      </th>
      <th>
        Register name
      </th>
      <th>
        Acess
      </th>
      <th>
        Description
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        30001
      </td>
      <td>
        0
      </td>
      <td>
        0x00
      </td>
      <td>
        Version/Type
      </td>
      <td>
        Read
      </td>
      <td>
        Version and Type of the device
      </td>
    </tr>
    <tr>
      <td>
        30002
      </td>
      <td>
        1
      </td>
      <td>
        0x01
      </td>
      <td>
        Switches
      </td>
      <td>
        Read
      </td>
      <td>
        Switches state
      </td>
    </tr>
    <tr>
      <td>
        40003
      </td>
      <td>
        2
      </td>
      <td>
        0x02
      </td>
      <td>
        Baud rate
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        RS485 baud rate
      </td>
    </tr>
    <tr>
      <td>
        40004
      </td>
      <td>
        3
      </td>
      <td>
        0x03
      </td>
      <td>
        Stop Bits & Data Bits
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of Stop bits & Data Bits
      </td>
    </tr>
    <tr>
      <td>
        40005
      </td>
      <td>
        4
      </td>
      <td>
        0x04
      </td>
      <td>
        Parity
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Parity bit
      </td>
    </tr>
    <tr>
      <td>
        40006
      </td>
      <td>
        5
      </td>
      <td>
        0x05
      </td>
      <td>
        Response Delay
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Response delay in ms
      </td>
    </tr>
    <tr>
      <td>
        40007
      </td>
      <td>
        6
      </td>
      <td>
        0x06
      </td>
      <td>
        Modbus Mode
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Modbus Mode (ASCII or RTU)
      </td>
    </tr>
    <tr>
      <td>
        40009
      </td>
      <td>
        8
      </td>
      <td>
        0x08
      </td>
      <td>
        Watchdog
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Watchdog
      </td>
    </tr>
    <tr>
      <td>
        40013
      </td>
      <td>
        12
      </td>
      <td>
        0x0C
      </td>
      <td>
        Default Output State
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Default output state (after power on or watchdog reset)
      </td>
    </tr>
    <tr>
      <td>
        40033
      </td>
      <td>
        32
      </td>
      <td>
        0x20
      </td>
      <td>
        Received packets MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets
      </td>
    </tr>
    <tr>
      <td>
        40034
      </td>
      <td>
        33
      </td>
      <td>
        0x21
      </td>
      <td>
        Received packets LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets
      </td>
    </tr>
    <tr>
      <td>
        40035
      </td>
      <td>
        34
      </td>
      <td>
        0x22
      </td>
      <td>
        Incorrect packets MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets with error
      </td>
    </tr>
    <tr>
      <td>
        40036
      </td>
      <td>
        35
      </td>
      <td>
        0x23
      </td>
      <td>
        Incorrect packets LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets with error
      </td>
    </tr>
    <tr>
      <td>
        40037
      </td>
      <td>
        36
      </td>
      <td>
        0x24
      </td>
      <td>
        Sent packets MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of sent packets
      </td>
    </tr>
    <tr>
      <td>
        40038
      </td>
      <td>
        37
      </td>
      <td>
        0x25
      </td>
      <td>
        Sent packets LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of sent packets
      </td>
    </tr>
    <tr>
      <td>
        40052
      </td>
      <td>
        51
      </td>
      <td>
        0x33
      </td>
      <td>
        Outputs
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Output state
      </td>
    </tr>
  </tbody>
</table>

### Bit access

<table>
  <tr>
    <th>
      Address
    </th>
    <th>
      Address
      <br>DEC
    </th>
    <th>
      Address
      <br>HEX
    </th>
    <th>
      Register name
    </th>
    <th>
      Access
    </th>
    <th>
      Description
    </th>
  </tr>
  <tr>
    <td>
      193
    </td>
    <td>
      192
    </td>
    <td>
      0x0C0
    </td>
    <td>
      Default state of output 1
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 1
    </td>
  </tr>
  <tr>
    <td>
      194
    </td>
    <td>
      193
    </td>
    <td>
      0x0C1
    </td>
    <td>
      Default state of output 2
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 2
    </td>
  </tr>
   <tr>
    <td>
      195
    </td>
    <td>
      194
    </td>
    <td>
      0x0C2
    </td>
    <td>
      Default state of output 3
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 3
    </td>
  </tr>
   <tr>
    <td>
      196
    </td>
    <td>
      195
    </td>
    <td>
      0x0C3
    </td>
    <td>
      Default state of output 4
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 4
    </td>
  </tr>
  <tr>
    <td>
      817
    </td>
    <td>
      816
    </td>
    <td>
      0x330
    </td>
    <td>
      Output 1
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 1 state
    </td>
  </tr>
  <tr>
    <td>
      818
    </td>
    <td>
      817
    </td>
    <td>
      0x331
    </td>
    <td>
      Output 2
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 2 state
    </td>
  </tr>
  <tr>
    <td>
      819
    </td>
    <td>
      818
    </td>
    <td>
      0x332
    </td>
    <td>
      Output 3
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 3 state
    </td>
  </tr>
  <tr>
    <td>
      820
    </td>
    <td>
      819
    </td>
    <td>
      0x333
    </td>
    <td>
      Output 4
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 4 state
    </td>
  </tr>
</table>

## Configuration software

Modbus Configurator is software that is designed to set the module registers responsible for communication over Modbus network as well as to read and write the current value of other registers of the module. This program can be a convenient way to test the system as well as to observe real-time changes in the registers.
Communication with the module is done via the USB cable. The module does not require any drivers.

Configurator is a universal program, whereby it is possible to configure all available modules.

![](images/MO-DO4-MI/configurator.png) {.img-responsive}

