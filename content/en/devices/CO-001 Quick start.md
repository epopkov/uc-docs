# Quick start guide

1. Remove top case cover from UC HUB.
1. Insert battery and SIM card.
1. Close top cover.
1. Connect power adapter according to scheme on case. UC HUB supports 10~30 V AC/DC, 500 mA
1. UC HUB will boot Linux. You should see orange LED Power blinking. If this doesn't happen, you need to press button SW1.
1. Boot time is up to 1 minute on average.
1. After successful boot you will LEDs Power, Internet and GSM.

## Initial connection

There are two ways to connect UC HUB initially:

  * Connection using Ethernet cable.
    * You have to connect UC HUB with your router with Ethernet cable. Router has to have working DHCP server.
  * Connection using WiFi
    * Hold SW2 button for at least 5 seconds
    * LED "Internet" will change its color to orange
    * Connect to WiFi network (ssid: ***UC-HUB***, password: ***unitecontrol***)

After that you can connect to UC HUB:

  * with SSH (22 port): #ssh ***root@10.10.10.1***, default password: ***unitecontrol***
  * with SFTP (22 порт), user: ***root***, default password: ***unitecontrol***

To turn off UC HUB, you have to hold SW1 button for at least 5 seconds. LED will change color to orange. UC HUB will turn off all processes safely. This usually takes about 1 minute.

# Components description

## LEDs

### Power

Status | Meaning
--- | ---
Orange static | Hub is loading
Green static | AC power is connected
Red static | No AC power, battery is connected
Red flashing | Low battery charge
Green flashing | Battery is not connected

### Internet

Status | Meaning
--- | ---
Green static | Internet connection is established
Red static | WiFi access point is on
Orange static | Internet connection is established and WiFi access point is on

 > *Red and green LEDs can work at the same time
 
### GSM

Status | Meaning
--- | ---
Red static | Initialization error or network is unavailable
Green static | Modem has initialized
Green flashing | Modem has initialized, but GSM signal is low

## Buttons

Button | Push | Explanation
--- | --- | ---
Power | Short click | Turn on hub
—  | Long push (> 5 sec) | Force reboot hub
AP | Long push (> 5 sec) | Turn on/off WiFi AP
SW | — | —

# Linux Guide

## Interfaces

Physical | In linux
--- | ---
1-wire | /dev/i2c-2
RS485-1 | /dev/ttyUSB0
RS485-2 | /dev/ttyUSB1
RS232 | /dev/ttyS1
Modem | /dev/ttyS2 

## WiFi AP

SSID: ***UC-Hub***

Password: ***unitecontrol***

Hub ip: ***10.10.10.1***

## SSH/SFTP

User: ***root***

Default password: ***unitecontrol***

### Linux/MacOS

SSH client is usually build in.

| *# ssh <user>@<hub ip address>* |

### Windows

1. Get putty from http://www.putty.org
1. Choose ssh and enter ip address of the hub. Leave other settings default.

### SFTP

We recommend to use FileZilla. It's crossplatform and easy to use ftp/sftp client. Get it from https://filezilla-project.org or use your packet manager.

## Autostart services

UC Hub uses default debian 7 init system - SysV scripts. Checkout configs in /etc/init.d folder.

### Add to autostart queue

| *root@UC:~# update-rc.d <service name> defaults* |

### Remove from autostart queue

| *root@UC:~# update-rc.d -f <service name> remove* |

## Network settings

UC Hub got two client interface - ethernet and wifi.
https://wiki.debian.org/NetworkConfiguration
Checkout examples in /etc/network folder.

## Modem settings

Modem service is deactivated by default. Turn autostart of modem service on before SMS or PPPD use. PIN code of the sim card should be deactivated.

### Connect to internet

Service wvdial, used for connection, is turned off by default. Wvdial should be configured before start.
Set your apn, login and password in /etc/wvdial.conf

### SMS daemon

Gammu is used as sms daemon (http://wammu.eu/docs/pdf/smsd.pdf), checkout /etc/gammu-smsdrc.
Service gammu-smsd is turned off by default.

#### SMS send with smsd runned

| *root@UC:~# gammu-smsd-inject TEXT <phone number> -unicode -text "Sample text"* |

#### SMS send without smsd

| *root@UC:~# gammu sendsms TEXT <phone number> -unicode -text "Sample text"* |

## Change root password

| *root@UC:~# passwd* |

## Continuously running and critycal programs

### Monit

Monit is a small Open Source utility for managing and monitoring Unix systems. Monit conducts automatic maintenance and repair and can execute meaningful causal actions in error situations.

Checkout http://mmonit.com/monit/ for more information and manuals. Monit 5.10 is preinstalled to UC Hub as service (autostart is off).

### Time and date

| *root@UC:~# date -R* |
| *Wed, 29 Oct 2014 16:27:02 +0200* |

### Select time zone

*root@UC:~# tzselect*
*Please identify a location so that time zone rules can be set correctly.*
*Please select a continent or ocean.*
 *1. Africa*
 *2. Americas*
 *3. Antarctica*
 *4. Arctic Ocean*
...

### One time sync with ntp server.

| *root@UC:~# ntpdate 0.europe.pool.ntp.org* |
| *29 Oct 16:31:02 ntpdate[13029]: step time server 192.53.103.108 offset 0.014582 sec* |

You may choose any other server (checkout pool.ntp.org).

### Sync with ntp server at boot.

| *root@UC:~# update-rc.d ntp defaults* |
| *update-rc.d: using dependency based boot sequencing* |

**That may cause system to boot slower.**

### Turn off autosync with ntp server

| *root@UC:~# update-rc.d -f ntp remove* |
| *update-rc.d: using dependency based boot sequencing* |

## 1-wire

OWFS is installed and configured. Check /etc/owfs.conf
Service owserver is runned by default. 
