# UC MO-DIDO8-ST {.header}

[UC MO-DIDO8-ST Expansion Modul](http://en.unitecontrol.com/MO-DIDO8-ST.html), 8 digital inputs, 8 digital outputs Modbus RTU (RS485)

![](images/MO-DIDO8-ST/device.png) {.img-responsive}

## User Manual

## Safety Rules

* Before first use, refer to this manual
* Before first use, make sure that all cables are connected properly
* Please ensure proper working conditions, according to the device specifications (e.g. supply voltage, temperature, maximum power consumption)
* Before making any modifications to wiring connections, turn off the power supply

## Module Features

### Purpose and description of the module

- Name: Expansion Module, 8 digital inputs, 8 digital outputs, Modbus RTU (RS485), ASCII
- art. MO-DIDO8-ST
- Module MO-DIDO8 works in Slave mode.
- The module has 8 digital inputs with configurable timer/counter option and 8 digital outputs. In addition, terminals IN1, IN2, IN3, IN4 can be used to connect two encoders. All inputs are isolated from the logic by optocouplers.
- This module is connected to the RS485 bus with twisted-pair wire. Communication is via Modbus RTU or Modbus ASCII.
- The module is designed for mounting on a DIN rail in accordance with DIN EN 5002.
- The module is equipped with a set of LEDs are used to indicate the status of inputs and outputs useful for diagnostics purposes and helping to find errors.
- Module configuration is done via USB by using a dedicated computer program. You can also change the parameters using the ModBus protocol.

### Techical Specifications

<table>
  <tr>
    <td>
      Power Supply
    </td>
    <td>
      Voltage
    </td>
    <td>
      10-30 V DC; 10-28 V AC
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Maximum Current*
    </td>
    <td>
      DC: 100 mA @ 24 V
      <br>AC: 125 mA @ 24 V
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Max Power Consumption
    </td>
    <td>
      DC: 2,4 W
      <br>AC: 3 VA
    </td>
  </tr>
  <tr>
    <td>
      Connection Speed
    </td>
    <td>
    </td>
    <td>
      2400 - 115200 bps
    </td>
  </tr>
  <tr>
    <td>
      Digital Inputs
    </td>
    <td>
      No of inputs
    </td>
    <td>
      8
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Voltage range
    </td>
    <td>
      0 – 30 V
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Low State „0”
    </td>
    <td>
      0 – 3 V
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      High State „1”
    </td>
    <td>
      6 – 30 V
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Input impedance
    </td>
    <td>
      4 kΩ
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Isolation
    </td>
    <td>
      1500 Vrms
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Input Type
    </td>
    <td>
      PNP or NPN
    </td>
  </tr>
  <tr>
    <td>
      Digital Outputs
    </td>
    <td>
      No of outputs
    </td>
    <td>
      8
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Max Voltage
    </td>
    <td>
      30 В
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Max current
    </td>
    <td>
      500 mA
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Output Type
    </td>
    <td>
      PNP
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Output protection
    </td>
    <td>
      4A polymer fuse
    </td>
  </tr>
  <tr>
    <td>
      Counters
    </td>
    <td>
      No
    </td>
    <td>
      8
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Resolution
    </td>
    <td>
      32 bits
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Frequency
    </td>
    <td>
      1 kHz (max)
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Impulse Width
    </td>
    <td>
      500 μs (min)
    </td>
  </tr>
  <tr>
    <td>
      Temperature
    </td>
    <td>
      Work
    </td>
    <td>
      -20°C ÷ +65°C
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Storage
    </td>
    <td>
      -40°C ÷ +85°C
    </td>
  </tr>
  <tr>
    <td>
      Connectors
    </td>
    <td>
      Power Supply
    </td>
    <td>
      2 pin
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Communication
    </td>
    <td>
      3 pin
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Inputs &amp; Outputs
    </td>
    <td>
      2 x 10 pin
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Configuration
    </td>
    <td>
      MiniUSB
    </td>
  </tr>
  <tr>
    <td>
      Dimensions
    </td>
    <td>
      Height
    </td>
    <td>
      88 mm
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Length
    </td>
    <td>
      110 mm
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Width
    </td>
    <td>
      62 mm
    </td>
  </tr>
  <tr>
    <td>
      Interface
    </td>
    <td>
      RS485
    </td>
    <td>
      Up to 128 devices
    </td>
  </tr>
</table>

### Dimensions of the product

Look and dimensions of the module are shown below. The module is mounted directly to the rail in the DIN industry standard. Power connectors, communication and IOs are at the bottom and top of the module. USB connector configuration and indicators located on the front of the module.

![](images/MO-DIDO8-ST/overview.png) {.img-responsive}

## Communication configuration

### Grounding and shielding

In most cases, IO (Inputs/Outputs) modules will be installed in an enclosure along with other devices which generate electromagnetic radiation. Examples of these devices are relays and contactors, transformers, motor controllers etc. This electromagnetic radiation can induce electrical noise into both power and signal lines, as well as direct radiation into the module causing negative effects on the system. Appropriate grounding, shielding and other protective steps should be taken at the installation stage to prevent these effects. These protective steps include control cabinet grounding, module grounding, cable shield grounding, protective elements for electromagnetic switching devices, correct wiring as well as consideration of cable types and their cross sections.

### Network Termination

Transmission line effects often present a problem on data communication networks. These problems include reflections and signal attenuation.

To eliminate the presence of reflections from the end of the cable, the cable must be terminated at both ends with a resistor across the line equal to its characteristic impedance. Both ends must be terminated since the direction of propagation is bidirectional. In the case of an RS485 twisted pair cable this termination is typically 120 Ω.

### Setting Module Address in RS485 Modbus Network

The following table shows how to set switch to determine the address of the module. The module address is set with the switches in the range of 0 to 127. Addresses From 127 to 255 can by set via RS485 or USB.

<table>
  <tr>
    <td>
      <b>Switch</b>
    </td>
    <td>
      <b>Address</b>
    </td>
  </tr>
  <tr>
    <td>
      SW1
    </td>
    <td>
      +1
    </td>
  </tr>
  <tr>
    <td>
      SW2
    </td>
    <td>
      +2
    </td>
  </tr>
  <tr>
    <td>
      SW3
    </td>
    <td>
      +4
    </td>
  </tr>
  <tr>
    <td>
      SW4
    </td>
    <td>
      +8
    </td>
  </tr>
  <tr>
    <td>
      SW5
    </td>
    <td>
      +16
    </td>
  </tr>
  <tr>
    <td>
      SW6
    </td>
    <td>
      +32
    </td>
  </tr>
  <tr>
    <td>
      SW7
    </td>
    <td>
      +64
    </td>
  </tr>
</table>

### Types of Modbus Registers

There are 4 types of variables available in the module

<table>
  <tr>
    <th>
      <b>Type</b>
    </th>
    <th>
      <b>Beginning address</b>
    </th>
    <th>
      <b>Variable</b>
    </th>
    <th>
      <b>Access</b>
    </th>
    <th>
      <b>Modbus Command</b>
    </th>
  </tr>
  <tr>
    <td>
      1
    </td>
    <td>
      00001
    </td>
    <td>
      Digital Outputs
    </td>
    <td>
      Bit Read &amp; Write
    </td>
    <td>
      1, 5, 15
    </td>
  </tr>
  <tr>
    <td>
      2
    </td>
    <td>
      10001
    </td>
    <td>
      Digital Inputs
    </td>
    <td>
      Bit Read
    </td>
    <td>
      2
    </td>
  </tr>
  <tr>
    <td>
      3
    </td>
    <td>
      30001
    </td>
    <td>
      Input Registers
    </td>
    <td>
      Registered Read
    </td>
    <td>
      3
    </td>
  </tr>
  <tr>
    <td>
      4
    </td>
    <td>
      40001
    </td>
    <td>
      Output Registers
    </td>
    <td>
      Registered Read &amp; Write
    </td>
    <td>
      4, 6, 16
    </td>
  </tr>
</table>

### Communication settings

The data stored in the modules memory are in 16-bit registers. Access to registers is via MODBUS RTU or MODBUS ASCII.

#### Default settings

You can restore the default configuration by the switch SW8 (see - Restore the default configuration)

<table>
  <tr>
    <td>
      Baud rate, bps
    </td>
    <td>
      19200
    </td>
  </tr>
  <tr>
    <td>
      Parity
    </td>
    <td>
      No
    </td>
  </tr>
  <tr>
    <td>
      Data bits
    </td>
    <td>
      1
    </td>
  </tr>
  <tr>
    <td>
      Stop bits
    </td>
    <td>
      1
    </td>
  </tr>
  <tr>
    <td>
      Reply Delay, ms
    </td>
    <td>
      0
    </td>
  </tr>
  <tr>
    <td>
      Modbus Type
    </td>
    <td>
      RTU
    </td>
  </tr>
</table>

#### Restore the default configuration

To restore the default configuration:
* turn off the power
* turn on the switch SW8
* turn on the power
* when power and communication LED flash turn off the switch SW8

Caution! After restoring the default configuration all values stored in the registers will be cleared as well.

#### Configuration registers

<table>
  <tr>
    <th>
      Address
    </th>
    <th>
      Name
    </th>
    <th>
      Values
    </th>
  </tr>
  <tr>
    <td>
      40003
    </td>
    <td>
      Baud rate
    </td>
    <td>
      0 - 2400
      <br>1 - 4800
      <br>2 - 9600
      <br>3 - 19200
      <br>4 - 38400
      <br>5 - 57600
      <br>6 - 115200
      <br>other – value * 10
    </td>
  </tr>
  <tr>
    <td>
      40005
    </td>
    <td>
      Parity
    </td>
    <td>
      0 - none
      <br>1 - odd
      <br>2 - even
      <br>3 - always 1
      <br>4 - always 0
    </td>
  </tr>
  <tr>
    <td>
      40004
    </td>
    <td>
      Stop Bits LSB
    </td>
    <td>
      1 - one stop bit
      <br>2 - 2 two stop bits
    </td>
  </tr>
  <tr>
    <td>
      40004
    </td>
    <td>
      Data Bits MSB
    </td>
    <td>
      7 - 7 data bits
      <br>8 - 8 data bits
    </td>
  </tr>
  <tr>
    <td>
      40006
    </td>
    <td>
      Response delay
    </td>
    <td>
      Time in ms
    </td>
  </tr>
  <tr>
    <td>
      40007
    </td>
    <td>
      Modbus Mode
    </td>
    <td>
      0 - RTU
      <br>1 - ASCII
    </td>
  </tr>
</table>

## Indicators

![](images/MO-DIDO8-ST/leds.png) {.img-responsive}

<table>
  <tr>
    <th>
      Indicator
    </th>
    <th>
      Description
    </th>
  </tr>
  <tr>
    <td>
      Power (ON)
    </td>
    <td>
      LED indicates that the module is correctly powered.
    </td>
  </tr>
  <tr>
    <td>
      TX
    </td>
    <td>
      The LED lights up when the unit received the correct packet and sends the answer.
    </td>
  </tr>
  <tr>
    <td>
      Inputs (DI1..DI8)
    </td>
    <td>
      LED indicates that on the input is high state.
    </td>
  </tr>
  <tr>
    <td>
      Outputs (DO1..DO8)
    </td>
    <td>
      LED indicates that on the output is high state.
    </td>
  </tr>
</table>

## Module Connection

![](images/MO-DIDO8-ST/connection.jpg) {.img-responsive}

## Front panel removing

![](images/MO-DIDO8-ST/disassemble.gif) {.img-responsive}

## Switches

![](images/MO-DIDO8-ST/switch.png) {.img-responsive}

<table>
  <tr>
    <th>
      Switch
    </th>
    <th>
      Function
    </th>
    <th>
      Description
    </th>
  </tr>
  <tr>
    <td>
      1
    </td>
    <td>
      Module address +1
    </td>
    <td rowspan="7">
      Setting module address from 0 to 127
    </td>
  </tr>
  <tr>
    <td>
      2
    </td>
    <td>
      Module address +2
    </td>
  </tr>
  <tr>
    <td>
      3
    </td>
    <td>
      Module address +4
    </td>
  </tr>
  <tr>
    <td>
      4
    </td>
    <td>
      Module address +8
    </td>
  </tr>
  <tr>
    <td>
      5
    </td>
    <td>
      Module address +16
    </td>
  </tr>
  <tr>
    <td>
      6
    </td>
    <td>
      Module address +32
    </td>
  </tr>
  <tr>
    <td>
      7
    </td>
    <td>
      Module address +64
    </td>
  </tr>
  <tr>
    <td>
      8
    </td>
    <td>
      Restoring default settings
    </td>
    <td>
      Restoring default settings (see - Default settings and - Restore the default configuration).
    </td>
  </tr>
</table>

## Modules Registers

### Registered access

<table>
  <thead>
    <tr>
      <th>
        Address
      </th>
      <th>
        Dec
      </th>
      <th>
        Hex
      </th>
      <th>
        Register name
      </th>
      <th>
        Acess
      </th>
      <th>
        Description
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        30001
      </td>
      <td>
        0
      </td>
      <td>
        0x00
      </td>
      <td>
        Version/Type
      </td>
      <td>
        Read
      </td>
      <td>
        Version and Type of the device
      </td>
    </tr>
    <tr>
      <td>
        30002
      </td>
      <td>
        1
      </td>
      <td>
        0x01
      </td>
      <td>
        Switches
      </td>
      <td>
        Read
      </td>
      <td>
        Switches state
      </td>
    </tr>
    <tr>
      <td>
        40003
      </td>
      <td>
        2
      </td>
      <td>
        0x02
      </td>
      <td>
        Baud rate
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        RS485 baud rate
      </td>
    </tr>
    <tr>
      <td>
        40004
      </td>
      <td>
        3
      </td>
      <td>
        0x03
      </td>
      <td>
        Stop Bits & Data Bits
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of Stop bits & Data Bits
      </td>
    </tr>
    <tr>
      <td>
        40005
      </td>
      <td>
        4
      </td>
      <td>
        0x04
      </td>
      <td>
        Parity
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Parity bit
      </td>
    </tr>
    <tr>
      <td>
        40006
      </td>
      <td>
        5
      </td>
      <td>
        0x05
      </td>
      <td>
        Response Delay
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Response delay in ms
      </td>
    </tr>
    <tr>
      <td>
        40007
      </td>
      <td>
        6
      </td>
      <td>
        0x06
      </td>
      <td>
        Modbus Mode
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Modbus Mode (ASCII or RTU)
      </td>
    </tr>
    <tr>
      <td>
        40009
      </td>
      <td>
        8
      </td>
      <td>
        0x08
      </td>
      <td>
        Watchdog
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Watchdog
      </td>
    </tr>
    <tr>
      <td>
        40013
      </td>
      <td>
        12
      </td>
      <td>
        0x0C
      </td>
      <td>
        Default Output State
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Default output state (after power on or watchdog reset)
      </td>
    </tr>
    <tr>
      <td>
        40033
      </td>
      <td>
        32
      </td>
      <td>
        0x20
      </td>
      <td>
        Received packets MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets
      </td>
    </tr>
    <tr>
      <td>
        40034
      </td>
      <td>
        33
      </td>
      <td>
        0x21
      </td>
      <td>
        Received packets LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets
      </td>
    </tr>
    <tr>
      <td>
        40035
      </td>
      <td>
        34
      </td>
      <td>
        0x22
      </td>
      <td>
        Incorrect packets MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets with error
      </td>
    </tr>
    <tr>
      <td>
        40036
      </td>
      <td>
        35
      </td>
      <td>
        0x23
      </td>
      <td>
        Incorrect packets LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets with error
      </td>
    </tr>
    <tr>
      <td>
        40037
      </td>
      <td>
        36
      </td>
      <td>
        0x24
      </td>
      <td>
        Sent packets MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of sent packets
      </td>
    </tr>
    <tr>
      <td>
        40038
      </td>
      <td>
        37
      </td>
      <td>
        0x25
      </td>
      <td>
        Sent packets LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of sent packets
      </td>
    </tr>
    <tr>
      <td>
        30051
      </td>
      <td>
        50
      </td>
      <td>
        0x32
      </td>
      <td>
        Inputs
      </td>
      <td>
        Read
      </td>
      <td>
        Inputs state
      </td>
    </tr>
    <tr>
      <td>
        40052
      </td>
      <td>
        51
      </td>
      <td>
        0x33
      </td>
      <td>
        Outputs
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Output state
      </td>
    </tr>
    <tr>
      <td>
        40053
      </td>
      <td>
        52
      </td>
      <td>
        0x34
      </td>
      <td>
        Counter 1 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 1
      </td>
    </tr>
    <tr>
      <td>
        40054
      </td>
      <td>
        53
      </td>
      <td>
        0x35
      </td>
      <td>
        Counter 1 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 1
      </td>
    </tr>
    <tr>
      <td>
        40055
      </td>
      <td>
        54
      </td>
      <td>
        0x36
      </td>
      <td>
        Counter 2 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 2
      </td>
    </tr>
    <tr>
      <td>
        40056
      </td>
      <td>
        55
      </td>
      <td>
        0x37
      </td>
      <td>
        Counter 2 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 2
      </td>
    </tr>
    <tr>
      <td>
        40057
      </td>
      <td>
        56
      </td>
      <td>
        0x38
      </td>
      <td>
        Counter 3 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 3
      </td>
    </tr>
    <tr>
      <td>
        40058
      </td>
      <td>
        57
      </td>
      <td>
        0x39
      </td>
      <td>
        Counter 3 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 3
      </td>
    </tr>
	<tr>
      <td>
        40059
      </td>
      <td>
        58
      </td>
      <td>
        0x3A
      </td>
      <td>
        Counter 4 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 4
      </td>
    </tr>
    <tr>
      <td>
        40060
      </td>
      <td>
        59
      </td>
      <td>
        0x3B
      </td>
      <td>
        Counter 4 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 4
      </td>
    </tr>
	<tr>
      <td>
        40061
      </td>
      <td>
        60
      </td>
      <td>
        0x3C
      </td>
      <td>
        Counter 5 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 5
      </td>
    </tr>
    <tr>
      <td>
        40062
      </td>
      <td>
        61
      </td>
      <td>
        0x3D
      </td>
      <td>
        Counter 5 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 5
      </td>
    </tr>
	<tr>
      <td>
        40063
      </td>
      <td>
        62
      </td>
      <td>
        0x3E
      </td>
      <td>
        Counter 6 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 6
      </td>
    </tr>
    <tr>
      <td>
        40064
      </td>
      <td>
        63
      </td>
      <td>
        0x3F
      </td>
      <td>
        Counter 6 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 6
      </td>
    </tr>
	<tr>
      <td>
        40065
      </td>
      <td>
        64
      </td>
      <td>
        0x40
      </td>
      <td>
        Counter 7 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 7
      </td>
    </tr>
    <tr>
      <td>
        40066
      </td>
      <td>
        65
      </td>
      <td>
        0x41
      </td>
      <td>
        Counter 7 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 7
      </td>
    </tr>
	<tr>
      <td>
        40067
      </td>
      <td>
        66
      </td>
      <td>
        0x42
      </td>
      <td>
        Counter 8 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 8
      </td>
    </tr>
    <tr>
      <td>
        40068
      </td>
      <td>
        67
      </td>
      <td>
        0x43
      </td>
      <td>
        Counter 8 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 8
      </td>
    </tr>
	<tr>
      <td>
        40085
      </td>
      <td>
        84
      </td>
      <td>
        0x54
      </td>
      <td>
        CCounter 1 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 1
      </td>
    </tr>
    <tr>
      <td>
        40086
      </td>
      <td>
        85
      </td>
      <td>
        0x55
      </td>
      <td>
        CCounter 1 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 1
      </td>
    </tr>
    <tr>
      <td>
        40087
      </td>
      <td>
        86
      </td>
      <td>
        0x56
      </td>
      <td>
        CCounter 2 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 2
      </td>
    </tr>
    <tr>
      <td>
        40088
      </td>
      <td>
        87
      </td>
      <td>
        0x57
      </td>
      <td>
        CCounter 2 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 2
      </td>
    </tr>
    <tr>
      <td>
        40089
      </td>
      <td>
        88
      </td>
      <td>
        0x58
      </td>
      <td>
        CCounter 3 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 3
      </td>
    </tr>
    <tr>
      <td>
        40090
      </td>
      <td>
        89
      </td>
      <td>
        0x59
      </td>
      <td>
        CCounter 3 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 3
      </td>
    </tr>
	<tr>
      <td>
        40091
      </td>
      <td>
        90
      </td>
      <td>
        0x5A
      </td>
      <td>
        CCounter 4 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 4
      </td>
    </tr>
    <tr>
      <td>
        40092
      </td>
      <td>
        91
      </td>
      <td>
        0x5B
      </td>
      <td>
        CCounter 4 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 4
      </td>
    </tr>
	<tr>
      <td>
        40093
      </td>
      <td>
        92
      </td>
      <td>
        0x5C
      </td>
      <td>
        CCounter 5 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 5
      </td>
    </tr>
    <tr>
      <td>
        40094
      </td>
      <td>
        93
      </td>
      <td>
        0x5D
      </td>
      <td>
        CCounter 5 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 5
      </td>
    </tr>
	<tr>
      <td>
        40095
      </td>
      <td>
        94
      </td>
      <td>
        0x5E
      </td>
      <td>
        CCounter 6 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 6
      </td>
    </tr>
    <tr>
      <td>
        40096
      </td>
      <td>
        95
      </td>
      <td>
        0x5F
      </td>
      <td>
        CCounter 6 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 6
      </td>
    </tr>
	<tr>
      <td>
        40097
      </td>
      <td>
        96
      </td>
      <td>
        0x60
      </td>
      <td>
        CCounter 7 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 7
      </td>
    </tr>
    <tr>
      <td>
        40098
      </td>
      <td>
        97
      </td>
      <td>
        0x61
      </td>
      <td>
        CCounter 7 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 7
      </td>
    </tr>
	<tr>
      <td>
        40099
      </td>
      <td>
        98
      </td>
      <td>
        0x62
      </td>
      <td>
        CCounter 8 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 8
      </td>
    </tr>
    <tr>
      <td>
        40100
      </td>
      <td>
        99
      </td>
      <td>
        0x63
      </td>
      <td>
        CCounter 8 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 8
      </td>
    </tr>
	<tr>
      <td>
        40117
      </td>
      <td>
        116
      </td>
      <td>
        0x74
      </td>
      <td>
        Counter Config 1
      </td>
      <td>
        Read &amp; Write
      </td>
      <td rowspan="8">
         Counter Configuration:
        <br>+1 – time measurement (if 
        <br>0 counting impulses)
        <br>+2 – autocatch counter 
        <br>every 1 sec
        <br>+4 – catch value when input 
        <br>low
        <br>+8 – reset counter after 
        <br>catch
        <br>+16 – reset counter if 
        <br>input low
        <br>+32 – encoder (only for counter 1 and 3)
      </td>
    </tr>
    <tr>
      <td>
        40118
      </td>
	  <td>
        117
      </td>
      <td>
        0x75
      </td>
      <td>
        Counter Config 2
      </td>
      <td>
        Read &amp; Write
      </td>
    </tr>
    <tr>
      <td>
        40119
      </td>
	  <td>
        118
      </td>
      <td>
        0x76
      </td>
      <td>
        Counter Config 3
      </td>
      <td>
        Read &amp; Write
      </td>
    </tr>
    <tr>
      <td>
        40120
      </td>
	  <td>
        119
      </td>
      <td>
        0x77
      </td>
      <td>
        Counter Config 4
      </td>
      <td>
        Read &amp; Write
      </td>
    </tr>
    <tr>
      <td>
        40121
      </td>
	  <td>
        120
      </td>
      <td>
        0x78
      </td>
      <td>
        Counter Config 5
      </td>
      <td>
        Read &amp; Write
      </td>
    </tr>
    <tr>
      <td>
        40122
      </td>
	  <td>
        121
      </td>
      <td>
        0x79
      </td>
      <td>
        Counter Config 6
      </td>
      <td>
        Read &amp; Write
      </td>
    </tr>
    <tr>
      <td>
        40123
      </td>
	  <td>
        122
      </td>
      <td>
        0x7A
      </td>
      <td>
        Counter Config 7
      </td>
      <td>
        Read &amp; Write
      </td>
    </tr>
    <tr>
      <td>
        40124
      </td>
	  <td>
        123
      </td>
      <td>
        0x7B
      </td>
      <td>
        Counter Config 8
      </td>
      <td>
        Read &amp; Write
      </td>
    </tr>
    <tr>
      <td>
        40133
      </td>
      <td>
        132
      </td>
      <td>
        0x84
      </td>
      <td>
        Catch
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Catch counter
      </td>
    </tr>
    <tr>
      <td>
        40134
      </td>
      <td>
        133
      </td>
      <td>
        0x85
      </td>
      <td>
        Status
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Captured counter
      </td>
    </tr>
  </tbody>
</table>

### Bit access

<table>
  <tr>
    <th>
      Address
    </th>
    <th>
      Address
      <br>DEC
    </th>
    <th>
      Address
      <br>HEX
    </th>
    <th>
      Register name
    </th>
    <th>
      Access
    </th>
    <th>
      Description
    </th>
  </tr>
  <tr>
    <td>
      193
    </td>
    <td>
      192
    </td>
    <td>
      0x0C0
    </td>
    <td>
      Default state of output 1
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 1
    </td>
  </tr>
  <tr>
    <td>
      194
    </td>
    <td>
      193
    </td>
    <td>
      0x0C1
    </td>
    <td>
      Default state of output 2
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 2
    </td>
  </tr>
   <tr>
    <td>
      195
    </td>
    <td>
      194
    </td>
    <td>
      0x0C2
    </td>
    <td>
      Default state of output 3
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 3
    </td>
  </tr>
   <tr>
    <td>
      196
    </td>
    <td>
      195
    </td>
    <td>
      0x0C3
    </td>
    <td>
      Default state of output 4
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 4
    </td>
  </tr>
   <tr>
    <td>
      197
    </td>
    <td>
      196
    </td>
    <td>
      0x0C4
    </td>
    <td>
      Default state of output 5
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 5
    </td>
  </tr>
   <tr>
    <td>
      198
    </td>
    <td>
      197
    </td>
    <td>
      0x0C5
    </td>
    <td>
      Default state of output 6
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 6
    </td>
  </tr>
   <tr>
    <td>
      199
    </td>
    <td>
      198
    </td>
    <td>
      0x0C6
    </td>
    <td>
      Default state of output 7
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 7
    </td>
  </tr>
   <tr>
    <td>
      200
    </td>
    <td>
      199
    </td>
    <td>
      0x0C7
    </td>
    <td>
      Default state of output 8
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Default state of output 8
    </td>
  </tr>
  <tr>
    <td>
      10801
    </td>
	<td>
      800
    </td>
    <td>
      0x320
    </td>
    <td>
      Input 1
    </td>
    <td>
      Read
    </td>
    <td>
      Input 1 state
    </td>
  </tr>
  <tr>
    <td>
      10802
    </td>
	<td>
      801
    </td>
    <td>
      0x321
    </td>
    <td>
      Input 2
    </td>
    <td>
      Read
    </td>
    <td>
      Input 2 state
    </td>
  </tr>
  <tr>
    <td>
      10803
    </td>
	<td>
      802
    </td>
    <td>
      0x322
    </td>
    <td>
      Input 3
    </td>
    <td>
      Read
    </td>
    <td>
      Input 3 state
    </td>
  </tr>
  <tr>
    <td>
      10804
    </td>
	<td>
      803
    </td>
    <td>
      0x323
    </td>
    <td>
      Input 4
    </td>
    <td>
      Read
    </td>
    <td>
      Input 4 state
    </td>
  </tr>
  <tr>
    <td>
      10805
    </td>
	<td>
      804
    </td>
    <td>
      0x324
    </td>
    <td>
      Input 5
     </td>
    <td>
      Read
    </td>
    <td>
      Input 5 state
    </td>
  </tr>
  <tr>
    <td>
      10806
    </td>
	<td>
      805
    </td>
    <td>
      0x325
    </td>
    <td>
      Input 6
    </td>
    <td>
      Read
    </td>
    <td>
      Input 6 state
    </td>
  </tr>
  <tr>
    <td>
      10807
    </td>
	<td>
      806
    </td>
    <td>
      0x326
    </td>
     <td>
      Input 7
    </td>
    <td>
      Read
    </td>
    <td>
      Input 7 state
    </td>
  </tr>
  <tr>
    <td>
      10808
    </td>
	<td>
      807
    </td>
    <td>
      0x327
    </td>
    <td>
      Input 8
    </td>
    <td>
      Read
    </td>
    <td>
      Input 8 state
    </td>
  </tr>
  <tr>
    <td>
      817
    </td>
    <td>
      816
    </td>
    <td>
      0x330
    </td>
    <td>
      Output 1
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 1 state
    </td>
  </tr>
  <tr>
    <td>
      818
    </td>
    <td>
      817
    </td>
    <td>
      0x331
    </td>
    <td>
      Output 2
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 2 state
    </td>
  </tr>
  <tr>
    <td>
      819
    </td>
    <td>
      818
    </td>
    <td>
      0x332
    </td>
    <td>
      Output 3
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 3 state
    </td>
  </tr>
  <tr>
    <td>
      820
    </td>
    <td>
      819
    </td>
    <td>
      0x333
    </td>
    <td>
      Output 4
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 4 state
    </td>
  </tr>
  <tr>
    <td>
      821
    </td>
    <td>
      820
    </td>
    <td>
      0x334
    </td>
    <td>
      Output 5
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 5 state
    </td>
  </tr>
  <tr>
    <td>
      822
    </td>
    <td>
      821
    </td>
    <td>
      0x335
    </td>
    <td>
      Output 6
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 6 state
    </td>
  </tr>
  <tr>
    <td>
      823
    </td>
    <td>
      822
    </td>
    <td>
      0x336
    </td>
    <td>
      Output 7
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 7 state
    </td>
  </tr>
  <tr>
    <td>
      824
    </td>
    <td>
      823
    </td>
    <td>
      0x337
    </td>
    <td>
      Output 8
    </td>
    <td>
      Read &amp; Write
    </td>
    <td>
      Output 8 state
    </td>
  </tr>
  <tr>
      <td>
        2113
      </td>
	  <td>
        2112
      </td>
      <td>
        0x840
      </td>
      <td>
        Capture 1
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Capture counter 1
      </td>
    </tr>
    <tr>
      <td>
        2114
      </td>
	  <td>
        2113
      </td>
      <td>
        0x841
      </td>
      <td>
        Capture 2
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Capture counter 2
      </td>
    </tr>
    <tr>
      <td>
        2115
      </td>
	  <td>
        2114
      </td>
      <td>
        0x842
      </td>
      <td>
        Capture 3
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Capture counter 3
      </td>
    </tr>
    <tr>
      <td>
        2116
      </td>
	  <td>
        2115
      </td>
      <td>
        0x843
      </td>
      <td>
        Capture 4
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Capture counter 4
      </td>
    </tr>
    <tr>
      <td>
        2117
      </td>
	  <td>
        2116
      </td>
      <td>
        0x844
      </td>
      <td>
        Capture 5
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Capture counter 5
      </td>
    </tr>
    <tr>
      <td>
        2118
      </td>
	  <td>
        2117
      </td>
      <td>
        0x845
      </td>
      <td>
        Capture 6
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Capture counter 6
      </td>
    </tr>
    <tr>
      <td>
        2119
      </td>
	  <td>
        2118
      </td>
      <td>
        0x846
      </td>
      <td>
        Capture 7
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Capture counter 7
      </td>
    </tr>
    <tr>
      <td>
        2120
      </td>
	  <td>
        2119
      </td>
      <td>
        0x847
      </td>
      <td>
        Capture 8
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Capture counter 8
      </td>
    </tr>
  <tr>
      <td>
        2129
      </td>
	  <td>
        2128
      </td>
      <td>
        0x850
      </td>
      <td>
        Captured 1
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Captured counter 1
      </td>
    </tr>
    <tr>
      <td>
        2130
      </td>
	  <td>
        2129
      </td>
      <td>
        0x851
      </td>
      <td>
        Captured 2
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Captured counter 2
      </td>
    </tr>
    <tr>
      <td>
        2131
      </td>
	  <td>
        2130
      </td>
      <td>
        0x852
      </td>
      <td>
        Captured 3
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Captured counter 3
      </td>
    </tr>
    <tr>
      <td>
        2132
      </td>
	  <td>
        2131
      </td>
      <td>
        0x853
      </td>
      <td>
        Captured 4
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Captured counter 4
      </td>
    </tr>
    <tr>
      <td>
        2133
      </td>
	  <td>
        2132
      </td>
      <td>
        0x854
      </td>
      <td>
        Captured 5
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Captured counter 5
      </td>
    </tr>
    <tr>
      <td>
        2134
      </td>
	  <td>
        2133
      </td>
      <td>
        0x855
      </td>
      <td>
        Captured 6
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Captured counter 6
      </td>
    </tr>
    <tr>
      <td>
        2135
      </td>
	  <td>
        2134
      </td>
      <td>
        0x856
      </td>
      <td>
        Captured 7
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Captured counter 7
      </td>
    </tr>
    <tr>
      <td>
        2136
      </td>
	  <td>
        2135
      </td>
      <td>
        0x857
      </td>
      <td>
        Captured 8
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Captured counter 8
      </td>
    </tr>
</table>

## Configuration software

Modbus Configurator is software that is designed to set the module registers responsible for communication over Modbus network as well as to read and write the current value of other registers of the module. This program can be a convenient way to test the system as well as to observe real-time changes in the registers.
Communication with the module is done via the USB cable. The module does not require any drivers.

Configurator is a universal program, whereby it is possible to configure all available modules.

![](images/MO-DIDO8-ST/configurator.png) {.img-responsive}

