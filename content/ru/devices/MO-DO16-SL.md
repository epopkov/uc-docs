# UC MO-DO16-SL {.header}

[UC MO-DO16-SL Модуль вывода](http://ru.unitecontrol.com/MO-DO16-SL.html), 16 цифровых каналов вывода Modbus RTU (RS485)

![](images/MO-DO16-SL/device.png) {.img-responsive}

## Инструкция пользователя

## Указание мер безопасности

* По способу защиты от поражения электрическим током модуль соответствует классу II по ГОСТ 12.2.007.0-75.
* При эксплуатации и техническом обслуживании необходимо соблюдать требования ГОСТ 12.3.019-80, “Правил эксплуатации электроустановок потребителей” и “Правил охраны труда при эксплуатации электроустановок потребителей”.
* Открытые контакты клемм модуля при эксплуатации находятся под напряжением величиной до 30 В. Любые подключения к модулю и работы по его техническому обслуживанию производятся только при отключенном питании модуля.
* Не допускается попадание влаги на контакты выходных соединителей и внутренние элементы модуля. Запрещается использование модуля при наличии в атмосфере кислот, щелочей, масел и иных агрессивных веществ.
* Подключение, регулировка и техническое обслуживание модуля должны производится только квалифицированными специалистами, изучившими настоящее руководство по эксплуатации.

## Характеристика модуля

### Предназначение и описание модуля

- Название: Модуль вывода, 16 цифровых каналов вывода, Modbus RTU/ASCII (RS485).
- Артикул: MO-DO16-SL.
- Модуль дискретного вывода MO-DO16 Modbus подключается по шине Modbus в режиме Slave.
- Модуль имеет 16 цифровых выходов для подключения внешних устройств. Модуль производится в двух версиях с выходами типа PNP-переход выходного каскада транзисторов и NPN-переход: это позволяет подключать MO-DO16 в любые схемы питания оборудования, подключенного к выводам.
<br>Обозначение:
<br>Тип выходов PNP: MO-DO16-SL (-PNP)
<br>Тип выходов NPN: MO DO16-SL-NPN
- Модуль подключается к магистрали RS485 с помощью двухпроводной витой пары. Коммуниукация осуществляется по протоколу Modbus RTU/ASCII.
- Модуль предназначен для монтажа на шине DIN в соответствие со стандартом DIN EN 5002.
- Для индикации состояния модуль снабжен набором светодиодов, расположенных на корпусе устройства.
- Конфигурация устройства производится двумя способами: с помощью интерфейса USB и с помощью протокола Modbus.

### Спецификация

<table>
  <tr>
    <td>
      Питание
    </td>
    <td>
      Напряжение
    </td>
    <td>
      12-24 В DC±20%
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Максимальная сила тока
    </td>
    <td>
      180 мА @ 12 В / 100 мА @ 24 В
    </td>
  </tr>
  <tr>
    <td>
      Скорость связи
    </td>
    <td>
    </td>
    <td>
      2400 - 115200 бит/с
    </td>
  </tr>
  <tr>
    <td>
      Цифровые выходы
    </td>
    <td>
      Количество выходов
    </td>
    <td>
      16
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Максимальное напряжение
    </td>
    <td>
      30 В
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Максимальная сила тока на одном выходе
    </td>
    <td>
      500 мА
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Тип выхода
    </td>
    <td>
      PNP or NPN (depends on version)
    </td>
  </tr>
  <tr>
    <td>
      Температура
    </td>
    <td>
      Работы
    </td>
    <td>
      -10°C ÷ +50°C
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Хранения
    </td>
    <td>
      -40°C ÷ +85°C
    </td>
  </tr>
  <tr>
    <td>
      Разъемы
    </td>
    <td>
      Питания
    </td>
    <td>
      2 пин
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Коммуникации
    </td>
    <td>
      3 пин
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Выхода
    </td>
    <td>
      10 пин
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Quick Connector
    </td>
    <td>
      IDC10
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Конфигурационные
    </td>
    <td>
      MiniUSB
    </td>
  </tr>
  <tr>
    <td>
      Размеры
    </td>
    <td>
      Длина
    </td>
    <td>
      120 мм
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Высота
    </td>
    <td>
      101 мм
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Глубина
    </td>
    <td>
      22,5 мм
    </td>
  </tr>
  <tr>
    <td>
      Интерфейс
    </td>
    <td>
      RS485
    </td>
    <td>
      До 128 устройств
    </td>
  </tr>
</table>

### Габариты модуля

Внешний вид и размеры модуля представлены на рисунке. Модуль фиксируется на шине DIN. Разъемы питания, коммуникационные, входа находятся снизу и сверху модуля. Разъемы конфигурационные USB и индикаторы находятся на передней панели модуля.

![](images/MO-DO16-SL/overview.png) {.img-responsive}

## Конфигурация связи

### Заземление и экранирование

Монтаж модуля вблизи других устройств, генерирующих электромагнитное излучение (реле, контакторы, трансформаторы, электроконтроллеры и т.д.) может вызвать электрические помехи питания устройства и сигнальных проводов, а также негативные последствия для системы. Правильное заземление, а также другие меры безопасности необходимо произвести на этапе установки. Меры безопасности: заземление блока управления, заземление модуля, заземление экранированных проводников, защита переключающих устройств, правильно подобранные типы кабелей и сечения проводников.

### Терминатор

Эффекты передающей линии часто вызывают проблемы в информационно- коммуникационных сетях. Эти проблемы часто касаются подавления отраженного сигнала в сети. Чтобы ликвидировать наличие отражений на концах кабелей, необходимо на двух концах кабеля использовать резистор с сопротивлением, равным сопротивлению данной линии. В случае с витой парой, типовое значение - 120 Ом.

### Определение адреса модуля в сети

В Таблице 1 представлен способ настройки переключателей для определения адреса модуля. С помощью переключателей возможна настройка адреса от 0 до 31. Адреса от 32 до 255 можно настраивать с помощью интерфейса RS485 или USB.

<table>
  <tbody>
    <tr valign="TOP">
      <td bgcolor="#cfe2f3">
        Adr
      </td>
      <td>
        SW5
      </td>
      <td>
        SW4
      </td>
      <td>
        SW3
      </td>
      <td>
        SW2
      </td>
      <td>
        SW1
      </td>
      <td bgcolor="#cfe2f3">
        Adr
      </td>
      <td>
        SW5
      </td>
      <td>
        SW4
      </td>
      <td>
        SW3
      </td>
      <td>
        SW2
      </td>
      <td>
        SW1
      </td>
      <td bgcolor="#cfe2f3">
        Adr
      </td>
      <td>
        SW5
      </td>
      <td>
        SW4
      </td>
      <td>
        SW3
      </td>
      <td>
        SW2
      </td>
      <td>
        SW1
      </td>
    </tr>
    <tr valign="TOP">
      <td bgcolor="#cfe2f3">
        0
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        11
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        22
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
    </tr>
    <tr valign="TOP">
      <td bgcolor="#cfe2f3">
        1
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        12
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        23
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
    </tr>
    <tr valign="TOP">
      <td bgcolor="#cfe2f3">
        2
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        13
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        24
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
    </tr>
    <tr valign="TOP">
      <td bgcolor="#cfe2f3">
        3
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        14
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        25
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
    </tr>
    <tr valign="TOP">
      <td bgcolor="#cfe2f3">
        4
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        15
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        26
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
    </tr>
    <tr valign="TOP">
      <td bgcolor="#cfe2f3">
        5
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        16
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        27
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
    </tr>
    <tr valign="TOP">
      <td bgcolor="#cfe2f3">
        6
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        17
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        28
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
    </tr>
    <tr valign="TOP">
      <td bgcolor="#cfe2f3">
        7
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        18
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        29
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
    </tr>
    <tr valign="TOP">
      <td bgcolor="#cfe2f3">
        8
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        19
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        30
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
    </tr>
    <tr valign="TOP">
      <td bgcolor="#cfe2f3">
        9
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        20
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        31
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
    </tr>
    <tr valign="TOP">
      <td bgcolor="#cfe2f3">
        10
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        21
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td>
      </td>
      <td>
      </td>
      <td>
      </td>
      <td>
      </td>
      <td>
      </td>
      <td>
      </td>
    </tr>
  </tbody>
</table>

### Типы регистров Modbus

В модуле есть 4 типа доступных переменных.

<table>
  <tr>
    <th>
      <b>Тип</b>
    </th>
    <th>
      <b>Начальный адрес</b>
    </th>
    <th>
      <b>Переменная</b>
    </th>
    <th>
      <b>Доступ</b>
    </th>
    <th>
      <b>Команда Modbus</b>
    </th>
  </tr>
  <tr>
    <td>
      1
    </td>
    <td>
      00001
    </td>
    <td>
      Coils
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      1, 5, 15
    </td>
  </tr>
  <tr>
    <td>
      2
    </td>
    <td>
      10001
    </td>
    <td>
      Discrete inputs
    </td>
    <td>
      Чтение
    </td>
    <td>
      2
    </td>
  </tr>
  <tr>
    <td>
      3
    </td>
    <td>
      30001
    </td>
    <td>
      Input registers
    </td>
    <td>
      Чтение
    </td>
    <td>
      3
    </td>
  </tr>
  <tr>
    <td>
      4
    </td>
    <td>
      40001
    </td>
    <td>
      Holding registers
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      4, 6, 16
    </td>
  </tr>
</table>

### Настройка коммуникации

Данные в модулях хранятся в 16 битных регистрах. Доступ к регистрам производится посредством протокола Modbus RTU или Modbus ASCII.

#### Параметры по умолчанию

Конфигурацию по умолчанию можно вернуть с помощью переключателя SW6.

<table>
  <tr>
    <td>
      Скорость передачи, бит/с
    </td>
    <td>
      19200
    </td>
  </tr>
  <tr>
    <td>
      Четность
    </td>
    <td>
      Нет
    </td>
  </tr>
  <tr>
    <td>
      Кол-во стоповых битов
    </td>
    <td>
      1
    </td>
  </tr>
  <tr>
    <td>
      Задержка ответа, мс
    </td>
    <td>
      0
    </td>
  </tr>
  <tr>
    <td>
      Режим Modbus
    </td>
    <td>
      RTU
    </td>
  </tr>
</table>

#### Восстановление конфигурации по умолчанию

Для восстановления конфигурации по умолчанию необходимо при выключенном питании модуля переключить SW6, а затем включить питание. В модуле должны начать мигать индикаторы питания (светодиоды) и коммуникации. Все настройки будут перезаписаны. Внимание! Во время восстановления конфигурации по умолчанию будут удалены все значения записанные в регистрах модуля!

#### Конфигурационные регистры

<table>
  <tr>
    <th>
      Адрес
    </th>
    <th>
      Название
    </th>
    <th>
      Значения
    </th>
  </tr>
  <tr>
    <td>
      40003
    </td>
    <td>
      Скорость передачи
    </td>
    <td>
      0 - 2400
      <br>1 - 4800
      <br>2 - 9600
      <br>3 - 19200
      <br>4 - 38400
      <br>5 - 57600
      <br>6 - 115200
      <br>другое значение - значение * 10
    </td>
  </tr>
  <tr>
    <td>
      40005
    </td>
    <td>
      Четность
    </td>
    <td>
      0 - отсутствие
      <br>1 - нечетность
      <br>2 - четность
      <br>3 - всегда 1
      <br>4 - всегда 0
    </td>
  </tr>
  <tr>
    <td>
      40004
    </td>
    <td>
      Стоповые биты LSB
    </td>
    <td>
      1 - один стоповый бит
      <br>2 - 2 стоповых бита
    </td>
  </tr>
  <tr>
    <td>
      40004
    </td>
    <td>
      Стоповые биты MSB
    </td>
    <td>
      7 - 7 битов данных
      <br>8 - 8 битов данных
    </td>
  </tr>
  <tr>
    <td>
      40005
    </td>
    <td>
      Задержка ответа
    </td>
    <td>
      Время в мс
    </td>
  </tr>
  <tr>
    <td>
      40007
    </td>
    <td>
      Режим Modbus
    </td>
    <td>
      0 - RTU
      <br>1 - ASCII
    </td>
  </tr>
</table>

## Светодиодные индикаторы

![](images/MO-DO16-SL/leds.png) {.img-responsive}

<table>
  <tr>
    <th>
      Указатель
    </th>
    <th>
      Описание
    </th>
  </tr>
  <tr>
    <td>
      Питание
    </td>
    <td>
      Включенный светодиод означает, что модуль правильно подключен к источнику питания.
    </td>
  </tr>
  <tr>
    <td>
      Связь
    </td>
    <td>
      Светодиод включается, когда модуль получает правильный пакет данных и высылает ответ.
    </td>
  </tr>
  <tr>
    <td>
      Состояния выходов
    </td>
    <td>
      Включенный светодиод означает, что выход подсоединен.
    </td>
  </tr>
</table>

## Подключение модуля - тип выхода PNP

![](images/MO-DO16-SL/connection1.jpg) {.img-responsive}

## Подключение модуля - тип выхода NPN

![](images/MO-DO16-SL/connection2.jpg) {.img-responsive}

## Настройка переключателей

![](images/MO-DO16-SL/switch.png) {.img-responsive}

<table>
  <tr>
    <th>
      Переключатель
    </th>
    <th>
      Функция
    </th>
    <th>
      Описание
    </th>
  </tr>
  <tr>
    <td>
      1
    </td>
    <td>
      Адрес модуля +1
    </td>
    <td rowspan="5">
      Настройка адреса модуля в диапазоне от 0 до 31
    </td>
  </tr>
  <tr>
    <td>
      2
    </td>
    <td>
      Адрес модуля +2
    </td>
  </tr>
  <tr>
    <td>
      3
    </td>
    <td>
      Адрес модуля +4
    </td>
  </tr>
  <tr>
    <td>
      4
    </td>
    <td>
      Адрес модуля +8
    </td>
  </tr>
  <tr>
    <td>
      5
    </td>
    <td>
      Адрес модуля +16
    </td>
  </tr>
  <tr>
    <td>
      6
    </td>
    <td>
      Настройка модуля по умолчанию
    </td>
    <td>
      Настройка параметров передачи по умолчанию (см. параметры по умолчанию)
    </td>
  </tr>
</table>

## Регистры модуля

### Регистровый доступ

<table>
  <thead>
    <tr>
      <th>
        Адрес
      </th>
      <th>
        Адрес
        <br>DEC
      </th>
      <th>
        Адрес
        <br>HEX
      </th>
      <th>
        Название регистра
      </th>
      <th>
        Доступ
      </th>
      <th>
        Описание
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        30001
      </td>
      <td>
        0
      </td>
      <td>
        0x00
      </td>
      <td>
        Версия/Тип
      </td>
      <td>
        Чтение
      </td>
      <td>
        Тип и версия устройства
      </td>
    </tr>
    <tr>
      <td>
        30002
      </td>
      <td>
        1
      </td>
      <td>
        0x01
      </td>
      <td>
        Переключатели
      </td>
      <td>
        Чтение
      </td>
      <td>
        Состояние переключателей
      </td>
    </tr>
    <tr>
      <td>
        40003
      </td>
      <td>
        2
      </td>
      <td>
        0x02
      </td>
      <td>
        Скорость
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Скорость передачи
      </td>
    </tr>
    <tr>
      <td>
        40004
      </td>
      <td>
        3
      </td>
      <td>
        0x03
      </td>
      <td>
        Стоповые биты
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Кол-во стоповых битов
      </td>
    </tr>
    <tr>
      <td>
        40005
      </td>
      <td>
        4
      </td>
      <td>
        0x04
      </td>
      <td>
        Четность
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Бит четности
      </td>
    </tr>
    <tr>
      <td>
        40006
      </td>
      <td>
        5
      </td>
      <td>
        0x05
      </td>
      <td>
        Опоздание
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Опоздание ответа
      </td>
    </tr>
    <tr>
      <td>
        40007
      </td>
      <td>
        6
      </td>
      <td>
        0x06
      </td>
      <td>
        Тип Modbus
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Тип протокола Modbus
      </td>
    </tr>
    <tr>
      <td>
        40009
      </td>
      <td>
        8
      </td>
      <td>
        0x08
      </td>
      <td>
        Watchdog
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Watchdog
      </td>
    </tr>
    <tr>
      <td>
        40013
      </td>
      <td>
        12
      </td>
      <td>
        0x0C
      </td>
      <td>
        Состояние выходов по умолчанию
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выходов по умолчанию
      </td>
    </tr>
    <tr>
      <td>
        40033
      </td>
      <td>
        32
      </td>
      <td>
        0x20
      </td>
      <td>
        Полученные пакеты MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Кол-во полученных пакетов
      </td>
    </tr>
    <tr>
      <td>
        40034
      </td>
      <td>
        33
      </td>
      <td>
        0x21
      </td>
      <td>
        Полученные пакеты LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Кол-во полученных пакетов
      </td>
    </tr>
    <tr>
      <td>
        40035
      </td>
      <td>
        34
      </td>
      <td>
        0x22
      </td>
      <td>
        Ошибочные пакеты MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Кол-во полученных ошибочных пакетов
      </td>
    </tr>
    <tr>
      <td>
        40036
      </td>
      <td>
        35
      </td>
      <td>
        0x23
      </td>
      <td>
        Ошибочные пакеты LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Кол-во полученных ошибочных пакетов
      </td>
    </tr>
    <tr>
      <td>
        40037
      </td>
      <td>
        36
      </td>
      <td>
        0x24
      </td>
      <td>
        Высланные пакеты MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Кол-во высланных пакетов
      </td>
    </tr>
    <tr>
      <td>
        40038
      </td>
      <td>
        37
      </td>
      <td>
        0x25
      </td>
      <td>
        Высланные пакеты LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Кол-во высланных пакетов
      </td>
    </tr>
    <tr>
      <td>
        40052
      </td>
      <td>
        51
      </td>
      <td>
        0x33
      </td>
      <td>
        Выходы
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выходов
      </td>
    </tr>
  </tbody>
</table>

### Побитовый доступ

<table>
  <tr>
    <th>
      Адрес
    </th>
    <th>
      Адрес
      <br>DEC
    </th>
    <th>
      Адрес
      <br>HEX
    </th>
    <th>
      Название регистра
    </th>
    <th>
      Доступ
    </th>
    <th>
      Описание
    </th>
  </tr>
  <tr>
    <td>
      193
    </td>
    <td>
      192
    </td>
    <td>
      0x0C0
    </td>
    <td>
      Состояние выхода по умолчанию 1
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 1
    </td>
  </tr>
  <tr>
    <td>
      194
    </td>
    <td>
      193
    </td>
    <td>
      0x0C1
    </td>
    <td>
      Состояние выхода по умолчанию 2
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 2
    </td>
  </tr>
  <tr>
    <td>
      195
    </td>
    <td>
      194
    </td>
    <td>
      0x0C2
    </td>
    <td>
      Состояние выхода по умолчанию 3
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 3
    </td>
  </tr>
  <tr>
    <td>
      196
    </td>
    <td>
      195
    </td>
    <td>
      0x0C3
    </td>
    <td>
      Состояние выхода по умолчанию 4
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 4
    </td>
  </tr>
  <tr>
    <td>
      197
    </td>
    <td>
      196
    </td>
    <td>
      0x0C4
    </td>
    <td>
      Состояние выхода по умолчанию 5
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 5
    </td>
  </tr>
  <tr>
    <td>
      198
    </td>
    <td>
      197
    </td>
    <td>
      0x0C5
    </td>
    <td>
      Состояние выхода по умолчанию 6
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 6
    </td>
  </tr>
  <tr>
    <td>
      199
    </td>
    <td>
      198
    </td>
    <td>
      0x0C6
    </td>
    <td>
      Состояние выхода по умолчанию 7
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 7
    </td>
  </tr>
  <tr>
    <td>
      200
    </td>
    <td>
      199
    </td>
    <td>
      0x0C7
    </td>
    <td>
      Состояние выхода по умолчанию 8
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 8
    </td>
  </tr>
  <tr>
    <td>
      201
    </td>
    <td>
      200
    </td>
    <td>
      0x0C8
    </td>
    <td>
      Состояние выхода по умолчанию 9
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 9
    </td>
  </tr>
  <tr>
    <td>
      202
    </td>
    <td>
      201
    </td>
    <td>
      0x0C9
    </td>
    <td>
      Состояние выхода по умолчанию 10
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 10
    </td>
  </tr>
  <tr>
    <td>
      203
    </td>
    <td>
      202
    </td>
    <td>
      0x0CA
    </td>
    <td>
      Состояние выхода по умолчанию 11
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 11
    </td>
  </tr>
  <tr>
    <td>
      204
    </td>
    <td>
      203
    </td>
    <td>
      0x0CB
    </td>
    <td>
      Состояние выхода по умолчанию 12
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 12
    </td>
  </tr>
  <tr>
    <td>
      205
    </td>
    <td>
      204
    </td>
    <td>
      0x0CC
    </td>
    <td>
      Состояние выхода по умолчанию 13
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 13
    </td>
  </tr>
  <tr>
    <td>
      206
    </td>
    <td>
      205
    </td>
    <td>
      0x0CD
    </td>
    <td>
      Состояние выхода по умолчанию 14
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 14
    </td>
  </tr>
  <tr>
    <td>
      207
    </td>
    <td>
      206
    </td>
    <td>
      0x0CE
    </td>
    <td>
      Состояние выхода по умолчанию 15
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 15
    </td>
  </tr>
  <tr>
    <td>
      208
    </td>
    <td>
      207
    </td>
    <td>
      0x0CF
    </td>
    <td>
      Состояние выхода по умолчанию 16
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода по умолчанию 16
    </td>
  </tr>
  <tr>
    <td>
      817
    </td>
    <td>
      816
    </td>
    <td>
      0x330
    </td>
    <td>
      Выход 1
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 1
    </td>
  </tr>
  <tr>
    <td>
      818
    </td>
    <td>
      817
    </td>
    <td>
      0x331
    </td>
    <td>
      Выход 2
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 2
    </td>
  </tr>
  <tr>
    <td>
      819
    </td>
    <td>
      818
    </td>
    <td>
      0x332
    </td>
    <td>
      Выход 3
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 3
    </td>
  </tr>
  <tr>
    <td>
      820
    </td>
    <td>
      819
    </td>
    <td>
      0x333
    </td>
    <td>
      Выход 4
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 4
    </td>
  </tr>
  <tr>
    <td>
      821
    </td>
    <td>
      820
    </td>
    <td>
      0x334
    </td>
    <td>
      Выход 5
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 5
    </td>
  </tr>
  <tr>
    <td>
      822
    </td>
    <td>
      821
    </td>
    <td>
      0x335
    </td>
    <td>
      Выход 6
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 6
    </td>
  </tr>
  <tr>
    <td>
      823
    </td>
    <td>
      822
    </td>
    <td>
      0x336
    </td>
    <td>
      Выход 7
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 7
    </td>
  </tr>
  <tr>
    <td>
      824
    </td>
    <td>
      823
    </td>
    <td>
      0x337
    </td>
    <td>
      Выход 8
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 8
    </td>
  </tr>
  <tr>
    <td>
      825
    </td>
    <td>
      824
    </td>
    <td>
      0x338
    </td>
    <td>
      Выход 9
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 9
    </td>
  </tr>
  <tr>
    <td>
      826
    </td>
    <td>
      825
    </td>
    <td>
      0x339
    </td>
    <td>
      Выход 10
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 10
    </td>
  </tr>
  <tr>
    <td>
      827
    </td>
    <td>
      826
    </td>
    <td>
      0x33A
    </td>
    <td>
      Выход 11
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 11
    </td>
  </tr>
  <tr>
    <td>
      828
    </td>
    <td>
      827
    </td>
    <td>
      0x33B
    </td>
    <td>
      Выход 12
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 12
    </td>
  </tr>
  <tr>
    <td>
      829
    </td>
    <td>
      828
    </td>
    <td>
      0x33C
    </td>
    <td>
      Выход 13
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 13
    </td>
  </tr>
  <tr>
    <td>
      830
    </td>
    <td>
      829
    </td>
    <td>
      0x33D
    </td>
    <td>
      Выход 14
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 14
    </td>
  </tr>
  <tr>
    <td>
      831
    </td>
    <td>
      830
    </td>
    <td>
      0x33E
    </td>
    <td>
      Выход 15
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 15
    </td>
  </tr>
  <tr>
    <td>
      832
    </td>
    <td>
      831
    </td>
    <td>
      0x33F
    </td>
    <td>
      Выход 16
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      Состояние выхода 16
    </td>
  </tr>
</table>

## Конфигурационная программа

Программное обеспечение - “Конфигуратор” - служит для настройки регистров ответственных за коммуникацию модуля с шиной Modbus, а также для чтения и записи текущих значений остальных регистров модуля. Благодоря программе также можно удобным способом тестировать систему и следить за изменениями в регистрах в режиме реального времени. Коммуникация с модулем производится посредством кабеля USB. Для взаимодействия программного обеспечения с модулем нет необходимости устанавливать какие-либо драйверы.

![](images/MO-DO16-SL/configurator.png) {.img-responsive}

