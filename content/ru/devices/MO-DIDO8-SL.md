# UC MO-DIDO8-SL {.header}

[UC MO-DIDO8-SL Модуль ввода-вывода](http://ru.unitecontrol.com/MO-DIDO8-SL.html), 8 цифровых каналов ввода и 8 цифровых каналов вывода Modbus RTU (RS485)

![](images/MO-DIDO8-SL/device.png) {.img-responsive}

## Инструкция пользователя

## Указание мер безопасности

* По способу защиты от поражения электрическим током модуль соответствует классу II по ГОСТ 12.2.007.0-75.
* При эксплуатации и техническом обслуживании необходимо соблюдать требования ГОСТ 12.3.019-80, “Правил эксплуатации электроустановок потребителей” и “Правил охраны труда при эксплуатации электроустановок потребителей”.
* Открытые контакты клемм модуля при эксплуатации находятся под напряжением величиной до 30 В. Любые подключения к модулю и работы по его техническому обслуживанию производятся только при отключенном питании модуля.
* Не допускается попадание влаги на контакты выходных соединителей и внутренние элементы модуля. Запрещается использование модуля при наличии в атмосфере кислот, щелочей, масел и иных агрессивных веществ.
* Подключение, регулировка и техническое обслуживание модуля должны производится только квалифицированными специалистами, изучившими настоящее руководство по эксплуатации.

## Характеристика модуля

### Предназначение и описание модуля

- Название: Модуль ввода-вывода, 8 цифровых каналов ввода и 8 цифровых каналов вывода, Modbus RTU/ASCII (RS485).
- Артикул: MO-DIDO8-SL.
- Модуль имеет 8 цифровых входов с опцией счётчика. Дополнительно клеммы IN1 и IN2 а также IN3 и IN4 можно использовать для подключения двух шифраторов (encoder). Каждый канал может быть индивидуально сконфигурирован в один из нескольких режимов. Все входы оптоизолированы.
- Модуль имеет 8 цифровых выходов для подключения внешних устройств.
- Модуль подключается к магистрали RS485 с помощью двухпроводной витой пары. Коммуниукация осуществляется по протоколу Modbus RTU/ASCII.
- Модуль предназначен для монтажа на шине DIN в соответствие со стандартом DIN EN 5002.
- Для индикации состояния модуль снабжен набором светодиодов, расположенных на корпусе устройства.
- Конфигурация устройства производится двумя способами: с помощью интерфейса USB и с помощью протокола Modbus.

### Спецификация

<table>
  <tr>
    <td>
      Питание
    </td>
    <td>
      Напряжение
    </td>
    <td>
      12-24 В DC±20%
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Максимальная сила тока
    </td>
    <td>
      200 мА @ 12 В / 100 мА @ 24 В
    </td>
  </tr>
  <tr>
    <td>
      Скорость связи
    </td>
    <td>
    </td>
    <td>
      2400 - 115200 бит/с
    </td>
  </tr>
  <tr>
    <td>
      Цифровые входы
    </td>
    <td>
      Количество входов
    </td>
    <td>
      8
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Диапазон напряжений
    </td>
    <td>
      0 – 30 В
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Уровень “0”
    </td>
    <td>
      0 – 3 В
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Уровень “1”
    </td>
    <td>
      6 – 30 В
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Входное сопротивление
    </td>
    <td>
      4 кОм
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Изоляция
    </td>
    <td>
      1500 В rms
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Тип входов
    </td>
    <td>
      PNP or NPN
    </td>
  </tr>
  <tr>
    <td>
      Цифровые выходы
    </td>
    <td>
      Количество выходов
    </td>
    <td>
      8
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Максимальное напряжение
    </td>
    <td>
      30 В
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Максимальная сила тока
    </td>
    <td>
      500 мА
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Тип выхода
    </td>
    <td>
      PNP
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Защита выходов
    </td>
    <td>
      4 A предохранитель
    </td>
  </tr>
  <tr>
    <td>
      Счетчики
    </td>
    <td>
      Количество
    </td>
    <td>
      8
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Разрешение
    </td>
    <td>
      32 бита
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Частота
    </td>
    <td>
      1 кГц (max)
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Ширина импульса
    </td>
    <td>
      500 мкс (min)
    </td>
  </tr>
  <tr>
    <td>
      Температура
    </td>
    <td>
      Рабочая
    </td>
    <td>
      -10°C ÷ +50°C
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Хранения
    </td>
    <td>
      -40°C ÷ +85°C
    </td>
  </tr>
  <tr>
    <td>
      Разъемы
    </td>
    <td>
      Питания
    </td>
    <td>
      2 пин
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Коммуникации
    </td>
    <td>
      3 пин
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Входа
    </td>
    <td>
      10 пин
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Выхода
    </td>
    <td>
      10 пин
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Quick Connector
    </td>
    <td>
      IDC10
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Конфигурационные
    </td>
    <td>
      MiniUSB
    </td>
  </tr>
  <tr>
    <td>
      Габариты
    </td>
    <td>
      Длина
    </td>
    <td>
      120 мм
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Высота
    </td>
    <td>
      101 мм
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Глубина
    </td>
    <td>
      22,5 мм
    </td>
  </tr>
  <tr>
    <td>
      Интерфейс
    </td>
    <td>
      RS485
    </td>
    <td>
      До 128 устройств
    </td>
  </tr>
</table>

### Габариты модуля

Внешний вид и размеры модуля представлены на рисунке. Модуль фиксируется на шине DIN. Разъемы питания, коммуникационные, входа находятся снизу и сверху модуля. Разъемы конфигурационные USB и индикаторы находятся на передней панели модуля.

![](images/MO-DIDO8-SL/overview.png) {.img-responsive}

## Конфигурация связи

### Заземление и экранирование

Монтаж модуля вблизи других устройств, генерирующих электромагнитное излучение (реле, контакторы, трансформаторы, электроконтроллеры и т.д.) может вызвать электрические помехи питания устройства и сигнальных проводов, а также негативные последствия для системы. Правильное заземление, а также другие меры безопасности необходимо произвести на этапе установки. Меры безопасности: заземление блока управления, заземление модуля, заземление экранированных проводников, защита переключающих устройств, правильно подобранные типы кабелей и сечения проводников.

### Терминатор

Эффекты передающей линии часто вызывают проблемы в информационно- коммуникационных сетях. Эти проблемы часто касаются подавления отраженного сигнала в сети. Чтобы ликвидировать наличие отражений на концах кабелей, необходимо на двух концах кабеля использовать резистор с сопротивлением, равным сопротивлению данной линии. В случае с витой парой, типовое значение - 120 Ом.

### Определение адреса модуля в сети

В Таблице 1 представлен способ настройки переключателей для определения адреса модуля. С помощью переключателей возможна настройка адреса от 0 до 31. Адреса от 32 до 255 можно настраивать с помощью интерфейса USB.

<table>
  <tbody>
    <tr>
      <td bgcolor="#cfe2f3">
        Adr
      </td>
      <td>
        SW5
      </td>
      <td>
        SW4
      </td>
      <td>
        SW3
      </td>
      <td>
        SW2
      </td>
      <td>
        SW1
      </td>
      <td bgcolor="#cfe2f3">
        Adr
      </td>
      <td>
        SW5
      </td>
      <td>
        SW4
      </td>
      <td>
        SW3
      </td>
      <td>
        SW2
      </td>
      <td>
        SW1
      </td>
      <td bgcolor="#cfe2f3">
        Adr
      </td>
      <td>
        SW5
      </td>
      <td>
        SW4
      </td>
      <td>
        SW3
      </td>
      <td>
        SW2
      </td>
      <td>
        SW1
      </td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">
        0
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        11
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        22
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">
        1
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        12
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        23
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">
        2
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        13
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        24
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">
        3
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        14
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        25
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">
        4
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        15
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        26
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">
        5
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        16
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        27
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">
        6
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        17
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        28
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">
        7
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        18
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        29
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">
        8
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        19
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        30
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">
        9
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#cfe2f3">
        20
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        31
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
    </tr>
    <tr>
      <td bgcolor="#cfe2f3">
        10
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#cfe2f3">
        21
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td bgcolor="#f4cccc">
        OFF
      </td>
      <td bgcolor="#b6d7a8">
        ON
      </td>
      <td>
      </td>
      <td>
      </td>
      <td>
      </td>
      <td>
      </td>
      <td>
      </td>
      <td>
      </td>
    </tr>
  </tbody>
</table>

### Типы регистров Modbus

В модуле есть 4 типа доступных переменных.

<table>
  <tr>
    <th>
      <b>Тип</b>
    </th>
    <th>
      <b>Начальный адрес</b>
    </th>
    <th>
      <b>Переменная</b>
    </th>
    <th>
      <b>Доступ</b>
    </th>
    <th>
      <b>Команда Modbus</b>
    </th>
  </tr>
  <tr>
    <td>
      1
    </td>
    <td>
      00001
    </td>
    <td>
      Coils
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      1, 5, 15
    </td>
  </tr>
  <tr>
    <td>
      2
    </td>
    <td>
      10001
    </td>
    <td>
      Discrete inputs
    </td>
    <td>
      Чтение
    </td>
    <td>
      2
    </td>
  </tr>
  <tr>
    <td>
      3
    </td>
    <td>
      30001
    </td>
    <td>
      Input registers
    </td>
    <td>
      Чтение
    </td>
    <td>
      3
    </td>
  </tr>
  <tr>
    <td>
      4
    </td>
    <td>
      40001
    </td>
    <td>
      Holding registers
    </td>
    <td>
      Чтение и запись
    </td>
    <td>
      4, 6, 16
    </td>
  </tr>
</table>

### Настройка коммуникации

Данные в модулях хранятся в 16 битных регистрах. Доступ к регистрам производится посредством протокола Modbus RTU или Modbus ASCII.

#### Параметры по умолчанию

Конфигурацию по умолчанию можно вернуть с помощью переключателя SW6.

<table>
  <tr>
    <td>
      Скорость передачи, бит/с
    </td>
    <td>
      19200
    </td>
  </tr>
  <tr>
    <td>
      Четность
    </td>
    <td>
      Нет
    </td>
  </tr>
  <tr>
    <td>
      Кол-во стоповых битов
    </td>
    <td>
      1
    </td>
  </tr>
  <tr>
    <td>
      Задержка ответа, мс
    </td>
    <td>
      0
    </td>
  </tr>
  <tr>
    <td>
      Режим Modbus
    </td>
    <td>
      RTU
    </td>
  </tr>
</table>

#### Восстановление конфигурации по умолчанию

Для восстановления конфигурации по умолчанию необходимо при выключенном питании модуля переключить SW6, а затем включить питание. В модуле должны начать мигать индикаторы питания (светодиоды) и коммуникации. Все настройки будут перезаписаны. Внимание! Во время восстановления конфигурации по умолчанию будут удалены все значения записанные в регистрах модуля!

#### Конфигурационные регистры

<table>
  <tr>
    <th>
      Адрес
    </th>
    <th>
      Название
    </th>
    <th>
      Значения
    </th>
  </tr>
  <tr>
    <td>
      40003
    </td>
    <td>
      Скорость передачи
    </td>
    <td>
      0 - 2400
      <br>1 - 4800
      <br>2 - 9600
      <br>3 - 19200
      <br>4 - 38400
      <br>5 - 57600
      <br>6 - 115200
      <br>другое значение - значение * 10
    </td>
  </tr>
  <tr>
    <td>
      40005
    </td>
    <td>
      Четность
    </td>
    <td>
      0 - отсутствие
      <br>1 - нечетность
      <br>2 - четность
      <br>3 - всегда 1
      <br>4 - всегда 0
    </td>
  </tr>
  <tr>
    <td>
      40004
    </td>
    <td>
      Стоповые биты LSB
    </td>
    <td>
      1 - один стоповый бит
      <br>2 - 2 стоповых бита
    </td>
  </tr>
  <tr>
    <td>
      40004
    </td>
    <td>
      Стоповые биты MSB
    </td>
    <td>
      7 - 7 битов данных
      <br>8 - 8 битов данных
    </td>
  </tr>
  <tr>
    <td>
      40005
    </td>
    <td>
      Задержка ответа
    </td>
    <td>
      Время в мс
    </td>
  </tr>
  <tr>
    <td>
      40007
    </td>
    <td>
      Режим Modbus
    </td>
    <td>
      0 - RTU
      <br>1 - ASCII
    </td>
  </tr>
</table>

## Светодиодные индикаторы

![](images/MO-DIDO8-SL/leds.gif) {.img-responsive}

<table>
  <tr>
    <th>
      Указатель
    </th>
    <th>
      Описание
    </th>
  </tr>
  <tr>
    <td>
      Питание
    </td>
    <td>
      Включенный светодиод означает, что модуль правильно подключен к источнику питания.
    </td>
  </tr>
  <tr>
    <td>
      Связь
    </td>
    <td>
      Светодиод включается, когда модуль получает правильный пакет данных и высылает ответ.
    </td>
  </tr>
  <tr>
    <td>
      Состояния входов
    </td>
    <td>
      Включенный светодиод означает “1” на входе.
    </td>
  </tr>
  <tr>
    <td>
      Состояние выходов
    </td>
    <td>
      Включенный светодиод означает, что выход подсоединен.
    </td>
  </tr>
</table>

## Подключение модуля

![](images/MO-DIDO8-SL/connection.jpg) {.img-responsive}

## Настройка переключателей

![](images/MO-DIDO8-SL/switch.png) {.img-responsive}

<table>
  <tr>
    <th>
      Переключатель
    </th>
    <th>
      Функция
    </th>
    <th>
      Описание
    </th>
  </tr>
  <tr>
    <td>
      1
    </td>
    <td>
      Адрес модуля +1
    </td>
    <td rowspan="5">
      Настройка адреса модуля в диапазоне от 0 до 31
    </td>
  </tr>
  <tr>
    <td>
      2
    </td>
    <td>
      Адрес модуля +2
    </td>
  </tr>
  <tr>
    <td>
      3
    </td>
    <td>
      Адрес модуля +4
    </td>
  </tr>
  <tr>
    <td>
      4
    </td>
    <td>
      Адрес модуля +8
    </td>
  </tr>
  <tr>
    <td>
      5
    </td>
    <td>
      Адрес модуля +16
    </td>
  </tr>
  <tr>
    <td>
      6
    </td>
    <td>
      Настройка модуля по умолчанию
    </td>
    <td>
      Настройка параметров передачи по умолчанию (см. параметры по умолчанию)
    </td>
  </tr>
</table>

## Регистры модуля

### Регистровый доступ

<table>
  <thead>
    <tr>
      <th>
        Адрес
      </th>
      <th>
        Название регистра
      </th>
      <th>
        Доступ
      </th>
      <th>
        Описание
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        30001
      </td>
      <td>
        Версия/Тип
      </td>
      <td>
        Чтение
      </td>
      <td>
        Тип и версия устройства
      </td>
    </tr>
    <tr>
      <td>
        30002
      </td>
      <td>
        Переключатели
      </td>
      <td>
        Чтение
      </td>
      <td>
        Состояние переключателей
      </td>
    </tr>
    <tr>
      <td>
        40003
      </td>
      <td>
        Скорость
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Скорость передачи
      </td>
    </tr>
    <tr>
      <td>
        40004
      </td>
      <td>
        Стоповые биты
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Кол-во стоповых битов
      </td>
    </tr>
    <tr>
      <td>
        40005
      </td>
      <td>
        Четность
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Бит четности
      </td>
    </tr>
    <tr>
      <td>
        40006
      </td>
      <td>
        Опоздание
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Опоздание ответа
      </td>
    </tr>
    <tr>
      <td>
        40007
      </td>
      <td>
        Тип Modbus
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Тип протокола Modbus
      </td>
    </tr>
    <tr>
      <td>
        40009
      </td>
      <td>
        Watchdog
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Watchdog
      </td>
    </tr>
    <tr>
      <td>
        40013
      </td>
      <td>
        Состояние выходов по умолчанию
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выходов по умолчанию
      </td>
    </tr>
    <tr>
      <td>
        40033
      </td>
      <td>
        Полученные пакеты MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Кол-во полученных пакетов
      </td>
    </tr>
    <tr>
      <td>
        40034
      </td>
      <td>
        Полученные пакеты LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Кол-во полученных пакетов
      </td>
    </tr>
    <tr>
      <td>
        40035
      </td>
      <td>
        Ошибочные пакеты MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Кол-во полученных ошибочных пакетов
      </td>
    </tr>
    <tr>
      <td>
        40036
      </td>
      <td>
        Ошибочные пакеты LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Кол-во полученных ошибочных пакетов
      </td>
    </tr>
    <tr>
      <td>
        40037
      </td>
      <td>
        Высланные пакеты MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Кол-во высланных пакетов
      </td>
    </tr>
    <tr>
      <td>
        40038
      </td>
      <td>
        Высланные пакеты LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Кол-во высланных пакетов
      </td>
    </tr>
    <tr>
      <td>
        30051
      </td>
      <td>
        Входы
      </td>
      <td>
        Чтение
      </td>
      <td>
        Состояние входов
      </td>
    </tr>
    <tr>
      <td>
        40052
      </td>
      <td>
        Выходы
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выходов
      </td>
    </tr>
    <tr>
      <td>
        40053
      </td>
      <td>
        Счетчик 1 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 1
      </td>
    </tr>
    <tr>
      <td>
        40054
      </td>
      <td>
        Счетчик 1 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 1
      </td>
    </tr>
    <tr>
      <td>
        40055
      </td>
      <td>
        Счетчик 2 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 2
      </td>
    </tr>
    <tr>
      <td>
        40056
      </td>
      <td>
        Счетчик 2 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 2
      </td>
    </tr>
    <tr>
      <td>
        40057
      </td>
      <td>
        Счетчик 3 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 3
      </td>
    </tr>
    <tr>
      <td>
        40058
      </td>
      <td>
        Счетчик 3 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 3
      </td>
    </tr>
    <tr>
      <td>
        40059
      </td>
      <td>
        Счетчик 4 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 4
      </td>
    </tr>
    <tr>
      <td>
        40060
      </td>
      <td>
        Счетчик 4 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 4
      </td>
    </tr>
    <tr>
      <td>
        40061
      </td>
      <td>
        Счетчик 5 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 5
      </td>
    </tr>
    <tr>
      <td>
        40062
      </td>
      <td>
        Счетчик 5 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 5
      </td>
    </tr>
    <tr>
      <td>
        40063
      </td>
      <td>
        Счетчик 6 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 6
      </td>
    </tr>
    <tr>
      <td>
        40064
      </td>
      <td>
        Счетчик 6 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 6
      </td>
    </tr>
    <tr>
      <td>
        40065
      </td>
      <td>
        Счетчик 7 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 7
      </td>
    </tr>
    <tr>
      <td>
        40066
      </td>
      <td>
        Счетчик 7 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 7
      </td>
    </tr>
    <tr>
      <td>
        40067
      </td>
      <td>
        Счетчик 8 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 8
      </td>
    </tr>
    <tr>
      <td>
        40068
      </td>
      <td>
        Счетчик 8 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битный счетчик 8
      </td>
    </tr>
    <tr>
      <td>
        40085
      </td>
      <td>
        П-Счетчик 1 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 1
      </td>
    </tr>
    <tr>
      <td>
        40086
      </td>
      <td>
        П-Счетчик 1 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 1
      </td>
    </tr>
    <tr>
      <td>
        40087
      </td>
      <td>
        П-Счетчик 2 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 2
      </td>
    </tr>
    <tr>
      <td>
        40088
      </td>
      <td>
        П-Счетчик 2 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 2
      </td>
    </tr>
    <tr>
      <td>
        40089
      </td>
      <td>
        П-Счетчик 3 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 3
      </td>
    </tr>
    <tr>
      <td>
        40090
      </td>
      <td>
        П-Счетчик 3 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 3
      </td>
    </tr>
    <tr>
      <td>
        40091
      </td>
      <td>
        П-Счетчик 4 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 4
      </td>
    </tr>
    <tr>
      <td>
        40092
      </td>
      <td>
        П-Счетчик 4 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 4
      </td>
    </tr>
    <tr>
      <td>
        40093
      </td>
      <td>
        П-Счетчик 5 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 5
      </td>
    </tr>
    <tr>
      <td>
        40094
      </td>
      <td>
        П-Счетчик 5 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 5
      </td>
    </tr>
    <tr>
      <td>
        40095
      </td>
      <td>
        П-Счетчик 6 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 6
      </td>
    </tr>
    <tr>
      <td>
        40096
      </td>
      <td>
        П-Счетчик 6 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 6
      </td>
    </tr>
    <tr>
      <td>
        40097
      </td>
      <td>
        П-Счетчик 7 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 7
      </td>
    </tr>
    <tr>
      <td>
        40098
      </td>
      <td>
        П-Счетчик 7 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 7
      </td>
    </tr>
    <tr>
      <td>
        40099
      </td>
      <td>
        П-Счетчик 8 MSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 8
      </td>
    </tr>
    <tr>
      <td>
        40100
      </td>
      <td>
        П-Счетчик 8 LSB
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        32 битное перехвач. значение счетчика 8
      </td>
    </tr>
    <tr>
      <td>
        40117
      </td>
      <td>
        Кнф-Счетчик 1
      </td>
      <td>
        Чтение и запись
      </td>
      <td rowspan="8">
        Конфигурация счетчиков:
        <br>+1 - подсчет времени (если ноль подсчет импульсов)
        <br>+2 - перехват значения каждую 1 сек
        <br>+4 - перехват значения если неактивный вход
        <br>+8 - автоматический сброс после перехвата
        <br>+16 - сброс счетчика если неактивный вход
        <br>+32 - шифратор (encoder) (только для счетчика 1 и 3)
      </td>
    </tr>
    <tr>
      <td>
        40118
      </td>
      <td>
        Кнф-Счетчик 2
      </td>
      <td>
        Чтение и запись
      </td>
    </tr>
    <tr>
      <td>
        40119
      </td>
      <td>
        Кнф-Счетчик 3
      </td>
      <td>
        Чтение и запись
      </td>
    </tr>
    <tr>
      <td>
        40120
      </td>
      <td>
        Кнф-Счетчик 4
      </td>
      <td>
        Чтение и запись
      </td>
    </tr>
    <tr>
      <td>
        40121
      </td>
      <td>
        Кнф-Счетчик 5
      </td>
      <td>
        Чтение и запись
      </td>
    </tr>
    <tr>
      <td>
        40122
      </td>
      <td>
        Кнф-Счетчик 6
      </td>
      <td>
        Чтение и запись
      </td>
    </tr>
    <tr>
      <td>
        40123
      </td>
      <td>
        Кнф-Счетчик 7
      </td>
      <td>
        Чтение и запись
      </td>
    </tr>
    <tr>
      <td>
        40124
      </td>
      <td>
        Кнф-Счетчик 8
      </td>
      <td>
        Чтение и запись
      </td>
    </tr>
    <tr>
      <td>
        40133
      </td>
      <td>
        Перехват
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Перехват значений счетчиков
      </td>
    </tr>
    <tr>
      <td>
        40134
      </td>
      <td>
        Статус
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Счетчик перехвачен
      </td>
    </tr>
  </tbody>
</table>

### Побитовый доступ

<table>
  <thead>
    <tr>
      <th>
        Адрес
      </th>
      <th>
        Название регистра
      </th>
      <th>
        Доступ
      </th>
      <th>
        Описание
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        193
      </td>
      <td>
        Состояние выхода по умолчанию 1
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода по умолчанию 1
      </td>
    </tr>
    <tr>
      <td>
        194
      </td>
      <td>
        Состояние выхода по умолчанию 2
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода по умолчанию 2
      </td>
    </tr>
    <tr>
      <td>
        195
      </td>
      <td>
        Состояние выхода по умолчанию 3
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода по умолчанию 3
      </td>
    </tr>
    <tr>
      <td>
        196
      </td>
      <td>
        Состояние выхода по умолчанию 4
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода по умолчанию 4
      </td>
    </tr>
    <tr>
      <td>
        197
      </td>
      <td>
        Состояние выхода по умолчанию 5
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода по умолчанию 5
      </td>
    </tr>
    <tr>
      <td>
        198
      </td>
      <td>
        Состояние выхода по умолчанию 6
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода по умолчанию 6
      </td>
    </tr>
    <tr>
      <td>
        199
      </td>
      <td>
        Состояние выхода по умолчанию 7
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода по умолчанию 7
      </td>
    </tr>
    <tr>
      <td>
        200
      </td>
      <td>
        Состояние выхода по умолчанию 8
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода по умолчанию 8
      </td>
    </tr>
    <tr>
      <td>
        10801
      </td>
      <td>
        Вход 1
      </td>
      <td>
        Чтение
      </td>
      <td>
        Состояние входа 1
      </td>
    </tr>
    <tr>
      <td>
        10802
      </td>
      <td>
        Вход 2
      </td>
      <td>
        Чтение
      </td>
      <td>
        Состояние входа 2
      </td>
    </tr>
    <tr>
      <td>
        10803
      </td>
      <td>
        Вход 3
      </td>
      <td>
        Чтение
      </td>
      <td>
        Состояние входа 3
      </td>
    </tr>
    <tr>
      <td>
        10804
      </td>
      <td>
        Вход 4
      </td>
      <td>
        Чтение
      </td>
      <td>
        Состояние входа 4
      </td>
    </tr>
    <tr>
      <td>
        10805
      </td>
      <td>
        Вход 5
      </td>
      <td>
        Чтение
      </td>
      <td>
        Состояние входа 5
      </td>
    </tr>
    <tr>
      <td>
        10806
      </td>
      <td>
        Вход 6
      </td>
      <td>
        Чтение
      </td>
      <td>
        Состояние входа 6
      </td>
    </tr>
    <tr>
      <td>
        10807
      </td>
      <td>
        Вход 7
      </td>
      <td>
        Чтение
      </td>
      <td>
        Состояние входа 7
      </td>
    </tr>
    <tr>
      <td>
        10808
      </td>
      <td>
        Вход 8
      </td>
      <td>
        Чтение
      </td>
      <td>
        Состояние входа 8
      </td>
    </tr>
    <tr>
      <td>
        817
      </td>
      <td>
        Выход 1
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода 1
      </td>
    </tr>
    <tr>
      <td>
        818
      </td>
      <td>
        Выход 2
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода 2
      </td>
    </tr>
    <tr>
      <td>
        819
      </td>
      <td>
        Выход 3
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода 3
      </td>
    </tr>
    <tr>
      <td>
        820
      </td>
      <td>
        Выход 4
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода 4
      </td>
    </tr>
    <tr>
      <td>
        821
      </td>
      <td>
        Выход 5
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода 5
      </td>
    </tr>
    <tr>
      <td>
        822
      </td>
      <td>
        Выход 6
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода 6
      </td>
    </tr>
    <tr>
      <td>
        823
      </td>
      <td>
        Выход 7
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода 7
      </td>
    </tr>
    <tr>
      <td>
        824
      </td>
      <td>
        Выход 8
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Состояние выхода 8
      </td>
    </tr>
    <tr>
      <td>
        2113
      </td>
      <td>
        Перехват 1
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Перехват счетчика 1
      </td>
    </tr>
    <tr>
      <td>
        2114
      </td>
      <td>
        Перехват 2
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Перехват счетчика 2
      </td>
    </tr>
    <tr>
      <td>
        2115
      </td>
      <td>
        Перехват 3
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Перехват счетчика 3
      </td>
    </tr>
    <tr>
      <td>
        2116
      </td>
      <td>
        Перехват 4
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Перехват счетчика 4
      </td>
    </tr>
    <tr>
      <td>
        2117
      </td>
      <td>
        Перехват 5
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Перехват счетчика 5
      </td>
    </tr>
    <tr>
      <td>
        2118
      </td>
      <td>
        Перехват 6
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Перехват счетчика 6
      </td>
    </tr>
    <tr>
      <td>
        2119
      </td>
      <td>
        Перехват 7
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Перехват счетчика 7
      </td>
    </tr>
    <tr>
      <td>
        2120
      </td>
      <td>
        Перехват 8
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Перехват счетчика 8
      </td>
    </tr>
    <tr>
      <td>
        2129
      </td>
      <td>
        Перехвачен 1
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Счетчик перехвачен 1
      </td>
    </tr>
    <tr>
      <td>
        2130
      </td>
      <td>
        Перехвачен 2
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Счетчик перехвачен 2
      </td>
    </tr>
    <tr>
      <td>
        2131
      </td>
      <td>
        Перехвачен 3
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Счетчик перехвачен 3
      </td>
    </tr>
    <tr>
      <td>
        2132
      </td>
      <td>
        Перехвачен 4
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Счетчик перехвачен 4
      </td>
    </tr>
    <tr>
      <td>
        2133
      </td>
      <td>
        Перехвачен 5
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Счетчик перехвачен 5
      </td>
    </tr>
    <tr>
      <td>
        2134
      </td>
      <td>
        Перехвачен 6
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Счетчик перехвачен 6
      </td>
    </tr>
    <tr>
      <td>
        2135
      </td>
      <td>
        Перехвачен 7
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Счетчик перехвачен 7
      </td>
    </tr>
    <tr>
      <td>
        2136
      </td>
      <td>
        Перехвачен 8
      </td>
      <td>
        Чтение и запись
      </td>
      <td>
        Счетчик перехвачен 8
      </td>
    </tr>
  </tbody>
</table>

## Конфигурационная программа

Программное обеспечение - “Конфигуратор” - служит для настройки регистров ответственных за коммуникацию модуля с шиной Modbus, а также для чтения и записи текущих значений остальных регистров модуля. Благодоря программе также можно удобным способом тестировать систему и следить за изменениями в регистрах в режиме реального времени. Коммуникация с модулем производится посредством кабеля USB. Для взаимодействия программного обеспечения с модулем нет необходимости устанавливать какие-либо драйверы.

![](images/MO-DIDO8-SL/configurator.png) {.img-responsive}

