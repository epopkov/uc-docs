**Конструктор драйверов** - позволяет создавать собственные протоколы для работы с устройствами на последовательных портах, а так-же драйвера для работы с другими устройствами или сервисами по протоколам tcp/udp и http.

### Схема настроек ###

При создании устройства с созданным драйвером, часто необходимо назначить дополнительные настройки. Констуртор драйверов предлагает интерфейс для создания схемы настроек (см. общее описание драйвера).

### Внутрение компоненты и API###
*Будут дополняться*

* Менеджер последовательных портов
* CRC модуль
* Модуль сообщений
* [Нативные модули nodejs](http://nodejs.org/api/index.html)
  * DNS
  * Events
  * HTTP - только клиент
  * Net - только клиент
  * Punycode
  * Query Strings
  * Stream
  * String Decoder
  * Timers
  * UDP/Datagram
  * URL

#### Менеджер последовательных портов ####
*Serial port manager*

* Дает доступ к физичеким последовательным интерфейсам RS232, RS485-1, RS485-2
* Устанавливает настройки для интерфейса (скорость, битность, управление потоком и т.д.)
* Создает очередь из сообщейний и колбэков

![](images/spm.png) {.img-responsive}

#### CRC модуль ####

Используется для подсчета CRC в различных протоколах. [Nodejs module](https://www.npmjs.com/package/crc)

#### Модуль сообщений ####

По сути внутрення имплементация console (log,error), только с выводом в шину событий. Используется для дебага драйверов.


#### Примеры реализации драйвера на JS ####

* [IRidium](http://wiki2.iridiummobile.ru/IRidium_DDK)
* [Pronto](https://drive.google.com/file/d/0B1Tw2Xpu0QvQamlmdGZ3cFlKSkk/view?usp=sharing) см. стр. 41

#### Пример реализации очереди ####

``` javascript
var SerialStream = require('serialport-stream'),
    Serials = {};

var init = function(port, baud) {
    if (!Serials[port]) Serials[port] = new Serial(port, baud);
    return Serials[port];
}

var Serial = function(port, baud) {
    var self = this;
    self.port = port;
    self.stream = new SerialStream(port, baud);
    self.queue = [];
    self.processing = false;
    setInterval(function(serial) {
        if (!self.processing) self.doJob();
    }, 10, self);
}

Serial.prototype.doJob = function() {
    var self = this;
    var job = this.queue.shift();
    if (job) {
        self.processing = true;
        var timeout = setTimeout(function() {
            self.processing = false;
            self.stream.removeAllListeners('data');
            if (job.callback) job.callback(null, true);
        }, job.timeout);
        self.stream.once('data', function(data) {
            self.processing = false;
            clearTimeout(timeout);
            if (job.callback) job.callback(data);
        })
        self.stream.write(job.data);
    }
}

Serial.prototype.push = function(data, callback, timeout) {
    var self = this;
    self.queue.push({
        data: data,
        callback: callback,
        timeout: timeout || 500
    });
};

Serial.prototype.unshift = function(data, callback, timeout) {
    var self = this;
    self.queue.unshift({
        data: data,
        callback: callback,
        timeout: timeout || 500
    });
};

module.exports = init;
```
