Драйвер для устройств на шине 1-wire. [Список поддерживаемых устройств](http://owfs.org/index.php?page=standard-devices)

## Основные свойства и компоненты

### Поиск устройств ###

Предоставляет интерфейс для поиска устройств в шине 1-wire и определения их типа и вывода всего дерева внутренних перменных. Пример возвращаемого объекта с датчиком температуры и другим устройством:
``` Javascript
[{
    id:"10.2341564740000",
    data:[{
        name:"temperature",
        cast:"number"
    },{
        name:"fasttemp",
        cast:"number"
    }]
},{
    id:"22.2341564740000",
    data:[{
        name:"PIO.byte",
        cast:"bit array"
    },{
        name:"address",
        cast:"string"
    }] // Список всех папок из owfs, по умолчанию cast - string.
}]
```

#### Список автоматически определяемых полей ####

``` Javascript
{
    temperature:{
        cast:"number"
    },
    fasttemp:{
        cast:"number"
    },
    "PIO.byte":{
        cast:"bit array"
    }
}
```

### Схема настроек ###

``` Javascript
{
  "settings":{
    "id":{
      "title":"1-wire device id",
      "type": "string",
      "require":"true"
    }
  },
  "variables":[{
    "title":"Data channel"
    "name": {
        "type": "string",
        "title": "Variable name",
        "unique":true,
        "required":true
    },
    "data": {
        "type":"number",
        "title": "Data channel name",
        "required":true
    },
    "cast": {
        "type": "string",
        "enum":["boolean","number","bit array","string"],
        "title": "1-wire value type",
        "required":true,
        "default":"number"
    }
  }]
}
```