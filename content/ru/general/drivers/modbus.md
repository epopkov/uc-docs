**Modbus** - драйвер для работы в режиме мастера по протоколам Modbus RTU/ASCI и TCP/UDP.


##Адресация устройств##

ID - число от 0 до 255. 0 - широковещательный адресс, используется для массовой рассылки команд.

##Адресация данных##

Логический адрес данных представляет собой шестизначное жесятичное число, где старшая цифра обозначает тип данных. 

![](../images/modbus.png) {.img-responsive}

Для формирования физического адреса нужно:
1. Отбросить старшую цифру логического адреса.
2. Вычесть 1.

##Типы данных##

* Bit - 1 coil/input
* Bit array - 1 holding/input register.
* Integer unsigned (int16) (по умолчанию) - 1 holding/input register
* Integer signed (uint16) - 1 holding/input register
* Long unsigned (int32) - 2 holding/input registers
* Long signed (uint32) - 2 holding/input registers
* Float - 2 holding/input registers

##Примеры устройств##

[MO-TR8-SL](http://ru.unitecontrol.com/docs/devices/MO-TR8-SL.html)
[MO-DI4-MI](http://ru.unitecontrol.com/docs/devices/MO-DI4-MI.html)