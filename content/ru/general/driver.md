**Драйвер** - интерфейс настройки и взаимодействия системы с устройством или сервисом. С помощью драйвера создаются новые устройства в системе.

Существует два типа драйверов:
1. Встроенные - быстрые и проверенные драйвера написанные с использованием нативных решений. На данный момент:
  * ModBus RTU/ASCI
  * ModBus TCP (slave/master)
  * Mbus
  * 1-wire
2. Созданные пользователями - созданные с помощью конструктора драйверов, написанные на JS.

## Основные свойства и компоненты ##

### Интерфейс настройки ###

Состоит из двух JSON схем представления необходимых переменных для системы и настройки устройства на основе драйвера.
Пример схемы элемента для modbus драйвера:
``` Javascript
{
  "settings":{
    "port":{
      "title":"Serial port",
      "type": "string",
      "enum": ["RS485-1","RS485-2","RS232"],
      "require":"true"
    },
    "boudrate":{
      "title":"Serial port boudrate",
      "type": "number",
      "enum": [1200,2400,4800,9600,19200,38400,57600,115200],
      "require":"true",
      "default":115200
    },
    "parity":{
      "title":"Serial port parity",
      "type": "string",
      "enum": ["none","odd","even"],
      "require":"true",
      "default":"none"
    },
    "flow":{
      "title":"Serial port flow control",
      "type": "string",
      "enum": ["software","hardware"],
      "require":"true",
      "default":"none"
    }
    "address":{
      "title":"ModBus device address",
      "type": "string",
      "require":"true"
    },
    "protocol":{
      "title":"ModBus serial protocol",
      "type": "string",
      "enum":["RTU","ASCI"],
      "require":"true",
      "default":"RTU"
    },
  },
  "variables":[{
    "title":"One data address"
    "name": {
        "type": "string",
        "title": "Variable name",
        "unique":true,
        "required":true
    },
    "data": {
        "type":"number",
        "title": "Modbus data address",
        "required":true
    },
    "fun": {
        "type":"number",
        "enum":["coil","input","holding register","input register"],
        "title": "Modbus data address type"
    },
    "cast": {
        "type": "string",
        "enum":["boolean","integer","double","string","bit array"],
        "title": "ModBus value type",
        "required":true
    }
  },{
    "title":"Data address range"
    "name": {
        "type": "string",
        "title": "Variable name",
        "unique":true,
        "required":true
    },
    "data": {
        "type":"range",
        "title": "Modbus data address range",
        "required":true
    },
    "fun": {
        "type":"number",
        "enum":["coil","input","holding register","input register"],
        "title": "Modbus data address type"
    },
    "cast": {
        "type": "string",
        "enum":["boolean","integer","double","string","bit array"],
        "title": "ModBus value type",
        "required":true
    }
  }]
}
```
