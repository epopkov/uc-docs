# UC MO-DO16-SL {.header}

[UC MO-DO16-SL Moduł rozszerzający](http://pl.unitecontrol.com/MO-DO16-SL.html) – 16 wyjść cyfrowych

![](images/MO-DO16-SL/device.png) {.img-responsive}

## Zasady bezpieczeństwa

* Przed pierwszym uruchomieniem urządzenia należy zapoznać się z niniejszą instrukcją obsługi;
* Przed pierwszym uruchomieniem urządzenia należy upewnić się, że wszystkie przewody zostały podłączone prawidłowo;
* Należy zapewnić właściwe warunki pracy, zgodne ze specyfikacją urządzenia (np.: napięcie zasilania, temperatura, maksymalny pobór prądu);
* Przed dokonaniem jakichkolwiek modyfikacji przyłączeń przewodów, należy wyłączyć napięcie zasilania.

## Charakterystyka modułu

### Przeznaczenie i opis modułu

* Moduł 16O jest innowacyjnym urządzeniem zapewniającym proste i niedrogie rozszerzenie ilości linii wyjściowych w popularnych sterownikach PLC.
* Moduł posiada 16 wyjść cyfrowych. Wszystkie z wyjścia są izolowane od logiki za pomocą transoptorów.
* Moduł produkowany jest w dwóch wersjach z wyjściami typu PNP i NPN:
<table>
    <tr>
        <td bgcolor="#e6e6ff">Oznaczenie</td>
        <td bgcolor="#e6e6ff">Typ wyjść</td>
    </tr>
    <tr>
        <td>16O-PNP</td>
        <td>PNP</td>
    </tr>
    <tr>
        <td>16O-NPN</td>
        <td>NPN</td>
    </tr>
</table>
* Moduł ten podłączany jest do magistrali RS485 za pomocą dwu przewodowej skrętki. Komunikacja odbywa się z wykorzystaniem protokołu MODBUS RTU lub MODBUS ASCII. Zastosowanie 32‑bitowego procesora z rdzeniem ARM zapewnia szybkie przetwarzanie danych i szybką komunikację. Prędkość transmisji jest konfigurowalna od 2400 do 115200.
* Moduł przeznaczony jest do montażu na szynie DIN zgodnie z normą DIN EN 5002.
* Moduł został wyposażony z zestaw diod LED (kontrolek), używanych do wskazywania stanu wyjść przydatnych w celach diagnostycznych i pomagających w znalezieniu błędów.
* Konfiguracja modułu odbywa się przez USB za pomocą dedykowanego programu komputerowego. Możliwa jest również zmiana parametrów za pomocą protokołu MODBUS.

### Specyfikacja techniczna



<table>
  <tr>
    <th rowspan="2" bgcolor="#e6e6ff"><b>Zasilanie</b>
    </th>
    <th>Napięcie</th>
    <th>12-24 V DC ± 20%</th>
  </tr>
  <tr>
    <th>Prąd maksymalny</th>
    <th>180 mA @ 12V / 100 mA @ 24V</th>
  </tr>
  <tr>
    <td rowspan="4" bgcolor="#e6e6ff"><b>Wyjścia cyfrowe</b>
    </td>
    <td>Liczba wyjść</td>
    <td>16</td>
  </tr>
  <tr>
    <td>Typ wyjścia</td>
    <td>PNP lub NPN (w zależności od wersji)</td>
  </tr>
  <tr>
    <td>Maksymalne napięcie</td>
    <td>30V</td>
  </tr>
  <tr>
    <td>Maksymalny prąd jednego wyjścia</td>
    <td>0.5A </td>
  </tr>
  <tr>
    <td rowspan="2" bgcolor="#e6e6ff"><b>Temperatura</b>
    </td>
    <td>Pracy</td>
    <td>-10 °C - +50°C</td>
  </tr>
  <tr>
    <td>Przechowywania</td>
    <td>-40 °C - +85°C</td>
  </tr>
  <tr>
    <td rowspan="4" bgcolor="#e6e6ff"><b>Złącza</b>
    </td>
    <td>Zasilające</td>
    <td>2 pinowe</td>
  </tr>
  <tr>
    <td>Komunikacyjne</td>
    <td>3 pinowe</td>
  </tr>
  <tr>
    <td>Wyjścia</td>
    <td>10 pinowe</td>
  </tr>
  <tr>
    <td>Konfiguracyjne</td>
    <td>Mini USB</td>
  </tr>
  <tr>
    <td rowspan="3" bgcolor="#e6e6ff"><b>Wymiary</b>
    </td>
    <td>Wysokość</td>
    <td>120 mm</td>
  </tr>
  <tr>
    <td>Głębokość</td>
    <td>101 mm</td>
  </tr>
  <tr>
    <td>Szerokość</td>
    <td>22,5 mm</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Interfejs</b>
    </td>
    <td>RS485</td>
    <td>Do 128 urządzeń</td>
  </tr>
</table>

### Wymiary modułu

![](images/MO-DO16-SL/overview.png) {.img-responsive}

Wygląd i wymiary modułu znajdują się na rysunku poniżej. Moduł mocowany jest bezpośrednio do szyny w przemysłowym standardzie DIN. Złącza zasilające, komunikacyjne oraz wejść znajdują się od dołu i góry modułu. Złącze konfiguracyjne USB oraz wskaźniki znajdują się z przodu modułu.

## Konfiguracja komunikacji

### Uziemienie i ekranowanie

Moduł może być zainstalowany wraz z innymi urządzeniami, które generują promieniowanie elektromagnetyczne. Przykładami takich urządzeń są przekaźniki i styczniki, transformatory, sterowniki silników itp. To promieniowanie elektromagnetyczne może powodować zakłócenia elektryczne zasilania i przewodów sygnałowych, a także promieniując bezpośrednio do modułu, powodując negatywne skutki dla systemu. Odpowiednie uziemienie, osłony oraz inne działania ochronne należy podjąć na etapie instalacji, aby zapobiec tym efektom. Te działania ochronne obejmują m.in. uziemienie szafy sterowniczej, uziemienie modułu, uziemienie ekranowania przewodów, zabezpieczenie urządzeń przełączających, prawidłowego okablowania, jak również uwzględnienie typów kabli i ich przekrojów. 

### Terminator

Efekty linii transmisyjnej często powodują problemy w sieciach teleinformatycznych. Problemy te dotyczą najczęściej tłumienia sygnału i odbić w sieci.

Aby wyeliminować obecność odbić od końców kabla, należy na obu jego końcach zastosować rezystor o impedancji równej impedancji charakterystycznej linii. W przypadku skrętki RS485 typową wartością jest 120 Ω.

### Ustalanie adresu modułu w sieci

Poniższa tabela przedstawia sposób ustawienia przełączników w celu ustalenia adresu modułu. Za pomocą przełączników możliwe jest ustawienie adresu od 0 do 31. Adresy od 32 do 255 możliwe są do ustawienia za pomocą magistrali RS485 lub przez złącze USB.

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff">Adr</td>
      <td bgcolor="#e6e6ff">SW5</td>
      <td bgcolor="#e6e6ff">SW4</td>
      <td bgcolor="#e6e6ff">SW3</td>
      <td bgcolor="#e6e6ff">SW2</td>
      <td bgcolor="#e6e6ff">SW1</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>1</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>2</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>3</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>4</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>5</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>6</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>7</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>8</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>9</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>10</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>11</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>12</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>13</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>14</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>15</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>16</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>17</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>18</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>19</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>20</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>21</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>22</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>23</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>24</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>25</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>26</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>27</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>28</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>29</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>30</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>31</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
  </tbody>
</table>

### Typy rejestrów Modbus

Są 4 typy zmiennych dostępnych w module. 

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Typ</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres początkowy</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Zmienna</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Dostęp</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Rozkaz Modbus</b>
    </td>
  </tr>
  <tr>
    <td>1</td>
    <td>00001</td>
    <td>Wyjścia cyfrowe</td>
    <td>Bitowy
      <br>Odczyt i zapis</td>
    <td>1, 5, 15</td>
  </tr>
  <tr>
    <td>2</td>
    <td>10001</td>
    <td>Wejścia cyfrowe</td>
    <td>Bitowy
      <br>Odczyt</td>
    <td>2</td>
  </tr>
  <tr>
    <td>3</td>
    <td>30001</td>
    <td>Rejestry wejściowe</td>
    <td>Rejestrowy
      <br>Odczyt</td>
    <td>3</td>
  </tr>
  <tr>
    <td>4</td>
    <td>40001</td>
    <td>Rejestry wyjściowe</td>
    <td>Rejestrowy
      <br>Odczyt i zapis</td>
    <td>4, 6, 16</td>
  </tr>
</table>

### Ustawienia komunikacji

Dane w modułach przechowywane są w 16 bitowych rejestrach. Dostęp do rejestrów odbywa się za pomocą protokołu MODBUS RTU lub MODBUS ASCII.

#### Domyślne parametry

Domyślną konfigurację można przywrócić za pomocą przełącznika SW6 (szczegóły w - Przywracanie konfiguracji domyślnej)

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Prędkość transmisji</b>
    </td>
    <td>19200</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Parzystość</b>
    </td>
    <td>Nie</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Ilość bitów stopu</b>
    </td>
    <td>1</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Opóźnienie odpowiedzi [ms]</b>
    </td>
    <td>0</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Tryb Modbus</b>
    </td>
    <td>RTU</td>
  </tr>
</table>

#### Przywracanie konfiguracji domyślnej

W celu przywrócenia konfiguracji domyślnej należy przy wyłączonym zasilaniu modułu załączyć przełącznik SW6, a następnie włączyć zasilanie. Moduł zacznie migać na zmianę diodami wskazującymi zasilanie i komunikację. Jeżeli w tym stanie zostanie wyłączony przełącznik SW6 ustawienia zostaną nadpisane. 

**Uwaga!**Podczas przywracania konfiguracji domyślnej wykasowane zostaną również wszystkie inne wartości zapisane w rejestrach modułu!

#### Rejestry konfiguracyjne

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Adres</b>
      <br><b>Modbus</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres</b>
      <br><b>Dec</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres</b>
      <br><b>Hex</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Nazwa</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Wartości</b>
    </td>
  </tr>
  <tr>
    <td>40003</td>
    <td>2</td>
    <td>0x02</td>
    <td>Prędkość transmisji</td>
    <td>0 – 2400
      <br>1 – 4800
      <br>2 – 9600
      <br>3 – 19200
      <br>4 – 38400
      <br>5 – 57600
      <br>6 – 115200
      <br>inna wartość – wartość * 10</td>
  </tr>
  <tr>
    <td>40005</td>
    <td>4</td>
    <td>0x04</td>
    <td>Parzystość</td>
    <td>0 – brak
      <br>1 – nieparzystość
      <br>2 – parzystość
      <br>3 – zawsze 1
      <br>4 – zawsze 0</td>
  </tr>
  <tr>
    <td>40004</td>
    <td>3</td>
    <td>0x03</td>
    <td>Bity Stopu LSB</td>
    <td>1 – jeden bit stopu
      <br>2 – dwa bity stopu</td>
  </tr>
  <tr>
    <td>40004</td>
    <td>3</td>
    <td>0x03</td>
    <td>Bity Stopu MSB</td>
    <td>7 – 7 bitów danych
      <br>8 – 8 bitów danych</td>
  </tr>
  <tr>
    <td>40006</td>
    <td>5</td>
    <td>0x05</td>
    <td>Opóźnienie odpowiedzi</td>
    <td>Czas w ms</td>
  </tr>
  <tr>
    <td>40007</td>
    <td>6</td>
    <td>0x06</td>
    <td>Tryb Modbus</td>
    <td>0 – RTU 1 – ASCII</td>
  </tr>
</table>

## Wskaźniki diodowe

![](images/MO-DO16-SL/leds.png) {.img-responsive}

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Wskaźnik</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Opis</b>
    </td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Zasilanie</td>
    <td>Zapalona dioda oznacza, że moduł jest poprawnie zasilany.</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Komunikacja</td>
    <td>Dioda zapala się, gdy moduł odebrał prawidłowy pakiet i wysyła odpowiedź.</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Stany wyjść</td>
    <td>Zapalona dioda informuje, że wyjście jest załączone.</td>
  </tr>
</table>

## Podłączenie modułu – wyjścia PNP

![](images/MO-DO16-SL/connection.jpg) {.img-responsive}

## Podłączenie modułu – wyjścia NPN

![](images/MO-DO16-SL/connection.jpg) {.img-responsive}

## Ustawienia przełączników

![](images/MO-DO16-SL/switch.png) {.img-responsive}

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Przełącznik</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Funkcja</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Opis</b>
    </td>
  </tr>
  <tr>
    <td>1</td>
    <td>Adres modułu +1</td>
    <td rowspan="5">Ustawienie adresu modułu w zakresie od 0 do 31</td>
  </tr>
  <tr>
    <td>2</td>
    <td>Adres modułu +2</td>
  </tr>
  <tr>
    <td>3</td>
    <td>Adres modułu +4</td>
  </tr>
  <tr>
    <td>4</td>
    <td>Adres modułu +8</td>
  </tr>
  <tr>
    <td>5</td>
    <td>Adres modułu +16</td>
  </tr>
  <tr>
    <td>6</td>
    <td>Ustawienia domyślne modułu</td>
    <td>Ustawienie domyślnych parametrów transmisji
      <br>(patrz - Domyślne parametry i - Przywracanie konfiguracji domyślnej).</td>
  </tr>
</table>

## Rejestry modułu

### Dostęp rejestrowy

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Modbus</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Dec</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Hex</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Nazwa rejestru</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Dostęp</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Opis</b>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>30001</td>
      <td>0</td>
      <td>0x00</td>
      <td>Wersja/Typ</td>
      <td>Odczyt</td>
      <td>Typ i wersja urządzenia</td>
    </tr>
    <tr>
      <td>30002</td>
      <td>1</td>
      <td>0x01</td>
      <td>Przełączniki</td>
      <td>Odczyt</td>
      <td>Stan przełączników</td>
    </tr>
    <tr>
      <td>40003</td>
      <td>2</td>
      <td>0x02</td>
      <td>Prędkość</td>
      <td>Odczyt i zapis</td>
      <td>Prędkość transmisji</td>
    </tr>
    <tr>
      <td>40004</td>
      <td>3</td>
      <td>0x03</td>
      <td>Bity stopu</td>
      <td>Odczyt i zapis</td>
      <td>Ilość bitów stopu</td>
    </tr>
    <tr>
      <td>40005</td>
      <td>4</td>
      <td>0x04</td>
      <td>Parzystość</td>
      <td>Odczyt i zapis</td>
      <td>Bit parzystości</td>
    </tr>
    <tr>
      <td>40006</td>
      <td>5</td>
      <td>0x05</td>
      <td>Opóźnienie</td>
      <td>Odczyt i zapis</td>
      <td>Opóźnienie odpowiedzi</td>
    </tr>
    <tr>
      <td>40007</td>
      <td>6</td>
      <td>0x06</td>
      <td>Typ Modbus</td>
      <td>Odczyt i zapis</td>
      <td>Typ protokołu Modbus </td>
    </tr>
    <tr>
      <td>40009</td>
      <td>8</td>
      <td>0x08</td>
      <td>Watchdog</td>
      <td>Odczyt i zapis</td>
      <td>Watchdog</td>
    </tr>
    <tr>
      <td>40013</td>
      <td>12</td>
      <td>0x0C</td>
      <td>Domyślny stan wyjść</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjść</td>
    </tr>
    <tr>
      <td>40033</td>
      <td>32</td>
      <td>0x20</td>
      <td>Odebrane ramki MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość odebranych ramek</td>
    </tr>
    <tr>
      <td>40034</td>
      <td>33</td>
      <td>0x21</td>
      <td>Odebrane ramki LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40035</td>
      <td>34</td>
      <td>0x22</td>
      <td>Błędne ramki MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość odebranych błędnych ramek</td>
    </tr>
    <tr>
      <td>40036</td>
      <td>35</td>
      <td>0x23</td>
      <td>Błędne ramki LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40037</td>
      <td>36</td>
      <td>0x24</td>
      <td>Wysłane ramki MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość wysłanych ramek</td>
    </tr>
    <tr>
      <td>40038</td>
      <td>37</td>
      <td>0x25</td>
      <td>Wysłane ramki LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40052</td>
      <td>51</td>
      <td>0x33</td>
      <td>Wyjścia</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjść</td>
    </tr>
  </tbody>
</table>

### Dostęp bitowy

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Modbus</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Dec</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Hex</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Nazwa rejestru</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Dostęp</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Opis</b>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>193</td>
      <td>192</td>
      <td>0x0C0</td>
      <td>Domyślny stan wyjścia 1</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 1</td>
    </tr>
    <tr>
      <td>194</td>
      <td>193</td>
      <td>0x0C1</td>
      <td>Domyślny stan wyjścia 2</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 2</td>
    </tr>
    <tr>
      <td>195</td>
      <td>194</td>
      <td>0x0C2</td>
      <td>Domyślny stan wyjścia 3</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 3</td>
    </tr>
    <tr>
      <td>196</td>
      <td>195</td>
      <td>0x0C3</td>
      <td>Domyślny stan wyjścia 4</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 4</td>
    </tr>
    <tr>
      <td>197</td>
      <td>196</td>
      <td>0x0C4</td>
      <td>Domyślny stan wyjścia 5</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 5</td>
    </tr>
    <tr>
      <td>198</td>
      <td>197</td>
      <td>0x0C5</td>
      <td>Domyślny stan wyjścia 6</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 6</td>
    </tr>
    <tr>
      <td>199</td>
      <td>198</td>
      <td>0x0C6</td>
      <td>Domyślny stan wyjścia 7</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 7</td>
    </tr>
    <tr>
      <td>200</td>
      <td>199</td>
      <td>0x0C7</td>
      <td>Domyślny stan wyjścia 8</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 8</td>
    </tr>
    <tr>
      <td>201</td>
      <td>200</td>
      <td>0x0C8</td>
      <td>Domyślny stan wyjścia 9</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 9</td>
    </tr>
    <tr>
      <td>202</td>
      <td>201</td>
      <td>0x0C9</td>
      <td>Domyślny stan wyjścia 10</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 10</td>
    </tr>
    <tr>
      <td>203</td>
      <td>202</td>
      <td>0x0CA</td>
      <td>Domyślny stan wyjścia 11</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 11</td>
    </tr>
    <tr>
      <td>204</td>
      <td>203</td>
      <td>0x0CB</td>
      <td>Domyślny stan wyjścia 12</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 12</td>
    </tr>
    <tr>
      <td>205</td>
      <td>204</td>
      <td>0x0CC</td>
      <td>Domyślny stan wyjścia 13</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 13</td>
    </tr>
    <tr>
      <td>206</td>
      <td>205</td>
      <td>0x0CD</td>
      <td>Domyślny stan wyjścia 14</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 14</td>
    </tr>
    <tr>
      <td>207</td>
      <td>206</td>
      <td>0x0CE</td>
      <td>Domyślny stan wyjścia 15</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 15</td>
    </tr>
    <tr>
      <td>208</td>
      <td>207</td>
      <td>0x0CF</td>
      <td>Domyślny stan wyjścia 16</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 16</td>
    </tr>
    <tr>
      <td>817</td>
      <td>816</td>
      <td>0x330</td>
      <td>Wyjście 1</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 1</td>
    </tr>
    <tr>
      <td>818</td>
      <td>817</td>
      <td>0x331</td>
      <td>Wyjście 2</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 2</td>
    </tr>
    <tr>
      <td>819</td>
      <td>818</td>
      <td>0x332</td>
      <td>Wyjście 3</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 3</td>
    </tr>
    <tr>
      <td>820</td>
      <td>819</td>
      <td>0x333</td>
      <td>Wyjście 4</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 4</td>
    </tr>
    <tr>
      <td>821</td>
      <td>820</td>
      <td>0x334</td>
      <td>Wyjście 5</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 5</td>
    </tr>
    <tr>
      <td>822</td>
      <td>821</td>
      <td>0x335</td>
      <td>Wyjście 6</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 6</td>
    </tr>
    <tr>
      <td>823</td>
      <td>822</td>
      <td>0x336</td>
      <td>Wyjście 7</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 7</td>
    </tr>
    <tr>
      <td>824</td>
      <td>823</td>
      <td>0x337</td>
      <td>Wyjście 8</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 8</td>
    </tr>
    <tr>
      <td>825</td>
      <td>824</td>
      <td>0x338</td>
      <td>Wyjście 9</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 9</td>
    </tr>
    <tr>
      <td>826</td>
      <td>825</td>
      <td>0x339</td>
      <td>Wyjście 10</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 10</td>
    </tr>
    <tr>
      <td>827</td>
      <td>826</td>
      <td>0x33A</td>
      <td>Wyjście 11</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 11</td>
    </tr>
    <tr>
      <td>828</td>
      <td>827</td>
      <td>0x33B</td>
      <td>Wyjście 12</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 12</td>
    </tr>
    <tr>
      <td>829</td>
      <td>828</td>
      <td>0x33C</td>
      <td>Wyjście 13</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 13</td>
    </tr>
    <tr>
      <td>830</td>
      <td>829</td>
      <td>0x33D</td>
      <td>Wyjście 14</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 14</td>
    </tr>
    <tr>
      <td>831</td>
      <td>830</td>
      <td>0x33E</td>
      <td>Wyjście 15</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 15</td>
    </tr>
    <tr>
      <td>832</td>
      <td>831</td>
      <td>0x33F</td>
      <td>Wyjście 16</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 16</td>
    </tr>
  </tbody>
</table>

## Program konfiguracyjny

![](images/MO-DO16-SL/configurator.png) {.img-responsive}

Konfigurator jest oprogramowaniem służącym do ustawienia rejestrów odpowiedzialnych za komunikację modułu w magistrali Modbus jak również do odczytu i zapisu aktualnych wartości pozostałych rejestrów modułu. Dzięki temu programowi można w wygodny sposób przetestować układ jak również w czasie rzeczywistym obserwować zmiany w rejestrach.

Komunikacja z modułem odbywa się poprzez kabel USB. Do współdziałania programu z modułem nie jest wymagana instalacja żadnych sterowników.

Konfigurator jest uniwersalnym programem, za pomocą którego możliwa jest konfiguracja wszystkich dostępnych modułów.

