# UC MO-RO6-ST {.header}

[UC MO-RO6-ST Moduł rozszerzający](http://pl.unitecontrol.com/MO-RO6-ST.html) – 6 wyjść przekaźnikowych

![](images/MO-RO6-ST/device.png) {.img-responsive}

## Zasady bezpieczeństwa

* Przed pierwszym uruchomieniem urządzenia należy zapoznać się z niniejszą instrukcją obsługi;
* Przed pierwszym uruchomieniem urządzenia należy upewnić się, że wszystkie przewody zostały podłączone prawidłowo;
* Należy zapewnić właściwe warunki pracy, zgodne ze specyfikacją urządzenia (np.: napięcie zasilania, temperatura, maksymalny pobór prądu);
* Przed dokonaniem jakichkolwiek modyfikacji przyłączeń przewodów, należy wyłączyć napięcie zasilania.

## Charakterystyka modułu

### Przeznaczenie i opis modułu

Moduł SDM-6RO jest innowacyjnym urządzeniem zapewniającym proste i niedrogie rozszerzenie ilości linii wyjściowych o dużej obciążalności prądowej.

Moduł posiada 6 wyjść przekaźnikowych.Każdy przekaźnik posiada 3 wyprowadzenia: wspólny (COM), normalnie otwarty (NO) i normalnie zamknięty (NC), dzięki czemu urządzenie jest bardzo elastyczne.

Moduł ten podłączany jest do magistrali RS485 za pomocą dwu przewodowej skrętki. Komunikacja odbywa się z wykorzystaniem protokołu MODBUS RTU lub MODBUS ASCII. Zastosowanie 32-bitowego procesora z rdzeniem ARM zapewnia szybkie przetwarzanie danych i szybką komunikację. Prędkość transmisji jest konfigurowalna od 2400 do 115200.

Moduł przeznaczony jest do montażu na szynie DIN zgodnie z normą DIN EN 5002.

Moduł został wyposażony z zestaw diod LED (kontrolek), używanych do wskazywania stanu wyjść przydatnych w celach diagnostycznych i pomagających w znalezieniu błędów.

Konfiguracja modułu odbywa się przez USB za pomocą dedykowanego programu komputerowego. Możliwa jest również zmiana parametrów za pomocą protokołu MODBUS.

### Specyfikacja techniczna

<table>
  <tr>
    <th rowspan="3" bgcolor="#e6e6ff"><b>Zasilanie</b>
    </th>
    <th>Napięcie</th>
    <th>10-30 VDC; 10-28VAC</th>
  </tr>
  <tr>
    <th>Prąd maksymalny<sup>*</sup>
    </th>
    <th>DC: 200 mA @ 24VDC
      <br>AC: 250 mA @ 24VAC</th>
  </tr>
  <tr>
    <th>Maksymalna pobierana moc</th>
    <th>DC: 4.8W; AC: 6VA</th>
  </tr>
  <tr>
    <td rowspan="3" bgcolor="#e6e6ff"><b>Wyjścia przekaźnikowe</b>
    </td>
    <td>Liczba wyjść</td>
    <td>6</td>
  </tr>
  <tr>
    <td rowspan="2">Maksymalny prąd i napięcie
      <br>(obciążenie rezystancyjne)</td>
    <td>5A 250VAC</td>
  </tr>
  <tr>
    <td>10A 30VDC</td>
  </tr>
  <tr>
    <td rowspan="2" bgcolor="#e6e6ff"><b>Temperatura</b>
    </td>
    <td>Pracy</td>
    <td>-10 °C - +50°C</td>
  </tr>
  <tr>
    <td>Przechowywania</td>
    <td>-40 °C - +85°C</td>
  </tr>
  <tr>
    <td rowspan="4" bgcolor="#e6e6ff"><b>Złącza</b>
    </td>
    <td>Zasilające</td>
    <td>2 pinowe</td>
  </tr>
  <tr>
    <td>Komunikacyjne</td>
    <td>3 pinowe</td>
  </tr>
  <tr>
    <td>Wyjścia</td>
    <td>2x 10 pinowe</td>
  </tr>
  <tr>
    <td>Konfiguracyjne</td>
    <td>Mini USB</td>
  </tr>
  <tr>
    <td rowspan="3" bgcolor="#e6e6ff"><b>Wymiary</b>
    </td>
    <td>Wysokość</td>
    <td>110 mm</td>
  </tr>
  <tr>
    <td>Głębokość</td>
    <td>62 mm</td>
  </tr>
  <tr>
    <td>Szerokość</td>
    <td>88 mm</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Interfejs</b>
    </td>
    <td>RS485</td>
    <td>Do 128 urządzeń</td>
  </tr>
</table>

### Wymiary modułu

![](images/MO-RO6-ST/overview.png) {.img-responsive}

Wygląd i wymiary modułu znajdują się na rysunku poniżej. Moduł mocowany jest bezpośrednio do szyny w przemysłowym standardzie DIN. Złącza zasilające, komunikacyjne oraz wejść znajdują się od dołu i góry modułu. Złącze konfiguracyjne USB oraz wskaźniki znajdują się z przodu modułu.

## Konfiguracja komunikacji

### Uziemienie i ekranowanie

Moduł może być zainstalowany wraz z innymi urządzeniami, które generują promieniowanie elektromagnetyczne. Przykładami takich urządzeń są przekaźniki i styczniki, transformatory, sterowniki silników itp. To promieniowanie elektromagnetyczne może powodować zakłócenia elektryczne zasilania i przewodów sygnałowych, a także promieniując bezpośrednio do modułu, powodując negatywne skutki dla systemu. Odpowiednie uziemienie, osłony oraz inne działania ochronne należy podjąć na etapie instalacji, aby zapobiec tym efektom. Te działania ochronne obejmują m.in. uziemienie szafy sterowniczej, uziemienie modułu, uziemienie ekranowania przewodów, zabezpieczenie urządzeń przełączających, prawidłowego okablowania, jak również uwzględnienie typów kabli i ich przekrojów. 

### Terminator

Efekty linii transmisyjnej często powodują problemy w sieciach teleinformatycznych. Problemy te dotyczą najczęściej tłumienia sygnału i odbić w sieci.

Aby wyeliminować obecność odbić od końców kabla, należy na obu jego końcach zastosować rezystor o impedancji równej impedancji charakterystycznej linii. W przypadku skrętki RS485 typową wartością jest 120 Ω.

### Ustalanie adresu modułu w sieci

Poniższa tabela przedstawia sposób ustawienia przełączników w celu ustalenia adresu modułu. Za pomocą przełączników możliwe jest ustawienie adresu od 0 do 127. Adresy od 128 do 255 możliwe są do ustawienia za pomocą magistrali RS485 lub przez złącze USB.

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Przełącznik</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres</b>
    </td>
  </tr>
  <tr>
    <td>SW1</td>
    <td>+1</td>
  </tr>
  <tr>
    <td>SW2</td>
    <td>+2</td>
  </tr>
  <tr>
    <td>SW3</td>
    <td>+4</td>
  </tr>
  <tr>
    <td>SW4</td>
    <td>+8</td>
  </tr>
  <tr>
    <td>SW5</td>
    <td>+16</td>
  </tr>
  <tr>
    <td>SW6</td>
    <td>+32</td>
  </tr>
  <tr>
    <td>SW7</td>
    <td>+64</td>
  </tr>
</table>

Np. włączenie przełączników 1, 3 i 5 spowoduje ustawienie adresu:

Adres = 1 + 4 + 16 = 21

### Typy rejestrów Modbus

Są 4 typy zmiennych dostępnych w module. 

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Typ</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres początkowy</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Zmienna</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Dostęp</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Rozkaz Modbus</b>
    </td>
  </tr>
  <tr>
    <td>1</td>
    <td>00001</td>
    <td>Wyjścia cyfrowe</td>
    <td>Bitowy
      <br>Odczyt i zapis</td>
    <td>1, 5, 15</td>
  </tr>
  <tr>
    <td>2</td>
    <td>10001</td>
    <td>Wejścia cyfrowe</td>
    <td>Bitowy
      <br>Odczyt</td>
    <td>2</td>
  </tr>
  <tr>
    <td>3</td>
    <td>30001</td>
    <td>Rejestry wejściowe</td>
    <td>Rejestrowy
      <br>Odczyt</td>
    <td>3</td>
  </tr>
  <tr>
    <td>4</td>
    <td>40001</td>
    <td>Rejestry wyjściowe</td>
    <td>Rejestrowy
      <br>Odczyt i zapis</td>
    <td>4, 6, 16</td>
  </tr>
</table>

### Ustawienia komunikacji

Dane w modułach przechowywane są w 16 bitowych rejestrach. Dostęp do rejestrów odbywa się za pomocą protokołu MODBUS RTU lub MODBUS ASCII.

#### Domyślne parametry

Domyślną konfigurację można przywrócić za pomocą przełącznika SW7 (szczegóły w - Przywracanie konfiguracji domyślnej)

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Prędkość transmisji</b>
    </td>
    <td>19200</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Parzystość</b>
    </td>
    <td>Nie</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Ilość bitów danych</b>
    </td>
    <td>8</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Ilość bitów stopu</b>
    </td>
    <td>1</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Opóźnienie odpowiedzi [ms]</b>
    </td>
    <td>0</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Tryb Modbus</b>
    </td>
    <td>RTU</td>
  </tr>
</table>

#### Przywracanie konfiguracji domyślnej

W celu przywrócenia konfiguracji domyślnej należy przy wyłączonym zasilaniu modułu załączyć przełącznik SW8 a następnie włączyć zasilanie. Moduł zacznie migać na zmianę diodami wskazującymi zasilanie i komunikację. Jeżeli w tym stanie zostanie wyłączony przełącznik SW8 ustawienia zostaną nadpisane. 

**Uwaga!**Podczas przywracania konfiguracji domyślnej wykasowane zostaną również wszystkie inne wartości zapisane w rejestrach modułu!

#### Rejestry konfiguracyjne

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Adres</b>
      <br><b>Modbus</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres</b>
      <br><b>Dec</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres</b>
      <br><b>Hex</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Nazwa</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Wartości</b>
    </td>
  </tr>
  <tr>
    <td>40003</td>
    <td>2</td>
    <td>0x02</td>
    <td>Prędkość transmisji</td>
    <td>0 – 2400
      <br>1 – 4800
      <br>2 – 9600
      <br>3 – 19200
      <br>4 – 38400
      <br>5 – 57600
      <br>6 – 115200
      <br>inna wartość – wartość * 10</td>
  </tr>
  <tr>
    <td>40005</td>
    <td>4</td>
    <td>0x04</td>
    <td>Parzystość</td>
    <td>0 – brak
      <br>1 – nieparzystość
      <br>2 – parzystość
      <br>3 – zawsze 1
      <br>4 – zawsze 0</td>
  </tr>
  <tr>
    <td>40004</td>
    <td>3</td>
    <td>0x03</td>
    <td>Bity Stopu LSB</td>
    <td>1 – jeden bit stopu
      <br>2 – dwa bity stopu</td>
  </tr>
  <tr>
    <td>40004</td>
    <td>3</td>
    <td>0x03</td>
    <td>Bity Stopu MSB</td>
    <td>7 – 7 bitów danych
      <br>8 – 8 bitów danych</td>
  </tr>
  <tr>
    <td>40006</td>
    <td>5</td>
    <td>0x05</td>
    <td>Opóźnienie odpowiedzi</td>
    <td>Czas w ms</td>
  </tr>
  <tr>
    <td>40007</td>
    <td>6</td>
    <td>0x06</td>
    <td>Tryb Modbus</td>
    <td>0 – RTU
      <br>1 – ASCII</td>
  </tr>
</table>

## Ustawienia przełączników

![](images/MO-RO6-ST/switch.png) {.img-responsive}

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Przełącznik</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Funkcja</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Opis</b>
    </td>
  </tr>
  <tr>
    <td>1</td>
    <td>Adres modułu +1</td>
    <td rowspan="7">Ustawienie adresu modułu
      <br>w zakresie od 0 do 127</td>
  </tr>
  <tr>
    <td>2</td>
    <td>Adres modułu +2</td>
  </tr>
  <tr>
    <td>3</td>
    <td>Adres modułu +4</td>
  </tr>
  <tr>
    <td>4</td>
    <td>Adres modułu +8</td>
  </tr>
  <tr>
    <td>5</td>
    <td>Adres modułu +16</td>
  </tr>
  <tr>
    <td>6</td>
    <td>Adres modułu +32</td>
  </tr>
  <tr>
    <td>7</td>
    <td>Adres modułu +64</td>
  </tr>
  <tr>
    <td>8</td>
    <td>Ustawienia domyślne modułu</td>
    <td>Ustawienie domyślnych parametrów transmisji
      <br>(patrz - Domyślne parametry i - Przywracanie konfiguracji domyślnej).</td>
  </tr>
</table>

## Zdejmowanie panelu  

W celu zdjęcia panelu i uzyskania dostępu do przełączników należy podważyć go z boku za pomocą cienkiego narzędzia (np. mały wkrętak) jak na poniższym zdjęciu.

## Wskaźniki diodowe

![](images/MO-RO6-ST/leds.png) {.img-responsive}

 <spacer type="block"> </spacer>

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Wskaźnik</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Opis</b>
    </td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Zasilanie</td>
    <td>Zapalona dioda oznacza, że moduł jest poprawnie zasilany.</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Komunikacja</td>
    <td>Dioda zapala się, gdy moduł odebrał prawidłowy pakiet i wysyła odpowiedź.</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Stany wyjść</td>
    <td>Zapalona dioda informuje, że wyjście jest załączone.</td>
  </tr>
</table>

## Podłączenie modułu

![](images/MO-RO6-ST/connection.jpg) {.img-responsive}

## Rejestry modułu

### Dostęp rejestrowy

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Modbus</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Dec</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Hex</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Nazwa rejestru</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Dostęp</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Opis</b>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>30001</td>
      <td>0</td>
      <td>0x00</td>
      <td>Wersja/Typ</td>
      <td>Odczyt</td>
      <td>Typ i wersja urządzenia</td>
    </tr>
    <tr>
      <td>30002</td>
      <td>1</td>
      <td>0x01</td>
      <td>Przełączniki</td>
      <td>Odczyt</td>
      <td>Stan przełączników</td>
    </tr>
    <tr>
      <td>40003</td>
      <td>2</td>
      <td>0x02</td>
      <td>Prędkość</td>
      <td>Odczyt i zapis</td>
      <td>Prędkość transmisji</td>
    </tr>
    <tr>
      <td>40004</td>
      <td>3</td>
      <td>0x03</td>
      <td>Bity stopu i danych</td>
      <td>Odczyt i zapis</td>
      <td>Ilość bitów stopu</td>
    </tr>
    <tr>
      <td>40005</td>
      <td>4</td>
      <td>0x04</td>
      <td>Parzystość</td>
      <td>Odczyt i zapis</td>
      <td>Bit parzystości</td>
    </tr>
    <tr>
      <td>40006</td>
      <td>5</td>
      <td>0x05</td>
      <td>Opóźnienie</td>
      <td>Odczyt i zapis</td>
      <td>Opóźnienie odpowiedzi</td>
    </tr>
    <tr>
      <td>40007</td>
      <td>6</td>
      <td>0x06</td>
      <td>Typ Modbus</td>
      <td>Odczyt i zapis</td>
      <td>Typ protokołu Modbus </td>
    </tr>
    <tr>
      <td>40009</td>
      <td>8</td>
      <td>0x08</td>
      <td>Watchdog</td>
      <td>Odczyt i zapis</td>
      <td>Watchdog</td>
    </tr>
    <tr>
      <td>40013</td>
      <td>12</td>
      <td>0x0C</td>
      <td>Domyślny stan wyjść</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjść</td>
    </tr>
    <tr>
      <td>40033</td>
      <td>32</td>
      <td>0x20</td>
      <td>Odebrane ramki LSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość odebranych ramek</td>
    </tr>
    <tr>
      <td>40034</td>
      <td>33</td>
      <td>0x21</td>
      <td>Odebrane ramki MSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40035</td>
      <td>34</td>
      <td>0x22</td>
      <td>Błędne ramki LSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość odebranych błędnych ramek</td>
    </tr>
    <tr>
      <td>40036</td>
      <td>35</td>
      <td>0x23</td>
      <td>Błędne ramki MSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40037</td>
      <td>36</td>
      <td>0x24</td>
      <td>Wysłane ramki LSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość wysłanych ramek</td>
    </tr>
    <tr>
      <td>40038</td>
      <td>37</td>
      <td>0x25</td>
      <td>Wysłane ramki MSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40052</td>
      <td>51</td>
      <td>0x33</td>
      <td>Wyjścia</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjść</td>
    </tr>
  </tbody>
</table>

### Dostęp bitowy

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Modbus</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Dec</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Hex</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Nazwa rejestru</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Dostęp</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Opis</b>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>193</td>
      <td>192</td>
      <td>0x0C0</td>
      <td>Domyślny stan wyjścia 1</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 1</td>
    </tr>
    <tr>
      <td>194</td>
      <td>193</td>
      <td>0x0C1</td>
      <td>Domyślny stan wyjścia 2</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 2</td>
    </tr>
    <tr>
      <td>195</td>
      <td>194</td>
      <td>0x0C2</td>
      <td>Domyślny stan wyjścia 3</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 3</td>
    </tr>
    <tr>
      <td>196</td>
      <td>195</td>
      <td>0x0C3</td>
      <td>Domyślny stan wyjścia 4</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 4</td>
    </tr>
    <tr>
      <td>197</td>
      <td>196</td>
      <td>0x0C4</td>
      <td>Domyślny stan wyjścia 5</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 5</td>
    </tr>
    <tr>
      <td>198</td>
      <td>197</td>
      <td>0x0C5</td>
      <td>Domyślny stan wyjścia 6</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 6</td>
    </tr>
    <tr>
      <td>817</td>
      <td>816</td>
      <td>0x330</td>
      <td>Wyjście 1</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 1</td>
    </tr>
    <tr>
      <td>818</td>
      <td>817</td>
      <td>0x331</td>
      <td>Wyjście 2</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 2</td>
    </tr>
    <tr>
      <td>819</td>
      <td>818</td>
      <td>0x332</td>
      <td>Wyjście 3</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 3</td>
    </tr>
    <tr>
      <td>820</td>
      <td>819</td>
      <td>0x333</td>
      <td>Wyjście 4</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 4</td>
    </tr>
    <tr>
      <td>821</td>
      <td>820</td>
      <td>0x334</td>
      <td>Wyjście 5</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 5</td>
    </tr>
    <tr>
      <td>822</td>
      <td>821</td>
      <td>0x335</td>
      <td>Wyjście 6</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia 6</td>
    </tr>
  </tbody>
</table>

## Program konfiguracyjny

![](images/MO-RO6-ST/configurator.png) {.img-responsive}

Konfigurator jest oprogramowaniem służącym do ustawienia rejestrów odpowiedzialnych za komunikację modułu w magistrali Modbus jak również do odczytu i zapisu aktualnych wartości pozostałych rejestrów modułu. Dzięki temu programowi można w wygodny sposób przetestować układ jak również w czasie rzeczywistym obserwować zmiany w rejestrach.

Komunikacja z modułem odbywa się poprzez kabel USB. Do współdziałania programu z modułem nie jest wymagana instalacja żadnych sterowników.

Konfigurator jest uniwersalnym programem, za pomocą którego możliwa jest konfiguracja wszystkich dostępnych modułów.

*Maksymalny prąd przy aktywnej transmisji Modbus i załączonych wszystkich wyjściach.

