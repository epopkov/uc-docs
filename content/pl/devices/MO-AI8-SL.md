# UC MO-AI8-SL {.header}

[UC MO-AI8-SL Moduł rozszerzający](http://pl.unitecontrol.com/MO-AI8-SL.html) – 8 wejście analogowe

![](images/MO-AI8-SL/device.png) {.img-responsive}

## Zasady bezpieczeństwa

* Przed pierwszym uruchomieniem urządzenia należy zapoznać się z niniejszą instrukcją obsługi;
* Przed pierwszym uruchomieniem urządzenia należy upewnić się, że wszystkie przewody zostały podłączone prawidłowo;
* Należy zapewnić właściwe warunki pracy, zgodne ze specyfikacją urządzenia (np.: napięcie zasilania, temperatura, maksymalny pobór prądu);
* Przed dokonaniem jakichkolwiek modyfikacji przyłączeń przewodów, należy wyłączyć napięcie zasilania.

## Charakterystyka modułu

### Przeznaczenie i opis modułu

* Moduł 8AI umożliwia pomiar wartości napięć oraz prądów. Odczyt wartości następuje za pomocą magistrali RS485 (protokół Modbus), dzięki czemu w prosty sposób można zintegrować moduł z popularnymi sterownikami PLC, HMI lub komputerami PC wyposażonymi w odpowiednie przejściówki.
* Urządzenie posiada zestaw 8 wejść do pomiarów analogowych. Dodatkowo moduł wyposażony jest w 2 konfigurowalne wyjścia cyfrowe.
* Moduł ten podłączany jest do magistrali RS485 za pomocą dwu przewodowej skrętki. Komunikacja odbywa się z wykorzystaniem protokołu MODBUS RTU lub MODBUS ASCII. Zastosowanie 32-bitowego procesora z rdzeniem ARM zapewnia szybkie przetwarzanie danych i szybką komunikację. Prędkość transmisji jest konfigurowalna od 2400 do 115200.
* Moduł przeznaczony jest do montażu na szynie DIN zgodnie z normą DIN EN 5002.
* Moduł został wyposażony z zestaw diod LED (kontrolek), używanych do wskazywania stanu wyjść przydatnych w celach diagnostycznych i pomagających w znalezieniu błędów.
* Konfiguracja modułu odbywa się przez USB za pomocą dedykowanego programu komputerowego. Możliwa jest również zmiana parametrów za pomocą protokołu MODBUS.

### Specyfikacja techniczna

<table>
  <tr>
    <th rowspan="2" bgcolor="#e6e6ff"><b>Zasilanie</b>
    </th>
    <th>Napięcie</th>
    <th>12-24 V DC ± 20%</th>
  </tr>
  <tr>
    <th>Prąd maksymalny</th>
    <th>120 mA @ 12V / 100 mA @ 24V</th>
  </tr>
  <tr>
    <td rowspan="12" bgcolor="#e6e6ff"><b>Wejścia </b>
    </td>
    <td>Liczba wejść</td>
    <td>8</td>
  </tr>
  <tr>
    <td>Wejście napięciowe</td>
    <td>0V do 10V (rozdzielczość 1.5mV)</td>
  </tr>
  <tr>
    <td>Wejście napięciowe</td>
    <td>-10V do 10V (rozdzielczość 1.5mV)</td>
  </tr>
  <tr>
    <td>Wejście napięciowe</td>
    <td>0V do 1V(rozdzielczość 0.1875mV)</td>
  </tr>
  <tr>
    <td>Wejście napięciowe</td>
    <td>-1V do 1V(rozdzielczość 0.1875V)</td>
  </tr>
  <tr>
    <td>Wejście prądowe</td>
    <td>4mA do 20mA (rozdzielczość 3.75μA)</td>
  </tr>
  <tr>
    <td>Wejście prądowe</td>
    <td>0mA do 20mA (rozdzielczość 3.75μA)</td>
  </tr>
  <tr>
    <td>Wejście prądowe</td>
    <td>-20mA do 20mA (rozdzielczość 3.75μA)</td>
  </tr>
  <tr>
    <td>Rozdzielczość przetwornika</td>
    <td>14 bitów</td>
  </tr>
  <tr>
    <td>Czas przetwarzania ADC</td>
    <td>16ms / kanał</td>
  </tr>
  <tr>
    <td>Dokładność pomiaru napięcia</td>
    <td>maksymalnie ±0,1% w całym przedziale</td>
  </tr>
  <tr>
    <td>Dokładność pomiaru prądu</td>
    <td>maksymalnie ±0,1% w całym przedziale</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Wyjścia cyfrowe</b>
    </td>
    <td>Maksymalny prąd i napięcie</td>
    <td>500mA / 55V</td>
  </tr>
  <tr>
    <td rowspan="2" bgcolor="#e6e6ff"><b>Temperatura</b>
    </td>
    <td>Pracy</td>
    <td>-20 °C - +65°C</td>
  </tr>
  <tr>
    <td>Przechowywania</td>
    <td>-40 °C - +85°C</td>
  </tr>
  <tr>
    <td rowspan="5" bgcolor="#e6e6ff"><b>Złącza</b>
    </td>
    <td>Zasilające</td>
    <td>2 pinowe</td>
  </tr>
  <tr>
    <td>Komunikacyjne</td>
    <td>3 pinowe</td>
  </tr>
  <tr>
    <td>Wejścia i wyjścia</td>
    <td>2 x 10 pinowe</td>
  </tr>
  <tr>
    <td>Szybkozłączka</td>
    <td>IDC10 </td>
  </tr>
  <tr>
    <td>Konfiguracyjne</td>
    <td>Mini USB</td>
  </tr>
  <tr>
    <td rowspan="3" bgcolor="#e6e6ff"><b>Wymiary</b>
    </td>
    <td>Wysokość</td>
    <td>120 mm</td>
  </tr>
  <tr>
    <td>Głębokość</td>
    <td>101 mm</td>
  </tr>
  <tr>
    <td>Szerokość</td>
    <td>22,5 mm</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Interfejs</b>
    </td>
    <td>RS485</td>
    <td>Do 128 urządzeń</td>
  </tr>
</table>

### Wymiary modułu

![](images/MO-AI8-SL/overview.png) {.img-responsive}

Wygląd i wymiary modułu znajdują się na rysunku poniżej. Moduł mocowany jest bezpośrednio do szyny w przemysłowym standardzie DIN. Złącza zasilające, komunikacyjne oraz wejść znajdują się od dołu i góry modułu. Złącze konfiguracyjne USB oraz wskaźniki znajdują się z przodu modułu.

## Konfiguracja komunikacji

### Uziemienie i ekranowanie

W większości przypadków, moduł będzie zainstalowany w obudowie wraz z innymi urządzeniami, które generują promieniowanie elektromagnetyczne. Przykładami takich urządzeń są przekaźniki i styczniki, transformatory, sterowniki silników itp. To promieniowanie elektromagnetyczne może powodować zakłócenia elektryczne zasilania i przewodów sygnałowych, a także promieniując bezpośrednio do modułu, powodując negatywne skutki dla systemu. Odpowiednie uziemienie, osłony oraz inne działania ochronne należy podjąć na etapie instalacji, aby zapobiec tym efektom. Te działania ochronne obejmują m.in. uziemienie szafy sterowniczej, uziemienie modułu, uziemienie ekranowania przewodów, zabezpieczenie urządzeń przełączających, prawidłowego okablowania, jak również uwzględnienie typów kabli i ich przekrojów. 

### Terminator

Efekty linii transmisyjnej często powodują problemy w sieciach teleinformatycznych. Problemy te dotyczą najczęściej tłumienia sygnału i odbić w sieci.

Aby wyeliminować obecność odbić od końców kabla, należy na obu jego końcach zastosować rezystor o impedancji równej impedancji charakterystycznej linii. W przypadku skrętki RS485 typową wartością jest 120 Ω.

### Ustalanie adresu modułu w sieci

Poniższa tabela przedstawia sposób ustawienia przełączników w celu ustalenia adresu modułu. Za pomocą przełączników możliwe jest ustawienie adresu od 0 do 31. Adresy od 32 do 255 możliwe są do ustawienia za pomocą magistrali RS485 lub przez złącze USB.

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff">Adr</td>
      <td bgcolor="#e6e6ff">SW5</td>
      <td bgcolor="#e6e6ff">SW4</td>
      <td bgcolor="#e6e6ff">SW3</td>
      <td bgcolor="#e6e6ff">SW2</td>
      <td bgcolor="#e6e6ff">SW1</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>1</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>2</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>3</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>4</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>5</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>6</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>7</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>8</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>9</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>10</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>11</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>12</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>13</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>14</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>15</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>16</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>17</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>18</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>19</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>20</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>21</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>22</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>23</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>24</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>25</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>26</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>27</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>28</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>29</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>30</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>31</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
  </tbody>
</table>

### Typy rejestrów Modbus

Są 4 typy zmiennych dostępnych w module. 

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Typ</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres początkowy</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Zmienna</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Dostęp</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Rozkaz Modbus</b>
    </td>
  </tr>
  <tr>
    <td>1</td>
    <td>00001</td>
    <td>Wyjścia cyfrowe</td>
    <td>Bitowy
      <br>Odczyt i zapis</td>
    <td>1, 5, 15</td>
  </tr>
  <tr>
    <td>2</td>
    <td>10001</td>
    <td>Wejścia cyfrowe</td>
    <td>Bitowy
      <br>Odczyt</td>
    <td>2</td>
  </tr>
  <tr>
    <td>3</td>
    <td>30001</td>
    <td>Rejestry wejściowe</td>
    <td>Rejestrowy
      <br>Odczyt</td>
    <td>3</td>
  </tr>
  <tr>
    <td>4</td>
    <td>40001</td>
    <td>Rejestry wyjściowe</td>
    <td>Rejestrowy
      <br>Odczyt i zapis</td>
    <td>4, 6, 16</td>
  </tr>
</table>

### Ustawienia komunikacji

Dane w modułach przechowywane są w 16 bitowych rejestrach. Dostęp do rejestrów odbywa się za pomocą protokołu MODBUS RTU lub MODBUS ASCII.

#### Domyślne parametry

Domyślną konfigurację można przywrócić za pomocą przełącznika SW6 (szczegóły w - Przywracanie konfiguracji domyślnej)

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Prędkość transmisji</b>
    </td>
    <td>19200</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Parzystość</b>
    </td>
    <td>Nie</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Ilość bitów danych</b>
    </td>
    <td>8</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Ilość bitów stopu</b>
    </td>
    <td>1</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Opóźnienie odpowiedzi [ms]</b>
    </td>
    <td>0</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Tryb Modbus</b>
    </td>
    <td>RTU</td>
  </tr>
</table>

#### Przywracanie konfiguracji domyślnej

W celu przywrócenia konfiguracji domyślnej należy przy wyłączonym zasilaniu modułu załączyć przełącznik SW6, a następnie włączyć zasilanie. Moduł zacznie migać na zmianę diodami wskazującymi zasilanie i komunikację. Jeżeli w tym stanie zostanie wyłączony przełącznik SW6 ustawienia zostaną nadpisane. 

**Uwaga!**Podczas przywracania konfiguracji domyślnej wykasowane zostaną również wszystkie inne wartości zapisane w rejestrach modułu!

#### Rejestry konfiguracyjne

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Adres Modbus</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres Dec</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres Hex</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Nazwa</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Wartości</b>
    </td>
  </tr>
  <tr>
    <td>40003</td>
    <td>2</td>
    <td>0x02</td>
    <td>Prędkość transmisji</td>
    <td>0 – 2400
      <br>1 – 4800
      <br>2 – 9600
      <br>3 – 19200
      <br>4 – 38400
      <br>5 – 57600
      <br>6 – 115200
      <br>inna wartość – wartość * 10</td>
  </tr>
  <tr>
    <td>40005</td>
    <td>4</td>
    <td>0x04</td>
    <td>Parzystość</td>
    <td>0 – brak
      <br>1 – nieparzystość
      <br>2 – parzystość
      <br>3 – zawsze 1
      <br>4 – zawsze 0</td>
  </tr>
  <tr>
    <td>40004</td>
    <td>3</td>
    <td>0x03</td>
    <td>Bity Stopu LSB</td>
    <td>1 – jeden bit stopu
      <br>2 – dwa bity stopu</td>
  </tr>
  <tr>
    <td>40004</td>
    <td>3</td>
    <td>0x03</td>
    <td>Bity Stopu MSB</td>
    <td>7 – 7 bitów danych
      <br>8 – 8 bitów danych</td>
  </tr>
  <tr>
    <td>40006</td>
    <td>5</td>
    <td>0x05</td>
    <td>Opóźnienie odpowiedzi</td>
    <td>Czas w ms</td>
  </tr>
  <tr>
    <td>40007</td>
    <td>6</td>
    <td>0x06</td>
    <td>Tryb Modbus</td>
    <td>0 – RTU
      <br>1 – ASCII</td>
  </tr>
</table>

## Wskaźniki diodowe

![](images/MO-AI8-SL/leds.jpg) {.img-responsive}

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Wskaźnik</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Opis</b>
    </td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Zasilanie</td>
    <td>Zapalona dioda oznacza, że moduł jest poprawnie zasilany.</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Komunikacja</td>
    <td>Dioda zapala się, gdy moduł odebrał prawidłowy pakiet i wysyła odpowiedź.</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Stany wejść</td>
    <td>Zapalona dioda informuje, że wejście jest podłączone.</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Stany wyjść</td>
    <td>Zapalona dioda informuje, że wyjście jest załączone.</td>
  </tr>
</table>

## Podłączenie modułu

![](images/MO-AI8-SL/connection.jpg) {.img-responsive}

## Wybór trybu pracy wejścia

Każde wejście może służyć do pomiaru napięcia (domyślnie) lub prądu. Aby zmienić tryb pracy oprócz zmiany konfiguracji za pomocą programu należy również odpowiednio ustawić zworki wewnątrz modułu zgodnie z poniższą tabelą

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Zworka</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Opis</b>
    </td>
  </tr>
  <tr>
    <td>zdjęta ![](images/MO-AI8-SL/jump0.png) {.img-responsive}</td>
    <td>Pomiar napięcia</td>
  </tr>
  <tr>
    <td>założona</td>
    <td>Pomiar prądu</td>
  </tr>
</table>

## Otwieranie obudowy

![](images/MO-AI8-SL/disassemble.jpg) {.img-responsive}

1. Zdjąć zaczep poprzez jego naciśnięcie i przesunięcie w kierunku środka obudowy. Uwaga na znajdującą się pod zaczepem sprężynę. 2. Rozdzielić obudowę delikatnie odchylając za pomocą cienkiego narzędzia zaczepy znajdujące się w punktach pokazanych na rysunku.

## Ustawienia przełączników

![](images/MO-AI8-SL/switch.png) {.img-responsive}

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Przełącznik</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Funkcja</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Opis</b>
    </td>
  </tr>
  <tr>
    <td>1</td>
    <td>Adres modułu +1</td>
    <td rowspan="5">Ustawienie adresu modułu w zakresie od 0 do 31</td>
  </tr>
  <tr>
    <td>2</td>
    <td>Adres modułu +2</td>
  </tr>
  <tr>
    <td>3</td>
    <td>Adres modułu +4</td>
  </tr>
  <tr>
    <td>4</td>
    <td>Adres modułu +8</td>
  </tr>
  <tr>
    <td>5</td>
    <td>Adres modułu +16</td>
  </tr>
  <tr>
    <td>6</td>
    <td>Ustawienia domyślne modułu</td>
    <td>Ustawienie domyślnych parametrów transmisji
      <br>(patrz - Domyślne parametry i - Przywracanie konfiguracji domyślnej).</td>
  </tr>
</table>

## Rejestry modułu

### Dostęp rejestrowy

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Modbus</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Dec</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Hex</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Nazwa rejestru</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Dostęp</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Opis</b>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>30001</td>
      <td>0</td>
      <td>0x00</td>
      <td>Wersja/Typ</td>
      <td>Odczyt</td>
      <td>Typ i wersja urządzenia</td>
    </tr>
    <tr>
      <td>30002</td>
      <td>1</td>
      <td>0x01</td>
      <td>Przełączniki</td>
      <td>Odczyt</td>
      <td>Stan przełączników</td>
    </tr>
    <tr>
      <td>40003</td>
      <td>2</td>
      <td>0x02</td>
      <td>Prędkość</td>
      <td>Odczyt i zapis</td>
      <td>Prędkość transmisji</td>
    </tr>
    <tr>
      <td>40004</td>
      <td>3</td>
      <td>0x03</td>
      <td>Bity stopu</td>
      <td>Odczyt i zapis</td>
      <td>Ilość bitów stopu</td>
    </tr>
    <tr>
      <td>40005</td>
      <td>4</td>
      <td>0x04</td>
      <td>Parzystość</td>
      <td>Odczyt i zapis</td>
      <td>Bit parzystości</td>
    </tr>
    <tr>
      <td>40006</td>
      <td>5</td>
      <td>0x05</td>
      <td>Opóźnienie</td>
      <td>Odczyt i zapis</td>
      <td>Opóźnienie odpowiedzi</td>
    </tr>
    <tr>
      <td>40007</td>
      <td>6</td>
      <td>0x06</td>
      <td>Typ Modbus</td>
      <td>Odczyt i zapis</td>
      <td>Typ protokołu Modbus </td>
    </tr>
    <tr>
      <td>40033</td>
      <td>32</td>
      <td>0x20</td>
      <td>Odebrane ramki MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość odebranych ramek</td>
    </tr>
    <tr>
      <td>40034</td>
      <td>33</td>
      <td>0x21</td>
      <td>Odebrane ramki LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40035</td>
      <td>34</td>
      <td>0x22</td>
      <td>Błędne ramki MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość odebranych błędnych ramek</td>
    </tr>
    <tr>
      <td>40036</td>
      <td>35</td>
      <td>0x23</td>
      <td>Błędne ramki LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40037</td>
      <td>36</td>
      <td>0x24</td>
      <td>Wysłane ramki MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość wysłanych ramek</td>
    </tr>
    <tr>
      <td>40038</td>
      <td>37</td>
      <td>0x25</td>
      <td>Wysłane ramki LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>30051</td>
      <td>50</td>
      <td>0x32</td>
      <td>Wejścia</td>
      <td>Odczyt </td>
      <td>Podłączone wejścia
        <br>zapalony bit → wejście podłączone</td>
    </tr>
    <tr>
      <td>40052</td>
      <td>51</td>
      <td>0x33</td>
      <td>Wyjścia</td>
      <td>Odczyt i zapis</td>
      <td>Wyjścia alarmowe
        <br>bit 8 i 9 wyjścia cyfrowe</td>
    </tr>
    <tr>
      <td>30053</td>
      <td>52</td>
      <td>0x34</td>
      <td>Analog 1</td>
      <td>Odczyt</td>
      <td rowspan="8">Wartość wejścia analogowego:
        <br>
        <br>w mV dla wejść napięciowych
        <br>w μA dla wejść prądowych</td>
    </tr>
    <tr>
      <td>30054</td>
      <td>53</td>
      <td>0x35</td>
      <td>Analog 2</td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>30055</td>
      <td>54</td>
      <td>0x36</td>
      <td>Analog 3</td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>30056</td>
      <td>55</td>
      <td>0x37</td>
      <td>Analog 4</td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>30057</td>
      <td>56</td>
      <td>0x38</td>
      <td>Analog 5</td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>30058</td>
      <td>57</td>
      <td>0x39</td>
      <td>Analog 6</td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>30059</td>
      <td>58</td>
      <td>0x3A</td>
      <td>Analog 7</td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>30060</td>
      <td>59</td>
      <td>0x3B</td>
      <td>Analog 8</td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>30061</td>
      <td>60</td>
      <td>0x3C</td>
      <td>Wartość 1 wejścia alarmowego</td>
      <td>Odczyt</td>
      <td rowspan="2">Aktualne wartości napięć/prądu dla wejść alarmowych</td>
    </tr>
    <tr>
      <td>30062</td>
      <td>61</td>
      <td>0x3D</td>
      <td>Wartość 2 wejścia alarmowego</td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>40063</td>
      <td>62</td>
      <td>0x3E</td>
      <td>Wartość max alarmu wejścia 1</td>
      <td>Odczyt i zapis</td>
      <td rowspan="8">Wartość maksymalna wejścia analogowego, po której przekroczeniu zapalony zostanie odpowiedni bit rejestru alarmowego.</td>
    </tr>
    <tr>
      <td>40064</td>
      <td>63</td>
      <td>0x3F</td>
      <td>Wartość max alarmu wejścia 2</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40065</td>
      <td>64</td>
      <td>0x40</td>
      <td>Wartość max alarmu wejścia 3</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40066</td>
      <td>65</td>
      <td>0x41</td>
      <td>Wartość max alarmu wejścia 4</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40067</td>
      <td>66</td>
      <td>0x42</td>
      <td>Wartość max alarmu wejścia 5</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40068</td>
      <td>67</td>
      <td>0x43</td>
      <td>Wartość max alarmu wejścia 6</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40069</td>
      <td>68</td>
      <td>0x44</td>
      <td>Wartość max alarmu wejścia 7</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40070</td>
      <td>69</td>
      <td>0x45</td>
      <td>Wartość max alarmu wejścia 8</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40071</td>
      <td>70</td>
      <td>0x46</td>
      <td>Wartość min alarmu wejścia 1</td>
      <td>Odczyt i zapis</td>
      <td rowspan="8">Minimalna poziom wejścia analogowego. Jeżeli wartość spadnie poniżej tej wartości zostanie zapalony odpowiedni bit w rejestru alarmowego.</td>
    </tr>
    <tr>
      <td>40072</td>
      <td>71</td>
      <td>0x47</td>
      <td>Wartość min alarmu wejścia 2</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40073</td>
      <td>72</td>
      <td>0x48</td>
      <td>Wartość min alarmu wejścia 3</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40074</td>
      <td>73</td>
      <td>0x49</td>
      <td>Wartość min alarmu wejścia 4</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40075</td>
      <td>74</td>
      <td>0x4A</td>
      <td>Wartość min alarmu wejścia 5</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40076</td>
      <td>75</td>
      <td>0x4B</td>
      <td>Wartość min alarmu wejścia 6</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40077</td>
      <td>76</td>
      <td>0x4C</td>
      <td>Wartość min alarmu wejścia 7</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40078</td>
      <td>77</td>
      <td>0x4D</td>
      <td>Wartość min alarmu wejścia 8</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40079</td>
      <td>78</td>
      <td>0x4E</td>
      <td>Konfiguracja alarmu 1</td>
      <td>Odczyt i zapis</td>
      <td rowspan="8">Konfiguracja działania alarmu:
        <br>
        <br>0 – Alarm wynika z aktualnych wartości
        <br>1 – Pamiętaj wartość alarmu, aż do wyzerowania przez Mastera</td>
    </tr>
    <tr>
      <td>40080</td>
      <td>79</td>
      <td>0x4F</td>
      <td>Konfiguracja alarmu 2</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40081</td>
      <td>80</td>
      <td>0x50</td>
      <td>Konfiguracja alarmu 3</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40082</td>
      <td>81</td>
      <td>0x51</td>
      <td>Konfiguracja alarmu 4</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40083</td>
      <td>82</td>
      <td>0x52</td>
      <td>Konfiguracja alarmu 5</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40084</td>
      <td>83</td>
      <td>0x53</td>
      <td>Konfiguracja alarmu 6</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40085</td>
      <td>84</td>
      <td>0x54</td>
      <td>Konfiguracja alarmu 7</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40086</td>
      <td>85</td>
      <td>0x55</td>
      <td>Konfiguracja alarmu 8</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40087</td>
      <td>86</td>
      <td>0x56</td>
      <td>Konfiguracja wejścia 1</td>
      <td>Odczyt i zapis</td>
      <td rowspan="8">Tryb pracy wejścia analogowego:
        <br>
        <br>0 – Wejście wyłączone
        <br>1 – Wejście od 0V do 10V
        <br>2 – Wejście od -10V do 10V
        <br>3 – Wejście od 0V do 1V
        <br>4 – Wejście od -1V do 1V
        <br>5 – Wejście od 4mA do 20mA
        <br>6 – Wejście od 0mA do 20mA
        <br>7 – Wejście od -20mA do 20mA
        <br>
        <br>Aby zmiana trybu wejścia odniosła skutek należy również odpowiednio ustawić zworkę wewnątrz modułu (patrz – Podłączenie modułu i - Wybór trybu pracy wejścia)</td>
    </tr>
    <tr>
      <td>40088</td>
      <td>87</td>
      <td>0x57</td>
      <td>Konfiguracja wejścia 2</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40089</td>
      <td>88</td>
      <td>0x58</td>
      <td>Konfiguracja wejścia 3</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40090</td>
      <td>89</td>
      <td>0x59</td>
      <td>Konfiguracja wejścia 4</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40091</td>
      <td>90</td>
      <td>0x5A</td>
      <td>Konfiguracja wejścia 5</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40092</td>
      <td>91</td>
      <td>0x5B</td>
      <td>Konfiguracja wejścia 6</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40093</td>
      <td>92</td>
      <td>0x5C</td>
      <td>Konfiguracja wejścia 7</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40094</td>
      <td>93</td>
      <td>0x5D</td>
      <td>Konfiguracja wejścia 8</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40095</td>
      <td>94</td>
      <td>0x5E</td>
      <td>Konfiguracja wyjścia alarmowego 1</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Konfiguracja wyjścia alarmowego
        <br>0 – Wyjście sterowane przez PLC
        <br>+1 – Wartość alarmowa z wejścia 1
        <br>+2 – Wartość alarmowa z wejścia 2
        <br>+4 – Wartość alarmowa z wejścia 3
        <br>+8 – Wartość alarmowa z wejścia 4
        <br>+16 – Wartość alarmowa z wejścia 5
        <br>+32 – Wartość alarmowa z wejścia 6
        <br>+64 – Wartość alarmowa z wejścia 7
        <br>+128 – Wartość alarmowa z wejścia 7
        <br>
        <br>+256 – Wyjście załączane jeśli wartość większa od Wartości alarmowej (rejestr 40097 lub 40098) („chłodzenie”)
        <br>+512 – Wyjście załączane jeśli wartość mniejsza od Wartości alarmowej (rejestr 40097 lub 40098) („grzanie”)
        <br>
        <br>+1024 – Wartość minimalna z wybranych wejść
        <br>+2048 – Wartość maksymalna z wybranych wejść
        <br>(jeśli nie wybrano żadnej z dwóch powyższych opcji to liczona jest średnia z wybranych wejść)</td>
    </tr>
    <tr>
      <td>40096</td>
      <td>95</td>
      <td>0x5F</td>
      <td>Konfiguracja wyjścia alarmowego 2</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40097</td>
      <td>96</td>
      <td>0x60</td>
      <td>Poziom alarmu 1</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Poziomy alarmów dla wyjść alarmowych</td>
    </tr>
    <tr>
      <td>40098</td>
      <td>97</td>
      <td>0x61</td>
      <td>Poziom alarmu 2</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40099</td>
      <td>98</td>
      <td>0x62</td>
      <td>Histereza alarmu 1</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Histereza wejść alarmowych</td>
    </tr>
    <tr>
      <td>40100</td>
      <td>99</td>
      <td>0x63</td>
      <td>Histereza alarmu 2</td>
      <td>Odczyt i zapis</td>
    </tr>
  </tbody>
</table>

### Dostęp bitowy

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Modbus</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Dec</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Hex</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Nazwa rejestru</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Dostęp</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Opis</b>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>801</td>
      <td>800</td>
      <td>0x320</td>
      <td>Wejście 1 </td>
      <td>Odczyt</td>
      <td>Czy podłączone wejście</td>
    </tr>
    <tr>
      <td>802</td>
      <td>801</td>
      <td>0x321</td>
      <td>Wejście 2</td>
      <td>Odczyt</td>
      <td>Czy podłączone wejście</td>
    </tr>
    <tr>
      <td>803</td>
      <td>802</td>
      <td>0x322</td>
      <td>Wejście 3 </td>
      <td>Odczyt</td>
      <td>Czy podłączone wejście</td>
    </tr>
    <tr>
      <td>804</td>
      <td>803</td>
      <td>0x323</td>
      <td>Wejście 4 </td>
      <td>Odczyt</td>
      <td>Czy podłączone wejście</td>
    </tr>
    <tr>
      <td>805</td>
      <td>804</td>
      <td>0x324</td>
      <td>Wejście 5 </td>
      <td>Odczyt</td>
      <td>Czy podłączone wejście</td>
    </tr>
    <tr>
      <td>806</td>
      <td>805</td>
      <td>0x325</td>
      <td>Wejście 6 </td>
      <td>Odczyt</td>
      <td>Czy podłączone wejście</td>
    </tr>
    <tr>
      <td>807</td>
      <td>806</td>
      <td>0x326</td>
      <td>Wejście 7</td>
      <td>Odczyt</td>
      <td>Czy podłączone wejście</td>
    </tr>
    <tr>
      <td>808</td>
      <td>807</td>
      <td>0x327</td>
      <td>Wejście 8</td>
      <td>Odczyt</td>
      <td>Czy podłączone wejście</td>
    </tr>
    <tr>
      <td>817</td>
      <td>816</td>
      <td>0x330</td>
      <td>Alarm 1</td>
      <td>Odczyt </td>
      <td>Stan alarmu 1</td>
    </tr>
    <tr>
      <td>818</td>
      <td>817</td>
      <td>0x331</td>
      <td>Alarm 2</td>
      <td>Odczyt </td>
      <td>Stan alarmu 2</td>
    </tr>
    <tr>
      <td>819</td>
      <td>818</td>
      <td>0x332</td>
      <td>Alarm 3</td>
      <td>Odczyt </td>
      <td>Stan alarmu 3</td>
    </tr>
    <tr>
      <td>820</td>
      <td>819</td>
      <td>0x333</td>
      <td>Alarm 4</td>
      <td>Odczyt </td>
      <td>Stan alarmu 4</td>
    </tr>
    <tr>
      <td>821</td>
      <td>820</td>
      <td>0x334</td>
      <td>Alarm 5</td>
      <td>Odczyt </td>
      <td>Stan alarmu 5</td>
    </tr>
    <tr>
      <td>822</td>
      <td>821</td>
      <td>0x335</td>
      <td>Alarm 6</td>
      <td>Odczyt</td>
      <td>Stan alarmu 6</td>
    </tr>
    <tr>
      <td>823</td>
      <td>822</td>
      <td>0x336</td>
      <td>Alarm 7</td>
      <td>Odczyt</td>
      <td>Stan alarmu 7</td>
    </tr>
    <tr>
      <td>824</td>
      <td>823</td>
      <td>0x337</td>
      <td>Alarm 8</td>
      <td>Odczyt</td>
      <td>Stan alarmu 8</td>
    </tr>
    <tr>
      <td>825</td>
      <td>824</td>
      <td>0x338</td>
      <td>Wyjście cyfrowe 1 </td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia cyfrowego 1</td>
    </tr>
    <tr>
      <td>826</td>
      <td>825</td>
      <td>0x339</td>
      <td>Wyjście cyfrowe 2</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia cyfrowego 2</td>
    </tr>
  </tbody>
</table>

## Program konfiguracyjny

![](images/MO-AI8-SL/configurator.png) {.img-responsive}

Konfigurator jest oprogramowaniem służącym do ustawienia rejestrów odpowiedzialnych za komunikację modułu w magistrali Modbus jak również do odczytu i zapisu aktualnych wartości pozostałych rejestrów modułu. Dzięki temu programowi można w wygodny sposób przetestować układ jak również w czasie rzeczywistym obserwować zmiany w rejestrach.

Komunikacja z modułem odbywa się poprzez kabel USB. Do współdziałania programu z modułem nie jest wymagana instalacja żadnych sterowników.

Konfigurator jest uniwersalnym programem, za pomocą którego możliwa jest konfiguracja wszystkich dostępnych modułów.

