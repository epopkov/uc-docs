# UC MO-DI16-SL {.header}

[UC MO-DI16-SL Moduł rozszerzający](http://pl.unitecontrol.com/MO-DI16-SL.html) – 16 wejść cyfrowych

![](images/MO-DI16-SL/device.png) {.img-responsive}

## Zasady bezpieczeństwa

* Przed pierwszym uruchomieniem urządzenia należy zapoznać się z niniejszą instrukcją obsługi;
* Przed pierwszym uruchomieniem urządzenia należy upewnić się, że wszystkie przewody zostały podłączone prawidłowo;
* Należy zapewnić właściwe warunki pracy, zgodne ze specyfikacją urządzenia (np.: napięcie zasilania, temperatura, maksymalny pobór prądu);
* Przed dokonaniem jakichkolwiek modyfikacji przyłączeń przewodów, należy wyłączyć napięcie zasilania.

## Charakterystyka modułu

### Przeznaczenie i opis modułu

Moduł 16I jest innowacyjnym urządzeniem zapewniającym proste i niedrogie rozszerzenie ilości linii wejściowych w popularnych sterownikach PLC. 

Moduł posiada 16 wejść cyfrowych z opcją licznika. Dodatkowo zaciski IN1 i IN2 oraz IN3 i IN4 można wykorzystać do podłączenia dwóch enkoderów. Wszystkie wejścia są izolowane od logiki za pomocą transoptorów. Każdy kanał może być indywidualnie skonfigurowany w jeden z kilku trybów.

Moduł ten podłączany jest do magistrali RS485 za pomocą dwu przewodowej skrętki. Komunikacja odbywa się z wykorzystaniem protokołu MODBUS RTU lub MODBUS ASCII. Zastosowanie 32‑bitowego procesora z rdzeniem ARM zapewnia szybkie przetwarzanie danych i szybką komunikację. Prędkość transmisji jest konfigurowalna od 2400 do 115200. 

Moduł przeznaczony jest do montażu na szynie DIN zgodnie z normą DIN EN 5002.

Moduł został wyposażony z zestaw diod LED (kontrolek), używanych do wskazywania stanu wejść przydatnych w celach diagnostycznych i pomagających w znalezieniu błędów.

Konfiguracja modułu odbywa się przez USB za pomocą dedykowanego programu komputerowego. Możliwa jest również zmiana parametrów za pomocą protokołu MODBUS.

### Specyfikacja techniczna

<table>
  <tr>
    <th rowspan="2" bgcolor="#e6e6ff"><b>Zasilanie</b>
    </th>
    <th>Napięcie</th>
    <th>12-24 V DC ± 20%</th>
  </tr>
  <tr>
    <th>Prąd maksymalny<sup>*</sup>
    </th>
    <th>60 mA @ 12V / 40 mA @ 24V</th>
  </tr>
  <tr>
    <td rowspan="7" bgcolor="#e6e6ff"><b>Wejścia cyfrowe</b>
    </td>
    <td>Liczba wejść</td>
    <td>16</td>
  </tr>
  <tr>
    <td>Zakres napięć</td>
    <td>0 – 36V</td>
  </tr>
  <tr>
    <td>Stan niski „0”</td>
    <td>0 – 3V</td>
  </tr>
  <tr>
    <td>Stan wysoki „1”</td>
    <td>6 – 36V</td>
  </tr>
  <tr>
    <td>Impedancja wejściowa</td>
    <td>4kΩ</td>
  </tr>
  <tr>
    <td>Izolacja</td>
    <td>1500 Vrms</td>
  </tr>
  <tr>
    <td>Typ wejść</td>
    <td>PNP lub NPN</td>
  </tr>
  <tr>
    <td rowspan="4" bgcolor="#e6e6ff"><b>Liczniki</b>
    </td>
    <td>Ilość</td>
    <td>16</td>
  </tr>
  <tr>
    <td>Rozdzielczość</td>
    <td>32 bity</td>
  </tr>
  <tr>
    <td>Częstotliwość</td>
    <td>1kHz (max)</td>
  </tr>
  <tr>
    <td>Szerokość impulsu</td>
    <td>500 μs (min)</td>
  </tr>
  <tr>
    <td rowspan="2" bgcolor="#e6e6ff"><b>Temperatura</b>
    </td>
    <td>Pracy</td>
    <td>-10 °C - +50°C</td>
  </tr>
  <tr>
    <td>Przechowywania</td>
    <td>-40 °C - +85°C</td>
  </tr>
  <tr>
    <td rowspan="4" bgcolor="#e6e6ff"><b>Złącza</b>
    </td>
    <td>Zasilające</td>
    <td>2 pinowe</td>
  </tr>
  <tr>
    <td>Komunikacyjne</td>
    <td>3 pinowe</td>
  </tr>
  <tr>
    <td>Wejścia</td>
    <td>2 x 10 pinów</td>
  </tr>
  <tr>
    <td>Konfiguracyjne</td>
    <td>Mini USB</td>
  </tr>
  <tr>
    <td rowspan="3" bgcolor="#e6e6ff"><b>Wymiary</b>
    </td>
    <td>Wysokość</td>
    <td>120 mm</td>
  </tr>
  <tr>
    <td>Głębokość</td>
    <td>101 mm</td>
  </tr>
  <tr>
    <td>Szerokość</td>
    <td>22,5 mm</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Interfejs</b>
    </td>
    <td>RS485</td>
    <td>Do 128 urządzeń</td>
  </tr>
</table>

### Wymiary modułu

![](images/MO-DI16-SL/overview.png) {.img-responsive}

Wygląd i wymiary modułu znajdują się na rysunku poniżej. Moduł mocowany jest bezpośrednio do szyny w przemysłowym standardzie DIN. Złącza zasilające, komunikacyjne oraz wejść znajdują się od dołu i góry modułu. Złącze konfiguracyjne USB oraz wskaźniki znajdują się z przodu modułu.

## Konfiguracja komunikacji

### Uziemienie i ekranowanie

Moduł może być zainstalowany w obudowie wraz z innymi urządzeniami, które generują promieniowanie elektromagnetyczne. Przykładami takich urządzeń są przekaźniki i styczniki, transformatory, sterowniki silników itp. To promieniowanie elektromagnetyczne może powodować zakłócenia elektryczne zasilania i przewodów sygnałowych, a także promieniując bezpośrednio do modułu, powodując negatywne skutki dla systemu. Odpowiednie uziemienie, osłony oraz inne działania ochronne należy podjąć na etapie instalacji, aby zapobiec tym efektom. Te działania ochronne obejmują m.in. uziemienie szafy sterowniczej, uziemienie modułu, uziemienie ekranowania przewodów, zabezpieczenie urządzeń przełączających, prawidłowego okablowania, jak również uwzględnienie typów kabli i ich przekrojów. 

### Terminator

Efekty linii transmisyjnej często powodują problemy w sieciach teleinformatycznych. Problemy te dotyczą najczęściej tłumienia sygnału i odbić w sieci.

Aby wyeliminować obecność odbić od końców kabla, należy na obu jego końcach zastosować rezystor o impedancji równej impedancji charakterystycznej linii. W przypadku skrętki RS485 typową wartością jest 120 Ω.

### Ustalanie adresu modułu w sieci

Poniższa tabela przedstawia sposób ustawienia przełączników w celu ustalenia adresu modułu. Za pomocą przełączników możliwe jest ustawienie adresu od 0 do 31. Adresy od 32 do 255 możliwe są do ustawienia za pomocą magistrali RS485 lub przez złącze USB.

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff">Adr</td>
      <td bgcolor="#e6e6ff">SW5</td>
      <td bgcolor="#e6e6ff">SW4</td>
      <td bgcolor="#e6e6ff">SW3</td>
      <td bgcolor="#e6e6ff">SW2</td>
      <td bgcolor="#e6e6ff">SW1</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>1</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>2</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>3</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>4</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>5</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>6</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>7</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>8</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>9</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>10</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>11</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>12</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>13</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>14</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>15</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>16</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>17</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>18</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>19</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>20</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>21</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>22</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>23</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>24</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>25</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>26</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>27</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>28</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>29</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>30</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>31</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
  </tbody>
</table>

### Typy rejestrów Modbus

Są 4 typy zmiennych dostępnych w module. 

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Typ</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres początkowy</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Zmienna</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Dostęp</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Rozkaz Modbus</b>
    </td>
  </tr>
  <tr>
    <td>1</td>
    <td>00001</td>
    <td>Wyjścia cyfrowe</td>
    <td>Bitowy
      <br>Odczyt i zapis</td>
    <td>1, 5, 15</td>
  </tr>
  <tr>
    <td>2</td>
    <td>10001</td>
    <td>Wejścia cyfrowe</td>
    <td>Bitowy
      <br>Odczyt</td>
    <td>2</td>
  </tr>
  <tr>
    <td>3</td>
    <td>30001</td>
    <td>Rejestry wejściowe</td>
    <td>Rejestrowy
      <br>Odczyt</td>
    <td>3</td>
  </tr>
  <tr>
    <td>4</td>
    <td>40001</td>
    <td>Rejestry wyjściowe</td>
    <td>Rejestrowy
      <br>Odczyt i zapis</td>
    <td>4, 6, 16</td>
  </tr>
</table>

### Ustawienia komunikacji

Dane w modułach przechowywane są w 16 bitowych rejestrach. Dostęp do rejestrów odbywa się za pomocą protokołu MODBUS RTU lub MODBUS ASCII.

#### Domyślne parametry

Domyślną konfigurację można przywrócić za pomocą przełącznika SW6 (szczegóły w - Przywracanie konfiguracji domyślnej)

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Prędkość transmisji</b>
    </td>
    <td>19200</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Parzystość</b>
    </td>
    <td>Nie</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Ilość bitów danych</b>
    </td>
    <td>8</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Ilość bitów stopu</b>
    </td>
    <td>1</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Opóźnienie odpowiedzi [ms]</b>
    </td>
    <td>0</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Tryb Modbus</b>
    </td>
    <td>RTU</td>
  </tr>
</table>

#### Przywracanie konfiguracji domyślnej

W celu przywrócenia konfiguracji domyślnej należy przy wyłączonym zasilaniu modułu załączyć przełącznik SW6, a następnie włączyć zasilanie. Moduł zacznie migać na zmianę diodami wskazującymi zasilanie i komunikację. Jeżeli w tym stanie zostanie wyłączony przełącznik SW6 ustawienia zostaną nadpisane. 

**Uwaga!**Podczas przywracania konfiguracji domyślnej wykasowane zostaną również wszystkie inne wartości zapisane w rejestrach modułu!

#### Rejestry konfiguracyjne

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Adres</b>
      <br><b>Modbus</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres</b>
      <br><b>Dec</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres</b>
      <br><b>Hex</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Nazwa</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Wartości</b>
    </td>
  </tr>
  <tr>
    <td>40003</td>
    <td>2</td>
    <td>0x02</td>
    <td>Prędkość transmisji</td>
    <td>0 – 2400
      <br>1 – 4800
      <br>2 – 9600
      <br>3 – 19200
      <br>4 – 38400
      <br>5 – 57600
      <br>6 – 115200
      <br>inna wartość – wartość * 10</td>
  </tr>
  <tr>
    <td>40005</td>
    <td>4</td>
    <td>0x04</td>
    <td>Parzystość</td>
    <td>0 – brak
      <br>1 – nieparzystość
      <br>2 – parzystość
      <br>3 – zawsze 1
      <br>4 – zawsze 0</td>
  </tr>
  <tr>
    <td>40004</td>
    <td>3</td>
    <td>0x03</td>
    <td>Bity Stopu LSB</td>
    <td>1 – jeden bit stopu
      <br>2 – dwa bity stopu</td>
  </tr>
  <tr>
    <td>40004</td>
    <td>3</td>
    <td>0x03</td>
    <td>Bity Stopu MSB</td>
    <td>7 – 7 bitów danych
      <br>8 – 8 bitów danych</td>
  </tr>
  <tr>
    <td>40006</td>
    <td>5</td>
    <td>0x05</td>
    <td>Opóźnienie odpowiedzi</td>
    <td>Czas w ms</td>
  </tr>
  <tr>
    <td>40007</td>
    <td>6</td>
    <td>0x06</td>
    <td>Tryb Modbus</td>
    <td>0 – RTU
      <br>1 – ASCII</td>
  </tr>
</table>

## Wskaźniki diodowe

![](images/MO-DI16-SL/leds.png) {.img-responsive}

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Wskaźnik</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Opis</b>
    </td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Zasilanie</td>
    <td>Zapalona dioda oznacza, że moduł jest poprawnie zasilany.</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Komunikacja</td>
    <td>Dioda zapala się, gdy moduł odebrał prawidłowy pakiet i wysyła odpowiedź.</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Stany wejść</td>
    <td>Zapalona dioda informuje, że na wejściu jest stan wysoki.</td>
  </tr>
</table>

## Podłączenie modułu

![](images/MO-DI16-SL/connection.jpg) {.img-responsive}

## Ustawienia przełączników

![](images/MO-DI16-SL/switch.png) {.img-responsive}

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Przełącznik</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Funkcja</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Opis</b>
    </td>
  </tr>
  <tr>
    <td>1</td>
    <td>Adres modułu +1</td>
    <td rowspan="5">Ustawienie adresu modułu w zakresie od 0 do 31</td>
  </tr>
  <tr>
    <td>2</td>
    <td>Adres modułu +2</td>
  </tr>
  <tr>
    <td>3</td>
    <td>Adres modułu +4</td>
  </tr>
  <tr>
    <td>4</td>
    <td>Adres modułu +8</td>
  </tr>
  <tr>
    <td>5</td>
    <td>Adres modułu +16</td>
  </tr>
  <tr>
    <td>6</td>
    <td>Ustawienia domyślne modułu</td>
    <td>Ustawienie domyślnych parametrów transmisji
      <br>(patrz - Domyślne parametry i - Przywracanie konfiguracji domyślnej).</td>
  </tr>
</table>

## Rejestry modułu

### Dostęp rejestrowy

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Modbus</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Dec</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Hex</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Nazwa rejestru</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Dostęp</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Opis</b>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>30001</td>
      <td>0</td>
      <td>0x00</td>
      <td>Wersja/Typ</td>
      <td>Odczyt</td>
      <td>Typ i wersja urządzenia</td>
    </tr>
    <tr>
      <td>30002</td>
      <td>1</td>
      <td>0x01</td>
      <td>Przełączniki</td>
      <td>Odczyt</td>
      <td>Stan przełączników</td>
    </tr>
    <tr>
      <td>40003</td>
      <td>2</td>
      <td>0x02</td>
      <td>Prędkość</td>
      <td>Odczyt i zapis</td>
      <td>Prędkość transmisji</td>
    </tr>
    <tr>
      <td>40004</td>
      <td>3</td>
      <td>0x03</td>
      <td>Bity stopu i danych</td>
      <td>Odczyt i zapis</td>
      <td>Ilość bitów stopu</td>
    </tr>
    <tr>
      <td>40005</td>
      <td>4</td>
      <td>0x04</td>
      <td>Parzystość</td>
      <td>Odczyt i zapis</td>
      <td>Bit parzystości</td>
    </tr>
    <tr>
      <td>40006</td>
      <td>5</td>
      <td>0x05</td>
      <td>Opóźnienie</td>
      <td>Odczyt i zapis</td>
      <td>Opóźnienie odpowiedzi</td>
    </tr>
    <tr>
      <td>40007</td>
      <td>6</td>
      <td>0x06</td>
      <td>Typ Modbus</td>
      <td>Odczyt i zapis</td>
      <td>Typ protokołu Modbus </td>
    </tr>
    <tr>
      <td>40033</td>
      <td>32</td>
      <td>0x20</td>
      <td>Odebrane ramki MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość odebranych ramek</td>
    </tr>
    <tr>
      <td>40034</td>
      <td>33</td>
      <td>0x21</td>
      <td>Odebrane ramki LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40035</td>
      <td>34</td>
      <td>0x22</td>
      <td>Błędne ramki MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość odebranych błędnych ramek</td>
    </tr>
    <tr>
      <td>40036</td>
      <td>35</td>
      <td>0x23</td>
      <td>Błędne ramki LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40037</td>
      <td>36</td>
      <td>0x24</td>
      <td>Wysłane ramki MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość wysłanych ramek</td>
    </tr>
    <tr>
      <td>40038</td>
      <td>37</td>
      <td>0x25</td>
      <td>Wysłane ramki LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>30051</td>
      <td>50</td>
      <td>0x32</td>
      <td>Wejścia</td>
      <td>Odczyt</td>
      <td>Stan wejść</td>
    </tr>
    <tr>
      <td>40053</td>
      <td>52</td>
      <td>0x34</td>
      <td>Licznik 1 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 1</td>
    </tr>
    <tr>
      <td>40054</td>
      <td>53</td>
      <td>0x35</td>
      <td>Licznik 1 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40055</td>
      <td>54</td>
      <td>0x36</td>
      <td>Licznik 2 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 2</td>
    </tr>
    <tr>
      <td>40056</td>
      <td>55</td>
      <td>0x37</td>
      <td>Licznik 2 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40057</td>
      <td>56</td>
      <td>0x38</td>
      <td>Licznik 3 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 3</td>
    </tr>
    <tr>
      <td>40058</td>
      <td>57</td>
      <td>0x39</td>
      <td>Licznik 3 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40059</td>
      <td>58</td>
      <td>0x3A</td>
      <td>Licznik 4 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 4</td>
    </tr>
    <tr>
      <td>40060</td>
      <td>59</td>
      <td>0x3B</td>
      <td>Licznik 4 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40061</td>
      <td>60</td>
      <td>0x3C</td>
      <td>Licznik 5 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 5</td>
    </tr>
    <tr>
      <td>40062</td>
      <td>61</td>
      <td>0x3D</td>
      <td>Licznik 5 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40063</td>
      <td>62</td>
      <td>0x3E</td>
      <td>Licznik 6 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 6</td>
    </tr>
    <tr>
      <td>40064</td>
      <td>63</td>
      <td>0x3F</td>
      <td>Licznik 6 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40065</td>
      <td>64</td>
      <td>0x40</td>
      <td>Licznik 7 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 7</td>
    </tr>
    <tr>
      <td>40066</td>
      <td>65</td>
      <td>0x41</td>
      <td>Licznik 7 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40067</td>
      <td>66</td>
      <td>0x42</td>
      <td>Licznik 8 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 8</td>
    </tr>
    <tr>
      <td>40068</td>
      <td>67</td>
      <td>0x43</td>
      <td>Licznik 8 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40069</td>
      <td>68</td>
      <td>0x44</td>
      <td>Licznik 9 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 9</td>
    </tr>
    <tr>
      <td>40070</td>
      <td>69</td>
      <td>0x45</td>
      <td>Licznik 9 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40071</td>
      <td>70</td>
      <td>0x46</td>
      <td>Licznik 10 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 10</td>
    </tr>
    <tr>
      <td>40072</td>
      <td>71</td>
      <td>0x47</td>
      <td>Licznik 10 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40073</td>
      <td>72</td>
      <td>0x48</td>
      <td>Licznik 11 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 11</td>
    </tr>
    <tr>
      <td>40074</td>
      <td>73</td>
      <td>0x49</td>
      <td>Licznik 11 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40075</td>
      <td>74</td>
      <td>0x4A</td>
      <td>Licznik 12 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 12</td>
    </tr>
    <tr>
      <td>40076</td>
      <td>75</td>
      <td>0x4B</td>
      <td>Licznik 12 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40077</td>
      <td>76</td>
      <td>0x4C</td>
      <td>Licznik 13 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 13</td>
    </tr>
    <tr>
      <td>40078</td>
      <td>77</td>
      <td>0x4D</td>
      <td>Licznik 13 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40079</td>
      <td>78</td>
      <td>0x4E</td>
      <td>Licznik 14 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 14</td>
    </tr>
    <tr>
      <td>40080</td>
      <td>79</td>
      <td>0x4F</td>
      <td>Licznik 14 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40081</td>
      <td>80</td>
      <td>0x50</td>
      <td>Licznik 15 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 15</td>
    </tr>
    <tr>
      <td>40082</td>
      <td>81</td>
      <td>0x51</td>
      <td>Licznik 15 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40083</td>
      <td>82</td>
      <td>0x52</td>
      <td>Licznik 16 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowy licznik 16</td>
    </tr>
    <tr>
      <td>40084</td>
      <td>83</td>
      <td>0x53</td>
      <td>Licznik 16 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40085</td>
      <td>84</td>
      <td>0x54</td>
      <td>LicznikP 1 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 1</td>
    </tr>
    <tr>
      <td>40086</td>
      <td>85</td>
      <td>0x55</td>
      <td>LicznikP 1 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40087</td>
      <td>86</td>
      <td>0x56</td>
      <td>LicznikP 2 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 2</td>
    </tr>
    <tr>
      <td>40088</td>
      <td>87</td>
      <td>0x57</td>
      <td>LicznikP 2 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40089</td>
      <td>88</td>
      <td>0x58</td>
      <td>LicznikP 3 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 3</td>
    </tr>
    <tr>
      <td>40090</td>
      <td>89</td>
      <td>0x59</td>
      <td>LicznikP 3 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40091</td>
      <td>90</td>
      <td>0x5A</td>
      <td>LicznikP 4 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 4</td>
    </tr>
    <tr>
      <td>40092</td>
      <td>91</td>
      <td>0x5B</td>
      <td>LicznikP 4 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40093</td>
      <td>92</td>
      <td>0x5C</td>
      <td>LicznikP 5 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 5</td>
    </tr>
    <tr>
      <td>40094</td>
      <td>93</td>
      <td>0x5D</td>
      <td>LicznikP 5 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40095</td>
      <td>94</td>
      <td>0x5E</td>
      <td>LicznikP 6 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 6</td>
    </tr>
    <tr>
      <td>40096</td>
      <td>95</td>
      <td>0x5F</td>
      <td>LicznikP 6 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40097</td>
      <td>96</td>
      <td>0x60</td>
      <td>LicznikP 7 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 7</td>
    </tr>
    <tr>
      <td>40098</td>
      <td>97</td>
      <td>0x61</td>
      <td>LicznikP 7 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40099</td>
      <td>98</td>
      <td>0x62</td>
      <td>LicznikP 8 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 8</td>
    </tr>
    <tr>
      <td>40100</td>
      <td>99</td>
      <td>0x63</td>
      <td>LicznikP 8 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40101</td>
      <td>100</td>
      <td>0x64</td>
      <td>LicznikP 9 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 9</td>
    </tr>
    <tr>
      <td>40102</td>
      <td>101</td>
      <td>0x65</td>
      <td>LicznikP 9 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40103</td>
      <td>102</td>
      <td>0x66</td>
      <td>LicznikP 10 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 10</td>
    </tr>
    <tr>
      <td>40104</td>
      <td>103</td>
      <td>0x67</td>
      <td>LicznikP 10 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40105</td>
      <td>104</td>
      <td>0x68</td>
      <td>LicznikP 11 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 11</td>
    </tr>
    <tr>
      <td>40106</td>
      <td>105</td>
      <td>0x69</td>
      <td>LicznikP 11 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40107</td>
      <td>106</td>
      <td>0x6A</td>
      <td>LicznikP 12 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 12</td>
    </tr>
    <tr>
      <td>40108</td>
      <td>107</td>
      <td>0x6B</td>
      <td>LicznikP 12 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40109</td>
      <td>108</td>
      <td>0x6C</td>
      <td>LicznikP 13 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 13</td>
    </tr>
    <tr>
      <td>40110</td>
      <td>109</td>
      <td>0x6D</td>
      <td>LicznikP 13 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40111</td>
      <td>110</td>
      <td>0x6E</td>
      <td>LicznikP 14 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 14</td>
    </tr>
    <tr>
      <td>40112</td>
      <td>111</td>
      <td>0x6F</td>
      <td>LicznikP 14 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40113</td>
      <td>112</td>
      <td>0x70</td>
      <td>LicznikP 15 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 15</td>
    </tr>
    <tr>
      <td>40114</td>
      <td>113</td>
      <td>0x71</td>
      <td>LicznikP 15 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40115</td>
      <td>114</td>
      <td>0x72</td>
      <td>LicznikP 16 MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">32 bitowa przechwycona wartość licznika 16</td>
    </tr>
    <tr>
      <td>40116</td>
      <td>115</td>
      <td>0x73</td>
      <td>LicznikP 16 LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40117</td>
      <td>116</td>
      <td>0x74</td>
      <td>LicznikKonf 1</td>
      <td>Odczyt i zapis</td>
      <td rowspan="16">Konfiguracja liczników
        <br>
        <br>+1 – liczenie czasu (jeśli zero liczenie impulsów)
        <br>+2 – przechwytywanie wartości co 1 sek
        <br>+4 – przechwycenie wartości, jeśli wejście nieaktywne
        <br>+8 – automatyczne zerowanie po przechwyceniu
        <br>+16 – zerowanie licznika, jeśli wejście nieaktywne
        <br>+32 – enkoder (tylko dla licznika 1 i 3)</td>
    </tr>
    <tr>
      <td>40118</td>
      <td>117</td>
      <td>0x75</td>
      <td>LicznikKonf 2</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40119</td>
      <td>118</td>
      <td>0x76</td>
      <td>LicznikKonf 3</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40120</td>
      <td>119</td>
      <td>0x77</td>
      <td>LicznikKonf 4</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40121</td>
      <td>120</td>
      <td>0x78</td>
      <td>LicznikKonf 5</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40122</td>
      <td>121</td>
      <td>0x79</td>
      <td>LicznikKonf 6</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40123</td>
      <td>122</td>
      <td>0x7A</td>
      <td>LicznikKonf 7</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40124</td>
      <td>123</td>
      <td>0x7B</td>
      <td>LicznikKonf 8</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40125</td>
      <td>124</td>
      <td>0x7C</td>
      <td>LicznikKonf 9</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40126</td>
      <td>125</td>
      <td>0x7D</td>
      <td>LicznikKonf 10</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40127</td>
      <td>126</td>
      <td>0x7E</td>
      <td>LicznikKonf 11</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40128</td>
      <td>127</td>
      <td>0x7F</td>
      <td>LicznikKonf 12</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40129</td>
      <td>128</td>
      <td>0x80</td>
      <td>LicznikKonf 13</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40130</td>
      <td>129</td>
      <td>0x81</td>
      <td>LicznikKonf 14</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40131</td>
      <td>130</td>
      <td>0x82</td>
      <td>LicznikKonf 15</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40132</td>
      <td>131</td>
      <td>0x83</td>
      <td>LicznikKonf 16</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40133</td>
      <td>132</td>
      <td>0x84</td>
      <td>Przechwyć</td>
      <td>Odczyt i zapis</td>
      <td>Przechwyć wartości liczników</td>
    </tr>
    <tr>
      <td>40134</td>
      <td>133</td>
      <td>0x85</td>
      <td>Status</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik</td>
    </tr>
  </tbody>
</table>

### Dostęp bitowy

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Modbus</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Dec</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Hex</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Nazwa rejestru</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Dostęp</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Opis</b>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>10801</td>
      <td>800</td>
      <td>0x320</td>
      <td>Wejście 1</td>
      <td>Odczyt</td>
      <td>Stan wejścia 1</td>
    </tr>
    <tr>
      <td>10802</td>
      <td>801</td>
      <td>0x321</td>
      <td>Wejście 2</td>
      <td>Odczyt</td>
      <td>Stan wejścia 2</td>
    </tr>
    <tr>
      <td>10803</td>
      <td>802</td>
      <td>0x322</td>
      <td>Wejście 3</td>
      <td>Odczyt</td>
      <td>Stan wejścia 3</td>
    </tr>
    <tr>
      <td>10804</td>
      <td>803</td>
      <td>0x323</td>
      <td>Wejście 4</td>
      <td>Odczyt</td>
      <td>Stan wejścia 4</td>
    </tr>
    <tr>
      <td>10805</td>
      <td>804</td>
      <td>0x324</td>
      <td>Wejście 5</td>
      <td>Odczyt</td>
      <td>Stan wejścia 5</td>
    </tr>
    <tr>
      <td>10806</td>
      <td>805</td>
      <td>0x325</td>
      <td>Wejście 6</td>
      <td>Odczyt</td>
      <td>Stan wejścia 6</td>
    </tr>
    <tr>
      <td>10807</td>
      <td>806</td>
      <td>0x326</td>
      <td>Wejście 7</td>
      <td>Odczyt</td>
      <td>Stan wejścia 7</td>
    </tr>
    <tr>
      <td>10808</td>
      <td>807</td>
      <td>0x327</td>
      <td>Wejście 8</td>
      <td>Odczyt</td>
      <td>Stan wejścia 8</td>
    </tr>
    <tr>
      <td>10809</td>
      <td>808</td>
      <td>0x328</td>
      <td>Wejście 9</td>
      <td>Odczyt</td>
      <td>Stan wejścia 9</td>
    </tr>
    <tr>
      <td>10810</td>
      <td>809</td>
      <td>0x329</td>
      <td>Wejście 10</td>
      <td>Odczyt</td>
      <td>Stan wejścia 10</td>
    </tr>
    <tr>
      <td>10811</td>
      <td>810</td>
      <td>0x32A</td>
      <td>Wejście 11</td>
      <td>Odczyt</td>
      <td>Stan wejścia 11</td>
    </tr>
    <tr>
      <td>10812</td>
      <td>811</td>
      <td>0x32B</td>
      <td>Wejście 12</td>
      <td>Odczyt</td>
      <td>Stan wejścia 12</td>
    </tr>
    <tr>
      <td>10813</td>
      <td>812</td>
      <td>0x32C</td>
      <td>Wejście 13</td>
      <td>Odczyt</td>
      <td>Stan wejścia 13</td>
    </tr>
    <tr>
      <td>10814</td>
      <td>813</td>
      <td>0x32D</td>
      <td>Wejście 14</td>
      <td>Odczyt</td>
      <td>Stan wejścia 14</td>
    </tr>
    <tr>
      <td>10815</td>
      <td>814</td>
      <td>0x32E</td>
      <td>Wejście 15</td>
      <td>Odczyt</td>
      <td>Stan wejścia 15</td>
    </tr>
    <tr>
      <td>10816</td>
      <td>815</td>
      <td>0x32F</td>
      <td>Wejście 16</td>
      <td>Odczyt</td>
      <td>Stan wejścia 16</td>
    </tr>
    <tr>
      <td>2113</td>
      <td>2112</td>
      <td>0x840</td>
      <td>Przechwyć 1</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 1</td>
    </tr>
    <tr>
      <td>2114</td>
      <td>2113</td>
      <td>0x841</td>
      <td>Przechwyć 2</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 2</td>
    </tr>
    <tr>
      <td>2115</td>
      <td>2114</td>
      <td>0x842</td>
      <td>Przechwyć 3</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 3</td>
    </tr>
    <tr>
      <td>2116</td>
      <td>2115</td>
      <td>0x843</td>
      <td>Przechwyć 4</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 4</td>
    </tr>
    <tr>
      <td>2117</td>
      <td>2116</td>
      <td>0x844</td>
      <td>Przechwyć 5</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 5</td>
    </tr>
    <tr>
      <td>2118</td>
      <td>2117</td>
      <td>0x845</td>
      <td>Przechwyć 6</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 6</td>
    </tr>
    <tr>
      <td>2119</td>
      <td>2118</td>
      <td>0x846</td>
      <td>Przechwyć 7</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 7</td>
    </tr>
    <tr>
      <td>2120</td>
      <td>2119</td>
      <td>0x847</td>
      <td>Przechwyć 8</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 8</td>
    </tr>
    <tr>
      <td>2121</td>
      <td>2120</td>
      <td>0x848</td>
      <td>Przechwyć 9</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 9</td>
    </tr>
    <tr>
      <td>2122</td>
      <td>2121</td>
      <td>0x849</td>
      <td>Przechwyć 10</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 10</td>
    </tr>
    <tr>
      <td>2123</td>
      <td>2122</td>
      <td>0x84A</td>
      <td>Przechwyć 11</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 11</td>
    </tr>
    <tr>
      <td>2124</td>
      <td>2123</td>
      <td>0x84B</td>
      <td>Przechwyć 12</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 12</td>
    </tr>
    <tr>
      <td>2125</td>
      <td>2124</td>
      <td>0x84C</td>
      <td>Przechwyć 13</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 13</td>
    </tr>
    <tr>
      <td>2126</td>
      <td>2125</td>
      <td>0x84D</td>
      <td>Przechwyć 14</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 14</td>
    </tr>
    <tr>
      <td>2127</td>
      <td>2126</td>
      <td>0x84E</td>
      <td>Przechwyć 15</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 15</td>
    </tr>
    <tr>
      <td>2128</td>
      <td>2127</td>
      <td>0x84F</td>
      <td>Przechwyć 16</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycenie licznika 16</td>
    </tr>
    <tr>
      <td>2129</td>
      <td>2128</td>
      <td>0x850</td>
      <td>Przechwycono 1</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 1</td>
    </tr>
    <tr>
      <td>2130</td>
      <td>2129</td>
      <td>0x851</td>
      <td>Przechwycono 2</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 2</td>
    </tr>
    <tr>
      <td>2131</td>
      <td>2130</td>
      <td>0x852</td>
      <td>Przechwycono 3</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 3</td>
    </tr>
    <tr>
      <td>2132</td>
      <td>2131</td>
      <td>0x853</td>
      <td>Przechwycono 4</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 4</td>
    </tr>
    <tr>
      <td>2133</td>
      <td>2132</td>
      <td>0x854</td>
      <td>Przechwycono 5</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 5</td>
    </tr>
    <tr>
      <td>2134</td>
      <td>2133</td>
      <td>0x855</td>
      <td>Przechwycono 6</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 6</td>
    </tr>
    <tr>
      <td>2135</td>
      <td>2134</td>
      <td>0x856</td>
      <td>Przechwycono 7</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 7</td>
    </tr>
    <tr>
      <td>2136</td>
      <td>2135</td>
      <td>0x857</td>
      <td>Przechwycono 8</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 8</td>
    </tr>
    <tr>
      <td>2137</td>
      <td>2136</td>
      <td>0x858</td>
      <td>Przechwycono 9</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 9</td>
    </tr>
    <tr>
      <td>2138</td>
      <td>2137</td>
      <td>0x859</td>
      <td>Przechwycono 10</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 10</td>
    </tr>
    <tr>
      <td>2139</td>
      <td>2138</td>
      <td>0x85A</td>
      <td>Przechwycono 11</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 11</td>
    </tr>
    <tr>
      <td>2140</td>
      <td>2139</td>
      <td>0x85B</td>
      <td>Przechwycono 12</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 12</td>
    </tr>
    <tr>
      <td>2141</td>
      <td>2140</td>
      <td>0x85C</td>
      <td>Przechwycono 13</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 13</td>
    </tr>
    <tr>
      <td>2142</td>
      <td>2141</td>
      <td>0x85D</td>
      <td>Przechwycono 14</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 14</td>
    </tr>
    <tr>
      <td>2143</td>
      <td>2142</td>
      <td>0x85E</td>
      <td>Przechwycono 15</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 15</td>
    </tr>
    <tr>
      <td>2144</td>
      <td>2143</td>
      <td>0x85F</td>
      <td>Przechwycono 16</td>
      <td>Odczyt i zapis</td>
      <td>Przechwycono licznik 16</td>
    </tr>
  </tbody>
</table>

## Program konfiguracyjny

![](images/MO-DI16-SL/configurator.png) {.img-responsive}

Konfigurator jest oprogramowaniem służącym do ustawienia rejestrów odpowiedzialnych za komunikację modułu w magistrali Modbus jak również do odczytu i zapisu aktualnych wartości pozostałych rejestrów modułu. Dzięki temu programowi można w wygodny sposób przetestować układ jak również w czasie rzeczywistym obserwować zmiany w rejestrach.

Komunikacja z modułem odbywa się poprzez kabel USB. Do współdziałania programu z modułem nie jest wymagana instalacja żadnych sterowników.

Konfigurator jest uniwersalnym programem, za pomocą którego możliwa jest konfiguracja wszystkich dostępnych modułów.

*Prąd przy stanie wysokim na wszystkich wejściach i aktywnej transmisji modbus

