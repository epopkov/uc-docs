# UC MO-AI1-MI {.header}

[UC MO-AI1-MI Moduł rozszerzający](http://pl.unitecontrol.com/MO-AI1-MI.html) – 1 wejście analogowe, 1 wyjście cyfrowe

![](images/MO-AI1-MI/device.png) {.img-responsive}

## Zasady bezpieczeństwa

* Przed pierwszym uruchomieniem urządzenia należy zapoznać się z niniejszą instrukcją obsługi;
* Przed pierwszym uruchomieniem urządzenia należy upewnić się, że wszystkie przewody zostały podłączone prawidłowo;
* Należy zapewnić właściwe warunki pracy, zgodne ze specyfikacją urządzenia (np.: napięcie zasilania, temperatura, maksymalny pobór prądu);
* Przed dokonaniem jakichkolwiek modyfikacji przyłączeń przewodów, należy wyłączyć napięcie zasilania.

## Charakterystyka modułu

### Przeznaczenie i opis modułu

* Moduł 1AI umożliwia pomiar wartości napięcia lub prądu oraz posiada jedno wyjście cyfrowe. Odczyt wartości następuje za pomocą magistrali RS485 (protokół Modbus), dzięki czemu w prosty sposób można zintegrować moduł z popularnymi sterownikami PLC, HMI lub komputerami PC wyposażonymi w odpowiednie przejściówki.
* Urządzenie posiada 1 wejście do pomiarów napięcia i 1 wejście do pomiaru prądu (jednocześnie mogą być wykorzystywane oba wejścia). Dodatkowo moduł wyposażony jest w jedno konfigurowalne wyjścia cyfrowe (typu PNP lub NPN).
* Moduł ten podłączany jest do magistrali RS485 za pomocą dwu przewodowej skrętki. Komunikacja odbywa się z wykorzystaniem protokołu MODBUS RTU lub MODBUS ASCII. Zastosowanie 32-bitowego procesora z rdzeniem ARM zapewnia szybkie przetwarzanie danych i szybką komunikację. Prędkość transmisji jest konfigurowalna od 2400 do 115200.
* Moduł przeznaczony jest do montażu na szynie DIN zgodnie z normą DIN EN 5002.
* Moduł został wyposażony z zestaw diod LED (kontrolek), używanych do wskazywania stanu wyjść przydatnych w celach diagnostycznych i pomagających w znalezieniu błędów.
* Konfiguracja modułu odbywa się przez USB za pomocą dedykowanego programu komputerowego. Możliwa jest również zmiana parametrów za pomocą protokołu MODBUS.

### Specyfikacja techniczna

<table>
  <tr>
    <th rowspan="2" bgcolor="#e6e6ff"><b>Zasilanie</b>
    </th>
    <th>Napięcie</th>
    <th>10-36 V DC ± 20%</th>
  </tr>
  <tr>
    <th>Prąd maksymalny</th>
    <th>70 mA @ 12V / 38 mA @ 24V</th>
  </tr>
  <tr>
    <td rowspan="7" bgcolor="#e6e6ff"><b>Wejścia </b>
    </td>
    <td>Liczba wejść</td>
    <td>2</td>
  </tr>
  <tr>
    <td>Wejście napięciowe</td>
    <td>-10V÷10V</td>
  </tr>
  <tr>
    <td>Wejście prądowe</td>
    <td>-20mA÷20mA</td>
  </tr>
  <tr>
    <td>Rozdzielczość przetwornika</td>
    <td>16 bitów</td>
  </tr>
  <tr>
    <td>Czas przetwarzania ADC</td>
    <td>70ms / kanał</td>
  </tr>
  <tr>
    <td>Błąd pomiaru napięcia</td>
    <td>Max ±1.7% </td>
  </tr>
  <tr>
    <td>Błąd pomiaru prądu</td>
    <td>Max ±0.1% </td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Wyjście cyfrowe</b>
    </td>
    <td>Maksymalny prąd i napięcie</td>
    <td>250mA / 50V</td>
  </tr>
  <tr>
    <td rowspan="2" bgcolor="#e6e6ff"><b>Temperatura</b>
    </td>
    <td>Pracy</td>
    <td>-20 °C - +65°C</td>
  </tr>
  <tr>
    <td>Przechowywania</td>
    <td>-40 °C - +85°C</td>
  </tr>
  <tr>
    <td rowspan="4" bgcolor="#e6e6ff"><b>Złącza</b>
    </td>
    <td>Zasilające</td>
    <td>3 pinowe</td>
  </tr>
  <tr>
    <td>Komunikacyjne</td>
    <td>3 pinowe</td>
  </tr>
  <tr>
    <td>Wejścia i wyjścia</td>
    <td>2 x 3 pinowe</td>
  </tr>
  <tr>
    <td>Konfiguracyjne</td>
    <td>Mini USB</td>
  </tr>
  <tr>
    <td rowspan="3" bgcolor="#e6e6ff"><b>Wymiary</b>
    </td>
    <td>Wysokość</td>
    <td>90 mm</td>
  </tr>
  <tr>
    <td>Głębokość</td>
    <td>56 mm</td>
  </tr>
  <tr>
    <td>Szerokość</td>
    <td>17 mm</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Interfejs</b>
    </td>
    <td>RS485</td>
    <td>Do 128 urządzeń</td>
  </tr>
</table>

### Wymiary modułu

![](images/MO-AI1-MI/overview.png) {.img-responsive}

Wygląd i wymiary modułu znajdują się na rysunku poniżej. Moduł mocowany jest bezpośrednio do szyny w przemysłowym standardzie DIN. 

## Konfiguracja komunikacji

### Uziemienie i ekranowanie

W większości przypadków, moduł będzie zainstalowany w obudowie wraz z innymi urządzeniami, które generują promieniowanie elektromagnetyczne. Przykładami takich urządzeń są przekaźniki i styczniki, transformatory, sterowniki silników itp. To promieniowanie elektromagnetyczne może powodować zakłócenia elektryczne zasilania i przewodów sygnałowych, a także promieniując bezpośrednio do modułu, powodując negatywne skutki dla systemu. Odpowiednie uziemienie, osłony oraz inne działania ochronne należy podjąć na etapie instalacji, aby zapobiec tym efektom. Te działania ochronne obejmują m.in. uziemienie szafy sterowniczej, uziemienie modułu, uziemienie ekranowania przewodów, zabezpieczenie urządzeń przełączających, prawidłowego okablowania, jak również uwzględnienie typów kabli i ich przekrojów. 

### Terminator

Efekty linii transmisyjnej często powodują problemy w sieciach teleinformatycznych. Problemy te dotyczą najczęściej tłumienia sygnału i odbić w sieci.

Aby wyeliminować obecność odbić od końców kabla, należy na obu jego końcach zastosować rezystor o impedancji równej impedancji charakterystycznej linii. W przypadku skrętki RS485 typową wartością jest 120 Ω.

### Typy rejestrów Modbus

Są 4 typy zmiennych dostępnych w module. 

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Typ</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres początkowy</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Zmienna</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Dostęp</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Rozkaz Modbus</b>
    </td>
  </tr>
  <tr>
    <td>1</td>
    <td>00001</td>
    <td>Wyjścia cyfrowe</td>
    <td>Bitowy
      <br>Odczyt i zapis</td>
    <td>1, 5, 15</td>
  </tr>
  <tr>
    <td>2</td>
    <td>10001</td>
    <td>Wejścia cyfrowe</td>
    <td>Bitowy
      <br>Odczyt</td>
    <td>2</td>
  </tr>
  <tr>
    <td>3</td>
    <td>30001</td>
    <td>Rejestry wejściowe</td>
    <td>Rejestrowy
      <br>Odczyt</td>
    <td>3</td>
  </tr>
  <tr>
    <td>4</td>
    <td>40001</td>
    <td>Rejestry wyjściowe</td>
    <td>Rejestrowy
      <br>Odczyt i zapis</td>
    <td>4, 6, 16</td>
  </tr>
</table>

### Ustawienia komunikacji

Dane w modułach przechowywane są w 16 bitowych rejestrach. Dostęp do rejestrów odbywa się za pomocą protokołu MODBUS RTU lub MODBUS ASCII.

#### Domyślne parametry

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Nazwa parametru</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Wartość</b>
    </td>
  </tr>
  <tr>
    <td><b>Adres</b>
    </td>
    <td>1</td>
  </tr>
  <tr>
    <td><b>Prędkość transmisji</b>
    </td>
    <td>19200</td>
  </tr>
  <tr>
    <td><b>Parzystość</b>
    </td>
    <td>Nie</td>
  </tr>
  <tr>
    <td><b>Ilość bitów danych</b>
    </td>
    <td>8</td>
  </tr>
  <tr>
    <td><b>Ilość bitów stopu</b>
    </td>
    <td>1</td>
  </tr>
  <tr>
    <td><b>Opóźnienie odpowiedzi [ms]</b>
    </td>
    <td>0</td>
  </tr>
  <tr>
    <td><b>Tryb Modbus</b>
    </td>
    <td>RTU</td>
  </tr>
</table>

#### Rejestry konfiguracyjne

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Adres</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Nazwa</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Wartości</b>
    </td>
  </tr>
  <tr>
    <td>40002</td>
    <td>Adres modułu</td>
    <td>Od 0 do 255</td>
  </tr>
  <tr>
    <td>40003</td>
    <td>Prędkość transmisji</td>
    <td>0 – 2400
      <br>1 – 4800
      <br>2 – 9600
      <br>3 – 19200
      <br>4 – 38400
      <br>5 – 57600
      <br>6 – 115200
      <br>inna wartość – wartość * 10</td>
  </tr>
  <tr>
    <td>40005</td>
    <td>Parzystość</td>
    <td>0 – brak
      <br>1 – nieparzystość
      <br>2 – parzystość
      <br>3 – zawsze 1
      <br>4 – zawsze 0</td>
  </tr>
  <tr>
    <td>40004</td>
    <td>Bity Stopu LSB</td>
    <td>1 – jeden bit stopu
      <br>2 – dwa bity stopu</td>
  </tr>
  <tr>
    <td>40004</td>
    <td>Bity Stopu MSB</td>
    <td>7 – 7 bitów danych
      <br>8 – 8 bitów danych</td>
  </tr>
  <tr>
    <td>40005</td>
    <td>Opóźnienie odpowiedzi</td>
    <td>Czas w ms</td>
  </tr>
  <tr>
    <td>40007</td>
    <td>Tryb Modbus</td>
    <td>0 – RTU
      <br>1 – ASCII</td>
  </tr>
</table>

## Wskaźniki diodowe

![](images/MO-AI1-MI/leds.gif) {.img-responsive}

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Wskaźnik</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Opis</b>
    </td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">ON</td>
    <td>Zapalona dioda oznacza, że moduł jest poprawnie zasilany.</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">TX</td>
    <td>Dioda zapala się, gdy moduł odebrał prawidłowy pakiet i wysyła odpowiedź.</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">V<sub>IN</sub>
    </td>
    <td>Dioda zapala się gdy napięcie wejściowe jest różne od 0</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">I<sub>IN</sub>
    </td>
    <td>Dioda zapala się gdy prąd wejściowy jest różny od 0</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">DO</td>
    <td>Stan wyjścia</td>
  </tr>
</table>

## Schemat blokowy modułu

![](images/MO-AI1-MI/scheme.png) {.img-responsive}

## Podłączenie modułu

![](images/MO-AI1-MI/connection.gif) {.img-responsive}

## Rejestry modułu

### Dostęp rejestrowy

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Modbus</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Dec</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Hex</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Nazwa rejestru</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Dostęp</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Opis</b>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>30001</td>
      <td>0</td>
      <td>0x00</td>
      <td>Wersja/Typ</td>
      <td>Odczyt</td>
      <td>Typ i wersja urządzenia</td>
    </tr>
    <tr>
      <td>40002</td>
      <td>1</td>
      <td>0x01</td>
      <td>Adres Modułu</td>
      <td>Odczyt i zapis</td>
      <td>Adres Modułu</td>
    </tr>
    <tr>
      <td>40003</td>
      <td>2</td>
      <td>0x02</td>
      <td>Prędkość</td>
      <td>Odczyt i zapis</td>
      <td>Prędkość transmisji</td>
    </tr>
    <tr>
      <td>40004</td>
      <td>3</td>
      <td>0x03</td>
      <td>Bity stopu</td>
      <td>Odczyt i zapis</td>
      <td>Ilość bitów stopu</td>
    </tr>
    <tr>
      <td>40005</td>
      <td>4</td>
      <td>0x04</td>
      <td>Parzystość</td>
      <td>Odczyt i zapis</td>
      <td>Bit parzystości</td>
    </tr>
    <tr>
      <td>40006</td>
      <td>5</td>
      <td>0x05</td>
      <td>Opóźnienie</td>
      <td>Odczyt i zapis</td>
      <td>Opóźnienie odpowiedzi</td>
    </tr>
    <tr>
      <td>40007</td>
      <td>6</td>
      <td>0x06</td>
      <td>Typ Modbus</td>
      <td>Odczyt i zapis</td>
      <td>Typ protokołu Modbus </td>
    </tr>
    <tr>
      <td>40010</td>
      <td>9</td>
      <td>0x09</td>
      <td>Filter</td>
      <td>Odczyt i zapis</td>
      <td>Filtrowanie pomiarów, wartość od 1 do 10</td>
    </tr>
    <tr>
      <td>40033</td>
      <td>32</td>
      <td>0x20</td>
      <td>Odebrane ramki MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość odebranych ramek</td>
    </tr>
    <tr>
      <td>40034</td>
      <td>33</td>
      <td>0x21</td>
      <td>Odebrane ramki LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40035</td>
      <td>34</td>
      <td>0x22</td>
      <td>Błędne ramki MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość odebranych błędnych ramek</td>
    </tr>
    <tr>
      <td>40036</td>
      <td>35</td>
      <td>0x23</td>
      <td>Błędne ramki LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40037</td>
      <td>36</td>
      <td>0x24</td>
      <td>Wysłane ramki MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość wysłanych ramek</td>
    </tr>
    <tr>
      <td>40038</td>
      <td>37</td>
      <td>0x25</td>
      <td>Wysłane ramki LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>30051</td>
      <td>50</td>
      <td>0x32</td>
      <td>Wejścia</td>
      <td>Odczyt</td>
      <td>Stan wejścia ≠ 0 → bit zapalony</td>
    </tr>
    <tr>
      <td>40052</td>
      <td>51</td>
      <td>0x33</td>
      <td>Wyjścia</td>
      <td>Odczyt i zapis</td>
      <td>Stan alarmów oraz wyjścia cyfrowego (bit 3)</td>
    </tr>
    <tr>
      <td>30053</td>
      <td>52</td>
      <td>0x34</td>
      <td>Napięcie</td>
      <td>Odczyt</td>
      <td>Wartość napięcia w μV</td>
    </tr>
    <tr>
      <td>30054</td>
      <td>53</td>
      <td>0x35</td>
      <td>Prąd</td>
      <td>Odczyt</td>
      <td>Wartość prądu w μA lub ‰</td>
    </tr>
    <tr>
      <td>40055</td>
      <td>54</td>
      <td>0x36</td>
      <td>Alarm – wartość max napięcia</td>
      <td>Odczyt i zapis</td>
      <td>Wartość maksymalna napięcia po przekroczeniu której zostanie zapalony bit 1 rejestru alarmowego</td>
    </tr>
    <tr>
      <td>40056</td>
      <td>55</td>
      <td>0x37</td>
      <td>Alarm – wartość min napięcia</td>
      <td>Odczyt i zapis</td>
      <td>Wartość minimalna napięcia. Jeśli wartość spanie poniżej tej wartości zostanie zapalony bit 1 rejestru alarmowego</td>
    </tr>
    <tr>
      <td>40057</td>
      <td>56</td>
      <td>0x38</td>
      <td>Alarm – wartość max prądu</td>
      <td>Odczyt i zapis</td>
      <td>Wartość maksymalna prądu po przekroczeniu której zostanie zapalony bit 2 rejestru alarmowego</td>
    </tr>
    <tr>
      <td>40058</td>
      <td>57</td>
      <td>0x39</td>
      <td>Alarm – wartość min prądu</td>
      <td>Odczyt i zapis</td>
      <td>Wartość minimalna prądu. Jeśli wartość spanie poniżej tej wartości zostanie zapalony bit 2 rejestru alarmowego</td>
    </tr>
    <tr>
      <td>40059</td>
      <td>58</td>
      <td>0x3A</td>
      <td>Konfiguracja alarmu napięciowego</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Konfiguracja działania alarmu:
        <br>0 – alarm wynika z aktualnych wartości
        <br>1 – pamiętanie alarmu – zerowanie przez mastera</td>
    </tr>
    <tr>
      <td>40060</td>
      <td>59</td>
      <td>0x3B</td>
      <td>Konfiguracja alarmu prądowego</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40061</td>
      <td>60</td>
      <td>0x3C</td>
      <td>Konfiguracja wejścia napięciowego</td>
      <td>Odczyt i zapis</td>
      <td>0 – wyłączone
        <br>1 – 0 .. 10V
        <br>2 – -10 .. 10V
        <br>3 – 0 .. 1V
        <br>4 – -1 .. 1V</td>
    </tr>
    <tr>
      <td>40062</td>
      <td>61</td>
      <td>0x3D</td>
      <td>Konfiguracja wejścia prądowego</td>
      <td>Odczyt i zapis</td>
      <td>0 – wyłączone
        <br>1 – 0 .. 20mA (w μA)
        <br>2 – 4 .. 20mA (w ‰)
        <br>3 – -20mA .. 20mA (w μA)</td>
    </tr>
    <tr>
      <td>40063</td>
      <td>62</td>
      <td>0x3E</td>
      <td>Konfiguracja wyjścia cyfrowego</td>
      <td>Odczyt i zapis</td>
      <td>Konfiguracja wyjścia cyfrowego
        <br>0 – wyjście sterowane przez mastera
        <br>1 – wartość alarmowa na podstawie napięcia
        <br>2 – wartość alarmowa na podstawie prądu
        <br>+256 – wyjście załączane jeśli wartość większa od wartości alarmowej (rejestr 40065) („chłodzenie”)
        <br>+512 – wyjście załączane jeśli wartość mniejsza od wartości alarmowej (rejestr 40065) („grzanie”)</td>
    </tr>
    <tr>
      <td>40064</td>
      <td>63</td>
      <td>0x3F</td>
      <td>Poziom alarmu</td>
      <td>Odczyt i zapis</td>
      <td>Poziom alarmu dla wyjścia cyfrowego</td>
    </tr>
    <tr>
      <td>40065</td>
      <td>64</td>
      <td>0x40</td>
      <td>Histereza alarmu</td>
      <td>Odczyt i zapis</td>
      <td>Histereza wejścia alarmowego</td>
    </tr>
  </tbody>
</table>

### Dostęp bitowy

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Modbus</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Dec</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Hex</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Nazwa rejestru</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Dostęp</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Opis</b>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>801</td>
      <td>800</td>
      <td>0x320</td>
      <td>Wejście napięciowe</td>
      <td>Odczyt </td>
      <td>Stan wejścia napięciowego</td>
    </tr>
    <tr>
      <td>802</td>
      <td>801</td>
      <td>0x321</td>
      <td>Wejście prądowe</td>
      <td>Odczyt </td>
      <td>Stan wejścia prądowego</td>
    </tr>
    <tr>
      <td>10817</td>
      <td>816</td>
      <td>0x330</td>
      <td>Alarm napięciowy</td>
      <td>Odczyt i zapis</td>
      <td>Stan alarmu napięciowego</td>
    </tr>
    <tr>
      <td>10818</td>
      <td>817</td>
      <td>0x331</td>
      <td>Alarm prądowy</td>
      <td>Odczyt i zapis</td>
      <td>Stan alarmu prądowego</td>
    </tr>
    <tr>
      <td>10819</td>
      <td>818</td>
      <td>0x332</td>
      <td>Wyjście cyfrowe</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia cyfrowego</td>
    </tr>
  </tbody>
</table>

## Program konfiguracyjny

![](images/MO-AI1-MI/configurator.png) {.img-responsive}

Konfigurator Mini Modbus jest oprogramowaniem służącym do ustawienia rejestrów odpowiedzialnych za komunikację modułu w magistrali Modbus jak również do odczytu i zapisu aktualnych wartości pozostałych rejestrów modułu. Dzięki temu programowi można w wygodny sposób przetestować układ jak również w czasie rzeczywistym obserwować zmiany w rejestrach.

Komunikacja z modułem odbywa się poprzez kabel USB. Do współdziałania programu z modułem nie jest wymagana instalacja żadnych sterowników.

Konfigurator jest uniwersalnym programem, za pomocą którego możliwa jest konfiguracja wszystkich dostępnych modułów Mini Modbus.

