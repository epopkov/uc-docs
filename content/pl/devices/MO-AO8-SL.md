# UC MO-AO8-SL {.header}

[UC MO-AO8-SL Moduł rozszerzający](http://pl.unitecontrol.com/MO-AO8-SL.html) – 8 wyjść analogowych

![](images/MO-AO8-SL/device.png) {.img-responsive}

## Zasady bezpieczeństwa

* Przed pierwszym uruchomieniem urządzenia należy zapoznać się z niniejszą instrukcją obsługi;
* Przed pierwszym uruchomieniem urządzenia należy upewnić się, że wszystkie przewody zostały podłączone prawidłowo;
* Należy zapewnić właściwe warunki pracy, zgodne ze specyfikacją urządzenia (np.: napięcie zasilania, temperatura, maksymalny pobór prądu);
* Przed dokonaniem jakichkolwiek modyfikacji przyłączeń przewodów, należy wyłączyć napięcie zasilania.

## Charakterystyka modułu

### Przeznaczenie i opis modułu

* Moduł 8AO posiada 8 wyjść analogowych mogących pracować jako wyjście prądowe (0-20mA lub 4-20mA) lub wyjście napięciowe (0-10V). Ustawienie wartości napięcia lub prądu wyjściowego następuje za pomocą magistrali RS485 (protokół Modbus), dzięki czemu w prosty sposób można zintegrować moduł z popularnymi sterownikami PLC, HMI lub komputerami PC wyposażonymi w odpowiednie przejściówki.
* Moduł ten podłączany jest do magistrali RS485 za pomocą dwu przewodowej skrętki. Komunikacja odbywa się z wykorzystaniem protokołu MODBUS RTU lub MODBUS ASCII. Zastosowanie 32-bitowego procesora z rdzeniem ARM zapewnia szybkie przetwarzanie danych i szybką komunikację. Prędkość transmisji jest konfigurowalna od 2400 do 115200.
* Moduł przeznaczony jest do montażu na szynie DIN zgodnie z normą DIN EN 5002.
* Moduł został wyposażony z zestaw diod LED (kontrolek), używanych do wskazywania stanu wyjść przydatnych w celach diagnostycznych i pomagających w znalezieniu błędów.
* Konfiguracja modułu odbywa się przez USB za pomocą dedykowanego programu komputerowego. Możliwa jest również zmiana parametrów za pomocą protokołu MODBUS.

### Specyfikacja techniczna

<table>
  <tr>
    <th rowspan="2" bgcolor="#e6e6ff"><b>Zasilanie</b>
    </th>
    <th>Napięcie</th>
    <th>12-24 V DC ± 20%</th>
  </tr>
  <tr>
    <th>Prąd maksymalny</th>
    <th>120 mA @ 12V / 100 mA @ 24V</th>
  </tr>
  <tr>
    <td rowspan="6" bgcolor="#e6e6ff"><b>Wyjścia </b>
    </td>
    <td>Liczba wyjść</td>
    <td>8</td>
  </tr>
  <tr>
    <td>Wyjście napięciowe</td>
    <td>0V do 10V (rozdzielczość 1.5mV)</td>
  </tr>
  <tr>
    <td>Wyjście prądowe</td>
    <td>0mA do 20mA (rozdzielczość 5μA)</td>
  </tr>
  <tr>
    <td>Wyjście prądowe</td>
    <td>4mA do 20mA (wynik w promilach – 1000 kroków) (rozdzielczość 16μA)</td>
  </tr>
  <tr>
    <td>Rozdzielczość przetwornika</td>
    <td>12 bitów</td>
  </tr>
  <tr>
    <td>Czas przetwarzania DAC</td>
    <td>16ms / kanał</td>
  </tr>
  <tr>
    <td rowspan="2" bgcolor="#e6e6ff"><b>Temperatura</b>
    </td>
    <td>Pracy</td>
    <td>-20 °C - +65°C</td>
  </tr>
  <tr>
    <td>Przechowywania</td>
    <td>-40 °C - +85°C</td>
  </tr>
  <tr>
    <td rowspan="5" bgcolor="#e6e6ff"><b>Złącza</b>
    </td>
    <td>Zasilające moduł</td>
    <td>2 pinowe</td>
  </tr>
  <tr>
    <td>Komunikacyjne</td>
    <td>3 pinowe</td>
  </tr>
  <tr>
    <td>Wyjścia</td>
    <td>2 x 10 pinowe</td>
  </tr>
  <tr>
    <td>Szybkozłączka</td>
    <td>IDC10 </td>
  </tr>
  <tr>
    <td>Konfiguracyjne</td>
    <td>Mini USB</td>
  </tr>
  <tr>
    <td rowspan="3" bgcolor="#e6e6ff"><b>Wymiary</b>
    </td>
    <td>Wysokość</td>
    <td>120 mm</td>
  </tr>
  <tr>
    <td>Głębokość</td>
    <td>101 mm</td>
  </tr>
  <tr>
    <td>Szerokość</td>
    <td>22,5 mm</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Interfejs</b>
    </td>
    <td>RS485</td>
    <td>Do 128 urządzeń</td>
  </tr>
</table>

### Wymiary modułu

![](images/MO-AO8-SL/overview.png) {.img-responsive}

Wygląd i wymiary modułu znajdują się na rysunku poniżej. Moduł mocowany jest bezpośrednio do szyny w przemysłowym standardzie DIN. Złącza zasilające, komunikacyjne oraz wejść znajdują się od dołu i góry modułu. Złącze konfiguracyjne USB oraz wskaźniki znajdują się z przodu modułu.

## Konfiguracja komunikacji

### Uziemienie i ekranowanie

W większości przypadków, moduł będzie zainstalowany w obudowie wraz z innymi urządzeniami, które generują promieniowanie elektromagnetyczne. Przykładami takich urządzeń są przekaźniki i styczniki, transformatory, sterowniki silników itp. To promieniowanie elektromagnetyczne może powodować zakłócenia elektryczne zasilania i przewodów sygnałowych, a także promieniując bezpośrednio do modułu, powodując negatywne skutki dla systemu. Odpowiednie uziemienie, osłony oraz inne działania ochronne należy podjąć na etapie instalacji, aby zapobiec tym efektom. Te działania ochronne obejmują m.in. uziemienie szafy sterowniczej, uziemienie modułu, uziemienie ekranowania przewodów, zabezpieczenie urządzeń przełączających, prawidłowego okablowania, jak również uwzględnienie typów kabli i ich przekrojów. 

### Terminator

Efekty linii transmisyjnej często powodują problemy w sieciach teleinformatycznych. Problemy te dotyczą najczęściej tłumienia sygnału i odbić w sieci.

Aby wyeliminować obecność odbić od końców kabla, należy na obu jego końcach zastosować rezystor o impedancji równej impedancji charakterystycznej linii. W przypadku skrętki RS485 typową wartością jest 120 Ω.

### Ustalanie adresu modułu w sieci

Poniższa tabela przedstawia sposób ustawienia przełączników w celu ustalenia adresu modułu. Za pomocą przełączników możliwe jest ustawienie adresu od 0 do 31. Adresy od 32 do 255 możliwe są do ustawienia za pomocą magistrali RS485 lub przez złącze USB.

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff">Adr</td>
      <td bgcolor="#e6e6ff">SW5</td>
      <td bgcolor="#e6e6ff">SW4</td>
      <td bgcolor="#e6e6ff">SW3</td>
      <td bgcolor="#e6e6ff">SW2</td>
      <td bgcolor="#e6e6ff">SW1</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>1</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>2</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>3</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>4</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>5</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>6</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>7</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>8</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>9</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>10</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>11</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>12</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>13</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>14</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>15</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>16</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>17</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>18</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>19</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>20</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>21</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>22</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>23</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>24</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>25</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>26</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>27</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>28</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>29</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
      <td>ON</td>
    </tr>
    <tr>
      <td>30</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>OFF</td>
    </tr>
    <tr>
      <td>31</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
      <td>ON</td>
    </tr>
  </tbody>
</table>

### Typy rejestrów Modbus

Są 4 typy zmiennych dostępnych w module. 

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Typ</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres początkowy</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Zmienna</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Dostęp</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Rozkaz Modbus</b>
    </td>
  </tr>
  <tr>
    <td>1</td>
    <td>00001</td>
    <td>Wyjścia cyfrowe</td>
    <td>Bitowy
      <br>Odczyt i zapis</td>
    <td>1, 5, 15</td>
  </tr>
  <tr>
    <td>2</td>
    <td>10001</td>
    <td>Wejścia cyfrowe</td>
    <td>Bitowy
      <br>Odczyt</td>
    <td>2</td>
  </tr>
  <tr>
    <td>3</td>
    <td>30001</td>
    <td>Rejestry wejściowe</td>
    <td>Rejestrowy
      <br>Odczyt</td>
    <td>3</td>
  </tr>
  <tr>
    <td>4</td>
    <td>40001</td>
    <td>Rejestry wyjściowe</td>
    <td>Rejestrowy
      <br>Odczyt i zapis</td>
    <td>4, 6, 16</td>
  </tr>
</table>

### Ustawienia komunikacji

Dane w modułach przechowywane są w 16 bitowych rejestrach. Dostęp do rejestrów odbywa się za pomocą protokołu MODBUS RTU lub MODBUS ASCII.

#### Domyślne parametry

Domyślną konfigurację można przywrócić za pomocą przełącznika SW6 (szczegóły w - Przywracanie konfiguracji domyślnej)

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Prędkość transmisji</b>
    </td>
    <td>19200</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Parzystość</b>
    </td>
    <td>Nie</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Ilość bitów danych</b>
    </td>
    <td>8</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Ilość bitów stopu</b>
    </td>
    <td>1</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Opóźnienie odpowiedzi [ms]</b>
    </td>
    <td>0</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Tryb Modbus</b>
    </td>
    <td>RTU</td>
  </tr>
</table>

#### Przywracanie konfiguracji domyślnej

W celu przywrócenia konfiguracji domyślnej należy przy wyłączonym zasilaniu modułu załączyć przełącznik SW6, a następnie włączyć zasilanie. Moduł zacznie migać na zmianę diodami wskazującymi zasilanie i komunikację. Jeżeli w tym stanie zostanie wyłączony przełącznik SW6 ustawienia zostaną nadpisane. 

**Uwaga!**Podczas przywracania konfiguracji domyślnej wykasowane zostaną również wszystkie inne wartości zapisane w rejestrach modułu!

#### Rejestry konfiguracyjne

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Adres</b>
      <br><b>Modbus</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres</b>
      <br><b>Dec</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres</b>
      <br><b>Hex</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Nazwa</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Wartości</b>
    </td>
  </tr>
  <tr>
    <td>40003</td>
    <td>2</td>
    <td>0x02</td>
    <td>Prędkość transmisji</td>
    <td>0 – 2400
      <br>1 – 4800
      <br>2 – 9600
      <br>3 – 19200
      <br>4 – 38400
      <br>5 – 57600
      <br>6 – 115200
      <br>inna wartość – wartość * 10</td>
  </tr>
  <tr>
    <td>40005</td>
    <td>4</td>
    <td>0x04</td>
    <td>Parzystość</td>
    <td>0 – brak
      <br>1 – nieparzystość
      <br>2 – parzystość
      <br>3 – zawsze 1
      <br>4 – zawsze 0</td>
  </tr>
  <tr>
    <td>40004</td>
    <td>3</td>
    <td>0x03</td>
    <td>Bity Stopu LSB</td>
    <td>1 – jeden bit stopu
      <br>2 – dwa bity stopu</td>
  </tr>
  <tr>
    <td>40004</td>
    <td>3</td>
    <td>0x03</td>
    <td>Bity Stopu MSB</td>
    <td>7 – 7 bitów danych
      <br>8 – 8 bitów danych</td>
  </tr>
  <tr>
    <td>40006</td>
    <td>5</td>
    <td>0x05</td>
    <td>Opóźnienie odpowiedzi</td>
    <td>Czas w ms</td>
  </tr>
  <tr>
    <td>40007</td>
    <td>6</td>
    <td>0x06</td>
    <td>Tryb Modbus</td>
    <td>0 – RTU
      <br>1 – ASCII</td>
  </tr>
</table>

## Wskaźniki diodowe

![](images/MO-AO8-SL/leds.jpg) {.img-responsive}

## Otwieranie obudowy

![](images/MO-AO8-SL/disassemble.jpg) {.img-responsive}

1. Zdjąć zaczep poprzez jego naciśnięcie i przesunięcie w kierunku środka obudowy. Uwaga na znajdującą się pod zaczepem sprężynę. 2. Rozdzielić obudowę delikatnie odchylając za pomocą cienkiego narzędzia zaczepy znajdujące się w punktach pokazanych na rysunku.

## Podłączenie modułu

![](images/MO-AO8-SL/connection.jpg) {.img-responsive}

## Konfiguracja typu wyjścia

![](images/MO-AO8-SL/input_type.jpg) {.img-responsive}

By zmienić typ wyjścia z prądowego na napięciowe należy oprócz ustawienia odpowiednich wartości rejestrów konfigurujących (rejestry 40069 – 40076) zmienić położenie zworek wewnątrz wg poniższego rysunku.

## Ustawienia przełączników

![](images/MO-AO8-SL/switch.png) {.img-responsive}

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Przełącznik</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Funkcja</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Opis</b>
    </td>
  </tr>
  <tr>
    <td>1</td>
    <td>Adres modułu +1</td>
    <td rowspan="5">Ustawienie adresu modułu w zakresie od 0 do 31</td>
  </tr>
  <tr>
    <td>2</td>
    <td>Adres modułu +2</td>
  </tr>
  <tr>
    <td>3</td>
    <td>Adres modułu +4</td>
  </tr>
  <tr>
    <td>4</td>
    <td>Adres modułu +8</td>
  </tr>
  <tr>
    <td>5</td>
    <td>Adres modułu +16</td>
  </tr>
  <tr>
    <td>6</td>
    <td>Ustawienia domyślne modułu</td>
    <td>Ustawienie domyślnych parametrów transmisji
      <br>(patrz - Domyślne parametry i - Przywracanie konfiguracji domyślnej).</td>
  </tr>
</table>

## Rejestry modułu

### Dostęp rejestrowy

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Modbus</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Dec</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Hex</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Nazwa rejestru</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Dostęp</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Opis</b>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>30001</td>
      <td>0</td>
      <td>0x00</td>
      <td>Wersja/Typ</td>
      <td>Odczyt</td>
      <td>Typ i wersja urządzenia</td>
    </tr>
    <tr>
      <td>30002</td>
      <td>1</td>
      <td>0x01</td>
      <td>Przełączniki</td>
      <td>Odczyt</td>
      <td>Stan przełączników</td>
    </tr>
    <tr>
      <td>40003</td>
      <td>2</td>
      <td>0x02</td>
      <td>Prędkość</td>
      <td>Odczyt i zapis</td>
      <td>Prędkość transmisji</td>
    </tr>
    <tr>
      <td>40004</td>
      <td>3</td>
      <td>0x03</td>
      <td>Bity stopu</td>
      <td>Odczyt i zapis</td>
      <td>Ilość bitów stopu</td>
    </tr>
    <tr>
      <td>40005</td>
      <td>4</td>
      <td>0x04</td>
      <td>Parzystość</td>
      <td>Odczyt i zapis</td>
      <td>Bit parzystości</td>
    </tr>
    <tr>
      <td>40006</td>
      <td>5</td>
      <td>0x05</td>
      <td>Opóźnienie</td>
      <td>Odczyt i zapis</td>
      <td>Opóźnienie odpowiedzi</td>
    </tr>
    <tr>
      <td>40007</td>
      <td>6</td>
      <td>0x06</td>
      <td>Typ Modbus</td>
      <td>Odczyt i zapis</td>
      <td>Typ protokołu Modbus </td>
    </tr>
    <tr>
      <td>40009</td>
      <td>8</td>
      <td>0x08</td>
      <td>Watchdog</td>
      <td>Odczyt i zapis</td>
      <td>Watchdog</td>
    </tr>
    <tr>
      <td>40033</td>
      <td>32</td>
      <td>0x20</td>
      <td>Odebrane ramki MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość odebranych ramek</td>
    </tr>
    <tr>
      <td>40034</td>
      <td>33</td>
      <td>0x21</td>
      <td>Odebrane ramki LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40035</td>
      <td>34</td>
      <td>0x22</td>
      <td>Błędne ramki MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość odebranych błędnych ramek</td>
    </tr>
    <tr>
      <td>40036</td>
      <td>35</td>
      <td>0x23</td>
      <td>Błędne ramki LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40037</td>
      <td>36</td>
      <td>0x24</td>
      <td>Wysłane ramki MSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość wysłanych ramek</td>
    </tr>
    <tr>
      <td>40038</td>
      <td>37</td>
      <td>0x25</td>
      <td>Wysłane ramki LSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>30051</td>
      <td>50</td>
      <td>0x32</td>
      <td>Wyjście</td>
      <td>Odczyt </td>
      <td>Zapalony bit jeżeli wyjście != 0</td>
    </tr>
    <tr>
      <td>40053</td>
      <td>52</td>
      <td>0x34</td>
      <td>Wyjście analogowe 1</td>
      <td>Odczyt i zapis</td>
      <td rowspan="8">Wartość wyjścia analogowego:
        <br>
        <br>w mV dla wyjść napięciowych
        <br>(max 10240)
        <br>
        <br>w μA dla wyjść prądowych
        <br>0 - 20mA (max 20480)
        <br>
        <br>w ‰ dla wyjść prądowych
        <br>4-20mA (max 1000)</td>
    </tr>
    <tr>
      <td>40054</td>
      <td>53</td>
      <td>0x35</td>
      <td>Wyjście analogowe 2</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40055</td>
      <td>54</td>
      <td>0x36</td>
      <td>Wyjście analogowe 3</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40056</td>
      <td>55</td>
      <td>0x37</td>
      <td>Wyjście analogowe 4</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40057</td>
      <td>56</td>
      <td>0x38</td>
      <td>Wyjście analogowe 5</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40058</td>
      <td>57</td>
      <td>0x39</td>
      <td>Wyjście analogowe 6</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40059</td>
      <td>58</td>
      <td>0x3A</td>
      <td>Wyjście analogowe 7</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40060</td>
      <td>59</td>
      <td>0x3B</td>
      <td>Wyjście analogowe 8</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40061</td>
      <td>60</td>
      <td>0x3C</td>
      <td>Wartość domyślna wyjścia 1</td>
      <td>Odczyt i zapis</td>
      <td rowspan="8">Wartość domyślna wyjść ustawiana w momencie załączenia zasilania oraz na skutek zadziałania watchdoga</td>
    </tr>
    <tr>
      <td>40062</td>
      <td>61</td>
      <td>0x3D</td>
      <td>Wartość domyślna wyjścia 2</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40063</td>
      <td>62</td>
      <td>0x3E</td>
      <td>Wartość domyślna wyjścia 3</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40064</td>
      <td>63</td>
      <td>0x3F</td>
      <td>Wartość domyślna wyjścia 4</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40065</td>
      <td>64</td>
      <td>0x40</td>
      <td>Wartość domyślna wyjścia 5</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40066</td>
      <td>65</td>
      <td>0x41</td>
      <td>Wartość domyślna wyjścia 6</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40067</td>
      <td>66</td>
      <td>0x42</td>
      <td>Wartość domyślna wyjścia 7</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40068</td>
      <td>67</td>
      <td>0x43</td>
      <td>Wartość domyślna wyjścia 8</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40069</td>
      <td>68</td>
      <td>0x44</td>
      <td>Konfiguracja wyjścia 1</td>
      <td>Odczyt i zapis</td>
      <td rowspan="8">Konfiguracja trybu wyjścia:
        <br>
        <br>0 – wyjście wyłączone
        <br>1 – wyjście napięciowe
        <br><b>2 – wyjście prądowe </b><b>0-20mA</b>
        <br>3 – wyjście prądowe 4-20mA
        <br>
        <br>Uwaga! Aby zmiana odniosła skutek, należy również przestawić zworkę wewnątrz modułu.</td>
    </tr>
    <tr>
      <td>40070</td>
      <td>69</td>
      <td>0x45</td>
      <td>Konfiguracja wyjścia 2</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40071</td>
      <td>70</td>
      <td>0x46</td>
      <td>Konfiguracja wyjścia 3</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40072</td>
      <td>71</td>
      <td>0x47</td>
      <td>Konfiguracja wyjścia 4</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40073</td>
      <td>72</td>
      <td>0x48</td>
      <td>Konfiguracja wyjścia 5</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40074</td>
      <td>73</td>
      <td>0x49</td>
      <td>Konfiguracja wyjścia 6</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40075</td>
      <td>74</td>
      <td>0x4A</td>
      <td>Konfiguracja wyjścia 7</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40076</td>
      <td>75</td>
      <td>0x4B</td>
      <td>Konfiguracja wyjścia 8</td>
      <td>Odczyt i zapis</td>
    </tr>
  </tbody>
</table>

### Dostęp bitowy

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Modbus</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Dec</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Hex</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Nazwa rejestru</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Dostęp</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Opis</b>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>801</td>
      <td>800</td>
      <td>0x320</td>
      <td>Wejście 1 </td>
      <td>Odczyt</td>
      <td rowspan="8">Jeżeli napięcie lub prąd wyjściowy jest większy od zera to odpowiedni bit jest zapalony.</td>
    </tr>
    <tr>
      <td>802</td>
      <td>801</td>
      <td>0x321</td>
      <td>Wejście 2</td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>803</td>
      <td>802</td>
      <td>0x322</td>
      <td>Wejście 3 </td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>804</td>
      <td>803</td>
      <td>0x323</td>
      <td>Wejście 4 </td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>805</td>
      <td>804</td>
      <td>0x324</td>
      <td>Wejście 5 </td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>806</td>
      <td>805</td>
      <td>0x325</td>
      <td>Wejście 6 </td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>807</td>
      <td>806</td>
      <td>0x326</td>
      <td>Wejście 7</td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>808</td>
      <td>807</td>
      <td>0x327</td>
      <td>Wejście 8</td>
      <td>Odczyt</td>
    </tr>
  </tbody>
</table>

## Program konfiguracyjny

![](images/MO-AO8-SL/configurator.png) {.img-responsive}

Konfigurator jest oprogramowaniem służącym do ustawienia rejestrów odpowiedzialnych za komunikację modułu w magistrali Modbus jak również do odczytu i zapisu aktualnych wartości pozostałych rejestrów modułu. Dzięki temu programowi można w wygodny sposób przetestować układ jak również w czasie rzeczywistym obserwować zmiany w rejestrach.

Komunikacja z modułem odbywa się poprzez kabel USB. Do współdziałania programu z modułem nie jest wymagana instalacja żadnych sterowników.

Konfigurator jest uniwersalnym programem, za pomocą którego możliwa jest konfiguracja wszystkich dostępnych modułów.

